"""
drcorr.py: spatial correlation function for quenched displacements between frames 0 and Dt,
for a range of Dt.
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)

import numpy as np
from  numba import njit

from swap_tools import get_xi_dyn
from numba_tools import get_dp_com,correlate_field,parse_brackets
from nlist_tools import PBC,get_nlist,merge_nlists

from datetime import datetime

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-r","--r_string",
                   help="Array of r bin edges.\
                         Input [r0,r1,num_rs] corresponds to array\
                         redges = np.linspace(r0,r1,num_rs,endpoint=True).\
                         Default: None (np.linspace(0.5,40,159))",
                   default=None)
parser.add_argument("-R","--cage_radius",type=float,
                    help="Calculate displacements relative to the motion of the centre of mass of particles\
                          within a distance R from each particle (excluding the particle).\
                          Default: None (get cage_radius from get_xi_dyn)",
                    default=None)
parser.add_argument("-t","--tmax",type=float,
                   help="Max Dt (in time units) to use, out of those available.\
                         Default: np.inf (use all available frames).",
                   default=np.inf)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-i","--inpath",
                    help='Path to directory containing cnf directories for trajectories at the given T.\
                          Default: "{}/trajs_EQ_poly_SS_2D_N{{N:d}}_T{{T:.4f}}"'.format(host_info.TwoTimeTrajs),
                    default=None)
parser.add_argument("-o","--outfile",
                    help='Path to .npz in which to save output.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/dr2corr.npz"'.format(host_info.TwoTimeOut),
                    default=None)
parser.add_argument("-c","--pcfile",
                    help='Path to p_c .npz file (p_c for the different species).\
                          Default: "{}/T{T:.4f}/N{N:d}/pc_plateau.npz".',
                    default=None)
parser.add_argument("-p","--plateau",action='store_true',
                    help="This option sets a flag to divide squared displacements by the diam-dependent msd plateau.")
parser.add_argument("-s","--suffix",
                    help='Custom suffix to file name.\
                          Default: None (no extra suffix).',
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

# parse args

T = args.temperature
N = args.num_particles
redges = np.linspace(.5,40,159) if args.r_string is None else np.linspace(*parse_brackets(args.r_string,[float,float,int])); redges2=redges**2
R = get_xi_dyn(T) if args.cage_radius is None else args.cage_radius
tmax = args.tmax
inpath = "{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}".format(host_info.TwoTimeTrajs,N,T) if args.inpath is None else os.path.expanduser(args.inpath)
suffix = "" if args.suffix is None else "_{}".format(args.suffix)
outfile = "{}/T{:.4f}/N{:d}/dr2corr{}.npz".format(host_info.TwoTimeOut,T,N,suffix) if args.outfile is None else os.path.expanduser(args.outfile)

if args.plateau:
    outmain = outfile[:-4] if outfile[-4:]=='.npz' else outfile
    outfile = "{}_plat.npz".format(outmain)
    pcfile = "{}/T{:.4f}/N{:d}/pc_plateau.npz".format(host_info.TwoTimeOut,T,N) if args.pcfile is None else os.path.expanduser(args.pcfile)
    platdata = np.load(pcfile)

Cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=="Cnf-"])
if args.Cnfs is not None:
    Cnfs = np.intersect1d(Cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,Cnfs[0]),allow_pickle=True)
d,N,L = [typ(sys_params[key]) for (key,typ) in zip(['d','N','L'],[int,int,float])]
assert d==2

assert sys_params['step'][0]==0
tmask = sys_params['time'][1:]<=tmax
Dt_eyes = sys_params['step'][1:][tmask]
Dts = sys_params['time'][1:][tmask]

##########################

dr2corr = np.zeros((Dt_eyes.size,redges.size-1)) # normalised correlator
hist_global = np.zeros((Dt_eyes.size,redges.size-1),dtype=int)

for Cnf in Cnfs:
    hist,dr2dr2 = np.zeros((Dt_eyes.size,redges.size-1),dtype=int),np.zeros((Dt_eyes.size,redges.size-1))
    mean,var = np.zeros(Dt_eyes.size),np.zeros(Dt_eyes.size)

    path = "{}/Cnf-{:d}".format(inpath,Cnf)
    diams = np.load('{}/sys_params.npz'.format(path))['diameter']

    if args.plateau:
        types = np.searchsorted(platdata['dedges'][:-1],diams)-1
        pc2 = platdata['p_c'][types]**2

    p0 = np.load('{}/quenched_position/0.npy'.format(path))
    nlist0,nlist_inds0 = get_nlist(p0.astype(float),L,R)

    for i,Dt_i in enumerate(Dt_eyes):
        p1 = np.load('{}/quenched_position/{:d}.npy'.format(path,Dt_i))
        nlist1,nlist_inds1 = get_nlist(p1.astype(float),L,R)

        nlist,nlist_inds = merge_nlists(nlist0,nlist_inds0,nlist1,nlist_inds1)
        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist,nlist_inds)
        dr2 = np.sum(dp**2,axis=1)
        if args.plateau:
            dr2 /= pc2

        correlate_field(dr2,p0,L,redges2,hist[i,:],dr2dr2[i,:])
        correlate_field(dr2,p1,L,redges2,hist[i,:],dr2dr2[i,:]) # allowed by time reversal symmetry

        mean[i] = np.mean(dr2); var[i] = np.mean((dr2-mean[i])**2)

        print("\r{:d}/{:d}".format(i+1,Dt_eyes.size),flush=True,end='')

    print("{:d}/{:d}".format(Cnf,Cnfs[-1]),flush=True)

    anmask = hist>0
    dr2dr2[anmask] /= hist[anmask].astype(dr2dr2.dtype)
    dr2corr += (dr2dr2-mean.reshape((Dt_eyes.size,1))**2)/var.reshape((Dt_eyes.size,1))

    hist_global += hist

dr2corr /= float(Cnfs.size)
dr2corr[hist_global==0] = np.nan

np.savez(outfile,corr=dr2corr,Dt=Dts,redges=redges,metadata=metadata)
