"""
tau_of_R.py: get tau_alpha as a function of the length scale R_MW used to
counteract Mermin-Wagner fluctuations.
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

from numba_tools import get_dp_com,parse_brackets
from swap_tools import PBC,get_cage_R,get_tau_alpha
from nlist_tools import get_nlist,merge_nlists

import numpy as np
from scipy import special

##### function defs #####

def F_k(f_i,R,cnfs,inpath):
    F_k = 0.
    for cnf in cnfs:
        cnfpath = '{}/Cnf-{:d}'.format(inpath,cnf)
        sys_params = np.load('{}/sys_params.npz'.format(cnfpath),allow_pickle=True)
        N,L = int(sys_params['N']),float(sys_params['L'])
        steps,diams = sys_params['step'],sys_params['diameter']
        frame = steps[f_i]

        p0 = PBC(np.load('{}/position/0.npy'.format(cnfpath)).astype(float),L)
        p1 = PBC(np.load('{}/position/{:d}.npy'.format(cnfpath,frame)).astype(float),L)

        nlist_c0,nlist_inds_c0 = get_nlist(p0,L,R)
        nlist_c1,nlist_inds_c1 = get_nlist(p1,L,R)
        nlist_c,nlist_inds_c = merge_nlists(nlist_c0,nlist_inds_c0,nlist_c1,nlist_inds_c1)

        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)

        F_k += np.sum(special.j0(2.*np.pi*np.sqrt(np.sum(dp**2,axis=1))))

    F_k /= float(N*cnfs.size)

    return F_k

def secant(x0,y0,x1,y1):
    x2 = (y1*x0-y0*x1)/(y1-y0)
    return x2

#########################

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-R","--R_string",
                   help='Array of R_MW values to calculate tau_alpha for.\
                         Input [R0,R1,num_Rs] corresponds to array Rs = np.linspace(R0,R1,num_Rs,endpoint=True).\
                         Default: "[0.5,10,20]"',
                   default="[0.5,10,20]")
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-i","--maxiter",type=int,
		    help='Maximum number of iterations of secant method to use when finding tau_alpha.\
			  Default: 100',
		    default=100)
parser.add_argument("-e","--tolerance",type=float,
		    help='Terminate secant method search when |F_k(t)-e^{-1}|/e^{-1} < tolerance.\
			  Default: .05',
		    default=.05)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

T = args.temperature
N = args.num_particles
Rs = np.linspace(*parse_brackets(args.R_string,[float,float,int]),endpoint=True)
maxiter = args.maxiter
tol = args.tolerance

inpath = '{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.TwoTimeTrajs,N,T)
outfile = '{}/T{:.4f}/N{:d}/ta_vs_R.npz'.format(host_info.TwoTimeOut,T,N)

##########################

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,cnfs[0]),allow_pickle=True)
ts,steps = sys_params['time'],sys_params['step']

df_i2 = int(round(max(np.log(2)/np.log(ts[-1]/ts[-2]),1))) # num frames required to double (or halve) the tau_alpha guess

ta = np.zeros(Rs.size)
for R_i,R in enumerate(Rs):
    print("R_i={:d}".format(R_i))
    f_i0 = np.searchsorted(ts,get_tau_alpha(T)) if R_i==0 else f_i1
    F_k0 = F_k(f_i0,R,cnfs,inpath)
    
    f_i1 = max(f_i0-df_i2,1) if F_k0 < 0. else min(f_i0+df_i2,ts.size-1)
    F_k1 = F_k(f_i1,R,cnfs,inpath)
    
    for i in range(maxiter):
        if np.abs(F_k1-np.exp(-1))/np.exp(-1) < tol:
            break
    
        f_i_guess = int(round(secant(f_i0,F_k0-np.exp(-1),f_i1,F_k1-np.exp(-1))))
        if f_i_guess<=1 or f_i_guess>=ts.size-1:
            break
    
        f_i0,f_i1 = f_i1,int(round(f_i_guess))
        F_k0,F_k1 = F_k1,F_k(f_i1,R,cnfs,inpath)
    else:
        print("Warning: num iterations reached maxiter without termination conditions being met.")

    ta[R_i] = ts[f_i1]

np.savez(outfile,R=Rs,ta=ta,metadata=metadata)
