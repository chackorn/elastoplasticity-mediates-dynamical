"""
S4kt.py: Calculate dynamic structure function S4(k,Dt=tau_alpha)
as a function of k (dimensional k, 2 pi / L times my usual integer k)
for various values of cage-relative lenth R and the corresponding tau_alpha for that R,
using Bessel function j0 to massively reduce the computational cost.
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

from numba_tools import get_dp_com,update_hist,update_field,parse_brackets
from swap_tools import PBC,get_cage_R,get_tau_alpha,get_dminmax
from nlist_tools import get_nlist,merge_nlists

import numpy as np
from scipy import special

##### function defs #####


##
"""
Extending numba to accept j0,
from Stuart Archibald's code at: https://github.com/numba/numba/issues/3086#issuecomment-403469308
"""

from numba.extending import get_cython_function_address
from numba import vectorize, njit
import ctypes
import numpy as np

addr = get_cython_function_address("scipy.special.cython_special", "j0")
functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double)
j0_fn = functype(addr)

@vectorize('float64(float64)')
def vec_j0(x):
    return j0_fn(x)

@njit
def j0_in_njit(x):
    return vec_j0(x)
##


def msd_plateau(inpath,Dt_i,R,cnfs,dedges):
    """
    get mean-squared displacement at Dt=10 (a decade or so after the start of the plateau)
    for particles within particle diameter bins, given cage-relative length R
    """
    num_bins = dedges.size-1
    hist = np.zeros(num_bins,dtype=int)
    msd = np.zeros(num_bins)
    
    for cnf in cnfs:
        sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,cnf),allow_pickle=True)
        diams,L = sys_params['diameter'],float(sys_params['L'])
        types = np.searchsorted(dedges[:-1],diams)-1
    
        update_hist(types,hist)
    
        p0 = np.load("{}/Cnf-{:d}/position/0.npy".format(inpath,cnf))
        nlist0,nlist_inds0 = get_nlist(p0.astype(float),L,R)
    
        p1 = np.load("{}/Cnf-{:d}/position/{:d}.npy".format(inpath,cnf,Dt_i))
        nlist1,nlist_inds1 = get_nlist(p1.astype(float),L,R)
        nlist,nlist_inds = merge_nlists(nlist0,nlist_inds0,nlist1,nlist_inds1)
        
        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist,nlist_inds)
        
        update_field(types,np.sum(dp**2,axis=1),msd)

    msd /= hist.astype(msd.dtype)
    return msd

@njit
def get_Qhat(ks,p0,p1,Q,Qhat):
    """
    Q_tilde as defined in SM of https://dx.doi.org/10.1103/PhysRevLett.121.085703,
    after accounting for the fact that the structure is isotropic, so Q(k)=Q(|k|)
    (leading to use of j0 from averaging over k=|k|)
    """
    N,d = Q.size,p0.shape[1]
    #Qhat = np.zeros(ks.size,dtype=complex)
    for k_i in range(ks.size):
        k = ks[k_i]
        for i in range(N):
            Qhat[k_i] += np.exp(1j*k*p0[i,0])*Q[i]
    return Qhat/N

#########################

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-k","--k_string",
                    help="Array of k values.\
                          Input [k0,k1,num_ks] corresponds to array ks = np.linspace(k0,k1,num_ks,endpoint=True)).\
                          Default: [0.,6.5,27]",
                    default="[0.,6.5,27]")
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-p","--plateau_frame",type=int,
                    help="Simulation frame corresponding to msd plateau time.\
                          Default: 1000",
                    default=1000)
parser.add_argument("-d","--num_dbins",type=int,
                    help="Number of diameter bins into which to separate particle diameters.\
                          Default: 10",
                    default=10)
parser.add_argument("-t","--taufile",
                    help='Path to .npz file containing data for cage-relative R values and their corresponding tau_alpha values.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/ta_vs_R.npz"'.format(host_info.TwoTimeOut),
                    default=None)
parser.add_argument("-i","--inpath",
                    help='Path to trajectory directory.\
                          Default: "{}/trajs_EQ_poly_SS_2D_N{{N:d}}_T{{T:.4f}}"'.format(host_info.TwoTimeTrajs),
                    default=None)
parser.add_argument("-o","--outfile",
                    help='Path to .npz in which to save rmsd output.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/S4kt.npz"'.format(host_info.TwoTimeOut),
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

T = args.temperature
N = args.num_particles
ks = np.linspace(*parse_brackets(args.k_string,[float,float,int]),endpoint=True)
Dt_i_plat = args.plateau_frame
dedges = np.logspace(*np.log10(get_dminmax()),args.num_dbins+1)
inpath = '{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.TwoTimeTrajs,N,T) if args.inpath is None else os.path.expanduser(args.inpath)
taufile = '{}/T{:.4f}/N{:d}/ta_vs_R.npz'.format(host_info.TwoTimeOut,T,N) if args.taufile is None else os.path.expanduser(args.taufile)
outfile = '{}/T{:.4f}/N{:d}/S4kt.npz'.format(host_info.TwoTimeOut,T,N) if args.outfile is None else os.path.expanduser(args.outfile)

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

ta_data = np.load(taufile,allow_pickle=True)
Rs,tas = ta_data['R'],ta_data['ta']

sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,cnfs[0]),allow_pickle=True)
assert int(sys_params['N'])==N and int(sys_params['d'])==2
L,ts,steps = float(sys_params['L']),sys_params['time'],sys_params['step']

##########################

S4 = np.zeros((Rs.size,ks.size))

for R_i,R in enumerate(Rs):
    Qhats = np.zeros((cnfs.size,ks.size),dtype=complex)

    ta_i = steps[np.searchsorted(ts,tas[R_i])]
    p_msd = msd_plateau(inpath,Dt_i_plat,R,cnfs,dedges)

    for cnf in cnfs:
        sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,cnf),allow_pickle=True)
        diams = sys_params['diameter']
        types = np.searchsorted(dedges[:-1],diams)-1

        pc2 = np.zeros(N)
        for i in range(dedges.size-1):
            pc2[types==i] = p_msd[i]
        pc2 *= 4.

        p0 = np.load("{}/Cnf-{:d}/position/0.npy".format(inpath,cnf))
        nlist0,nlist_inds0 = get_nlist(p0.astype(float),L,R)

        p1 = np.load("{}/Cnf-{:d}/position/{:d}.npy".format(inpath,cnf,ta_i))
        nlist1,nlist_inds1 = get_nlist(p1.astype(float),L,R)
        nlist,nlist_inds = merge_nlists(nlist0,nlist_inds0,nlist1,nlist_inds1)

        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist,nlist_inds)

        Q = np.exp(-.5*np.sum(dp**2,axis=1)/pc2)

        Qhats[cnf-1] = get_Qhat(ks,p0,p1,Q,np.zeros(ks.size,dtype=complex))

    S4[R_i] = N*np.var(Qhats,axis=0)

    print("{:d}/{:d}".format(R_i+1,Rs.size))

np.savez(outfile,R=Rs,k=ks,S4=S4,metadata=metadata)
