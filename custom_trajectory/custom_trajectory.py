"""
custom_trajectory.py: run LAMMPS starting from a given equilibrated configuration,
dumping output at a custom set of frames (determined by the file args.path_to_print_steps fed to LAMMPS).
"""

import os
from shutil import rmtree
import pathlib
import subprocess
import shlex

import argparse
import numpy as np

import host_info
import sys; sys.path.extend(host_info.import_paths)
from swap_tools import read_config_file

### function defs ###

def init_config_from_data(outpath,seed,T=0.,ps=np.empty((0,2)),diams=np.empty(0),L=0.):
    assert np.all([ps.size,diams.size,L,T]) > 0

    np.random.seed(seed)

    N = diams.size
    vs = np.random.normal(0.,np.sqrt(T),2*N).reshape(N,2)  # initialise velocities based on temp
    vs -= np.mean(vs,axis=0).reshape(1,2)

    xlo,ylo,zlo = np.repeat(-L/2.,3)
    xhi,yhi,zhi = np.repeat(L/2.,3)

    pos_string = ""
    for i,(diam,p) in enumerate(zip(diams,ps)):
        pos_string += "  {atom_id:d} {atom_type:d} {diam:g} {x:g} {y:g} {z:g}\n".format(atom_id=1+i,atom_type=1,diam=diam,x=p[0],y=p[1],z=0.)
    
    vel_string = ""
    for i,v in enumerate(vs):
        vel_string += "  {atom_id:d} {vx:g} {vy:g} {vz:g}\n".format(atom_id=1+i,vx=v[0],vy=v[1],vz=0.)
    
    argdict = {'N':N,'xlo':xlo,'ylo':ylo,'zlo':zlo,'xhi':xhi,'yhi':yhi,'zhi':zhi,'pos_string':pos_string,'vel_string':vel_string}

    with open("{outpath}/init_config.txt".format(outpath=outpath),'w') as f:
        f.write("""\
# Input datafile for 2D polydisperse system

{N:d} atoms
1 atom types

{xlo:g} {xhi:g} xlo xhi
{ylo:g} {yhi:g} ylo yhi
{zlo:g} {zhi:g} zlo zhi

Masses

 1 1 # type_I mass

Pair Coeffs # lj/swap

1 1.0 1.0 2.017 1.25 0.2

Atoms # charge

{pos_string}

Velocities

{vel_string}""".format(outpath=outpath,**argdict))

    return

#####

def in_swap_from_data(outpath,outfile,print_steps,dt=0.,print_every=100,max_t=0.,T=0.):
    assert np.all([dt,max_t,T]) > 0. and print_every>0

    max_iter = int(max_t/dt)
    assert max_iter > 0

    with open("{outpath}/in.swap".format(outpath=outpath),'w') as f:
        f.write("""\
atom_modify map array
  
dimension 2
variable        skin equal 0.2

units           lj
atom_style      charge
boundary        p p p
pair_style      lj/swap 2.02

read_data       init_config.txt
reset_timestep  0         # dump initial config

pair_coeff      1 1 1.0 1.0 2.017 1.25 0.2

neighbor        ${{skin}} bin
neigh_modify    every 1 delay 0 check yes

timestep        {dt:g}

fix             1 all nve

variable        f file {print_steps}
variable        s equal next(f)

dump            xyz1 all xyz {print_every:d} {outpath}/{outfile}/*.xyz
dump_modify     xyz1 every v_s first yes

restart         10000000 swap.restart.1 swap.restart.2

run             {max_iter:d}

unfix           1
""".format(outpath=outpath,dt=dt,print_steps=print_steps,print_every=print_every,outfile=outfile,max_iter=max_iter))

    return

def convert_xyz_to_npy(outpath,outfile,print_steps):
    """
    convert xyz text files output by LAMMPS to single-precision npy files
    """

    steps = np.insert(np.loadtxt(print_steps,dtype=int)[:-1],0,0)
    for step in steps:
        fname = "{}/{}/{:d}".format(outpath,outfile,step)
        p = np.genfromtxt("{}.xyz".format(fname),skip_header=2,usecols=(1,2))
        np.save("{}.npy".format(fname),p.astype(np.single))
        os.remove("{}.xyz".format(fname))
    return 

#####################

####### read input #######

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-s","--seed",type=int,
                    help="Random seed for initial velocities.\
                          Default: None",
                    default=None)
parser.add_argument("-C","--Cnf",type=int,
                    help='Index of configuration for the given temperature to use.\
                          Default: 1',
                    default=1)
parser.add_argument("-t","--dt",type=float,
                    help="Numerical timestep for LAMMPS MD run.\
                          Default: 1e-2",
                    default=1e-2)
parser.add_argument("-p","--path_to_print_steps",
                    help='Path to file containing print steps to be read by LAMMPS.\
                          Default: "./print.steps"',
                    default="./print.steps")
parser.add_argument("-i","--inpath",
                    help='Path to directory containing configuration files.\
                          Default: "{}/configs_EQ_poly_SS_2D_N{{N:d}}_T{{T:.4f}}"'.format(host_info.UnquenchedConfigs),
                    default=None)
parser.add_argument("-o","--outpath",
                    help='Path to position/ directory in which to dump output.\
                          Default: "{}/trajs_EQ_poly_SS_2D_N{{N:d}}_T{{T:.4f}}/Cnf-{{Cnf:d}}"'.format(host_info.TwoTimeTrajs),
                    default=None)
parser.add_argument("-u","--update",action='store_true',
                    help="This option sets a flag to only run code if {outpath} doesn't already contain dumped frames\
                          with the same set of filenames as running the code would generate.")
args = parser.parse_args()

# parse input

T = args.temperature
N = args.num_particles
Cnf = args.Cnf

path_to_print_steps = os.path.abspath(os.path.expanduser(args.path_to_print_steps))
print_steps = np.loadtxt(path_to_print_steps,dtype=int)
assert print_steps[0] > 0                     # catch common mistakes
assert print_steps[-1] == print_steps[-2]+1   # in print file

steps = np.concatenate(([0],print_steps[:-1]))
times = args.dt*steps

if args.inpath is None:
    inpath = "{}/configs_EQ_poly_SS_2D_N{:d}_T{:.4f}".format(host_info.UnquenchedConfigs,N,T)
else:
    inpath = os.path.abspath(os.path.expanduser(args.inpath))
cnffile = "{}/Cnf-{:d}".format(inpath,Cnf)

if args.outpath is None:
    outpath = "{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}/Cnf-{:d}".format(host_info.TwoTimeTrajs,N,T,Cnf)
else:
    outpath = os.path.abspath(os.path.expanduser(args.outpath))

# determine whether to clean out {outpath}/position #

if args.update and os.path.isdir("{}/position".format(outpath)):
    preexisting = np.sort([int(framefile[:-4]) for framefile in os.listdir("{}/position".format(outpath)) if framefile[-4:]==".npy"])
    if np.all(np.isin(steps,preexisting)): # allow for extra files in preexisting -- don't run if all files in steps are in preexisting
        for step in steps:
            try: # see if every npy file that would be produced exists and can be read without error (in case of corruption)
                np.load("{}/position/{:d}.npy".format(outpath,step))
            except:
                break
        else: # if for loop above is never broken out of
            sys.exit("Exiting: Update flag set and output already exists.")

if os.path.isdir("{}/position".format(outpath)):
    rmtree("{}/position".format(outpath)) # remove output folder if already exists
pathlib.Path("{}/position".format(outpath)).mkdir(parents=True,exist_ok=True)

##########################

###### store info in sys_params.npz file ######

config_data = read_config_file(cnffile)
d,L,diams = 2,config_data['L'],config_data['diams']
np.savez('{}/sys_params.npz'.format(outpath),d=d,N=N,L=L,step=steps,time=times,diameter=diams)

###############################################

init_config_from_data(outpath,args.seed,T=T,**read_config_file(cnffile)) # create init_config.txt
in_swap_from_data(outpath,"position",path_to_print_steps,dt=args.dt,max_t=times[-1],T=T) # create in.swap

with open("{}/in.swap".format(outpath)) as f:
    subprocess.call(shlex.split("{} -log none -screen none".format(host_info.lammps)),stdin=f,cwd=outpath) # suppress LAMMPS output
    #subprocess.call(host_info.lammps,stdin=f,cwd=outpath) # print LAMMPS output

convert_xyz_to_npy(outpath,"position",path_to_print_steps)
