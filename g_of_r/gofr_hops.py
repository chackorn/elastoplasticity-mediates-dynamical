"""
gofr_hops.py: pair distribution of events
t0=0,t1=Dt (default 10), rij determined separately at times t0 and t1
(except for strain, which is determined at time t0),
time-symmetric neighbourhoods (TS for ``time symmetric'') when calculating dp_com
and the local displacement gradient
and time-symmetric local displacement gradient
(average of forward grad u and reversed - grad u)
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

import numpy as np
from numba import njit

from swap_tools import PBC,get_xi_dyn,get_potential_energy,get_stress,rotation_matrix,FixNumHops
from numba_tools import get_dp_com,mask_to_indices,parse_brackets,update_hist
from nlist_tools import get_nlist,merge_nlists,shrink_nhood
from nlist_tools import calc_E as calc_E_unnorm
from nlist_tools import calc_E_normed

##### function defs #####

def r_and_th_inds(r,theta,r2edges,thedges):
    r2 = np.sum(r**2,axis=1)
    Rot = rotation_matrix(-theta)
    
    mask = (r2edges[0]<r2)*(r2<r2edges[-1])
    r,r2 = r[mask],r2[mask]
    
    r = np.abs(np.einsum('ij,...j',Rot,r)) # Rot: rotate so ext axis horizontal; abs: map to upper right quadrant
    th = np.arctan2(r[:,1],r[:,0])  # post-rotation angle w.r.t. ext axis
    
    return (np.searchsorted(r2edges,r2,side='right')-1,np.searchsorted(thedges,th,side='right')-1)

#########################

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-r","--r_string",
                   help="Array of r bin edges.\
                         Input [r0,r1,num_rs] corresponds to array ayes = np.logspace(r0,r1,num_rs).\
                         Default: None (np.logspace(log10(0.5),log10(40),51))",
                   default=None)
parser.add_argument("-t","--Dt_i",type=int,
                   help="Rearrangement interval Dt in MD simulation frame units.\
                         Default: 10000",
                   default=10000)
parser.add_argument("-c","--criterion",type=float,
                    help="A hop has occurred if (dr/rmsd_plat)**2 > criterion.\
                          Default: 5.",
                    default=5.)
parser.add_argument("-R","--cage_radius",type=float,
                    help="Calculate displacements relative to the motion of the centre of mass of particles within a distance R\
                          from each particle (excluding the particle).\
                          Default: None (get cage_radius from get_xi_dyn)",
                    default=None)
parser.add_argument("-F","--FL_radius",type=float,
                    help="Calculate best-fit displacement gradient tensor for particles within distance FL_radius\
                          from each particle (excluding the particle).\
                          Default: 3.",
                    default=3.)
parser.add_argument("-a","--num_th",type=int,
                   help="Bin half-circle into num_th bins.\
                         Default: 12",
                   default=12)
parser.add_argument("-u","--unidirectional",action='store_true',
                    help="This option sets a flag to only consider events in one direction in time.")
parser.add_argument("-d","--disjoint",action='store_true',
                    help="This option sets a flag to use pairs of events across disjoint intervals (t0,t1) and (t1,t2).")
parser.add_argument("-p","--post",action='store_true',
                    help="This option sets a flag to use relative positions at t1 instead of t1.")
parser.add_argument("-b","--backwards",action='store_true',
                    help="This option sets a flag to use t0=Dt and t1=0 instead of the converse.")
parser.add_argument("-z","--norm",action='store_true',
                    help="This option sets a flag to use calc_E_normed with density=False instead of calc_E.")
parser.add_argument("-Z","--normdense",action='store_true',
                    help="This option sets a flag to use calc_E_normed with density=True instead of calc_E.")
parser.add_argument("-x","--fixnumhops",type=float,
                    help="Temperature such that the number of hops is constrained to being less than\
                          that found for that temperature.\
                          Default: Don't fix the number of hops.",
                    default=None)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-V","--maxnumVs",type=int,
                    help='Maximum number of isoconfigurational trajectories per configuration to average over.\
                          Default: Use all available isoconfigurational trajectories.',
                    default=None)
parser.add_argument("-s","--suffix",
                    help='Custom suffix to file name.\
                          Default: None (no extra suffix).',
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

T = args.temperature
N = args.num_particles
Dt_i = args.Dt_i
crit = args.criterion
R_c = get_xi_dyn(T) if args.cage_radius is None else args.cage_radius
R_D = args.FL_radius
num_th = args.num_th
T_fix,fixflag = args.fixnumhops,(args.fixnumhops is not None)
maxnumVs = args.maxnumVs

prefix = ""
if args.normdense:
    prefix += "normdense_"
elif args.norm:
    prefix += "norm_"
if args.unidirectional:
    prefix += "unidir_"
if args.disjoint:
    prefix += "disjoint_"
if args.post:
    prefix += "post_"
if args.backwards:
    prefix += "reversed_" # I avoid calling it args.reversed because seq.reversed() is a standard python function
suffix = ""
if fixflag:
    suffix += "_x{:.4f}".format(T_fix)
if args.suffix is not None:
    suffix += "_{}".format(args.suffix)
argstring = "{}T{:.4f}_N{:d}_t{:d}_c{:.3g}_R{:.3g}_F{:.3g}_a{:d}{}".format(prefix,T,N,Dt_i,crit,R_c,R_D,num_th,suffix)

platpath = '{}/T{:.4f}/N{:d}/pc_plateau.npz'.format(host_info.TwoTimeOut,T,N)
inpath = '{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.TwoTimeTrajs,N,T)
outpath = '{}/T{:.4f}/N{:d}/g_hop_{}.npz'.format(host_info.TwoTimeOut,T,N,argstring)
platdata = np.load(platpath,allow_pickle=True)

rarg0,rarg1,rarg2 = [np.log10(0.5),np.log10(40.),51] if args.r_string is None else parse_brackets(args.r_string,[float,float,int])
redges=np.logspace(rarg0,rarg1,rarg2); r2edges=redges**2

thedges,dth = np.linspace(0,.5*np.pi,num_th+1,endpoint=True,retstep=True) # gradient and flow dirns are indistinguishable

if args.normdense:
    def calc_E(p0,dp,L,nlist,nlist_inds):
        return calc_E_normed(p0,dp,L,nlist,nlist_inds,density=True)
elif args.norm:
    def calc_E(p0,dp,L,nlist,nlist_inds):
        return calc_E_normed(p0,dp,L,nlist,nlist_inds,density=False)
else:
    calc_E = calc_E_unnorm

##########################

hist = np.zeros((redges.size-1,num_th),dtype=int)
num_hops = 0
numTrajs = 0

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

if fixflag:
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnfs[0])) if Vdir[:2]=='V-'])
    if maxnumVs is not None:
        Vs = Vs[:maxnumVs]
    if fixflag:
        fixer = FixNumHops(T_fix,T,N,Dt_i,crit,num_cnfs=cnfs.size,num_Vs=Vs.size)

for cnf_i,cnf in enumerate(cnfs):
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnf)) if Vdir[:2]=='V-'])
    if fixflag:
        Vs = Vs[:fixer.num_Vs(cnf_i)]
    elif maxnumVs is not None:
        Vs = Vs[:maxnumVs]

    for V in Vs:
        numTrajs += 1
        if not args.unidirectional:
            numTrajs += 1 # add numTrajs from reversed time direction

        Vpath = '{}/Cnf-{:d}/V-{:d}'.format(inpath,cnf,V)
        sys_params = np.load('{}/sys_params.npz'.format(Vpath),allow_pickle=True)
        d,L,diams = int(sys_params['d']),float(sys_params['L']),sys_params['diameter']
        assert d==2

        types,num_species = np.searchsorted(platdata['dedges'][:-1],diams)-1,platdata['dedges'].size-1
        plateaus = np.zeros(N)
        for i in range(platdata['dedges'].size-1):
            plateaus[types==i] = platdata['p_c'][i]
        pc2 = plateaus**2

        p0 = PBC(np.load('{}/quenched_position/0.npy'.format(Vpath)).astype(float),L)
        p1 = PBC(np.load('{}/quenched_position/{:d}.npy'.format(Vpath,Dt_i)).astype(float),L)
        if args.disjoint:
            p2 = PBC(np.load('{}/quenched_position/{:d}.npy'.format(Vpath,2*Dt_i)).astype(float),L)
        if args.backwards:
            if not args.disjoint:
                p0,p1 = p1,p0
            else:
                p0,p2 = p2,p0

        # prep. cage-relative stuff #
        nlist_c0,nlist_inds_c0 = get_nlist(p0,L,R_c)
        nlist_c1,nlist_inds_c1 = get_nlist(p1,L,R_c)
        nlist_c,nlist_inds_c = merge_nlists(nlist_c0,nlist_inds_c0,nlist_c1,nlist_inds_c1)
        if args.disjoint:
            nlist_c2,nlist_inds_c2 = get_nlist(p2,L,R_c)
            nlist_c_prime,nlist_inds_c_prime = merge_nlists(nlist_c1,nlist_inds_c1,nlist_c2,nlist_inds_c2)

        nlist_D0,nlist_inds_D0 = get_nlist(p0,L,R_D)
        nlist_D1,nlist_inds_D1 = get_nlist(p1,L,R_D)
        nlist_D,nlist_inds_D = merge_nlists(nlist_D0,nlist_inds_D0,nlist_D1,nlist_inds_D1)

        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)
        if args.disjoint:
            dp_prime = PBC(p2-p1,L); dp_prime -= get_dp_com(dp_prime,nlist_c_prime,nlist_inds_c_prime)

        hopmask = np.sum(dp**2,axis=-1)/pc2 > crit
        if args.disjoint:
            hopmask_prime = np.sum(dp_prime**2,axis=-1)/pc2 > crit
        if not np.any(hopmask):
            continue

        hoppers = mask_to_indices(hopmask)
        num_hops += hoppers.size
        if not args.unidirectional:
            num_hops += hoppers.size # add num_hops from reversed time direction

        # calc. strain (and invariants thereof) #
        F = .5*(calc_E(p0,dp,L,nlist_D,nlist_inds_D)-calc_E(p1,-dp,L,nlist_D,nlist_inds_D)) # deformation gradient tensor F,
        E = .5*(F+np.swapaxes(F,1,2)) # linear strain tensor E                              # antisym w.r.t. time reversal
        
        E_trace = np.trace(E,axis1=1,axis2=2)
        E_detraced = E-np.multiply.outer(E_trace,np.identity(d))/d
        
        iso_strain = E_trace
        dev_strain = np.sqrt(2)*np.linalg.norm(E_detraced,axis=(1,2))
        
        lambda_plus = .5*(iso_strain[hopmask]+dev_strain[hopmask]) # largest eigenvalue
        thetas = np.arctan((lambda_plus-E[hopmask,0,0])/E[hopmask,0,1]) # angle of eigvec corresp. largest eigval

        for i,theta in zip(hoppers,thetas):
            jmask = np.copy(hopmask) if not args.disjoint else np.copy(hopmask_prime)
            jmask[i] = False

            dr0,dr1 = PBC(p0[jmask]-p0[i].reshape((1,d)),L),PBC(p1[jmask]-p1[i].reshape((1,d)),L)
            if args.post:
                dr0,dr1 = dr1,dr0

            r_eyes,th_eyes = r_and_th_inds(dr0,theta,r2edges,thedges)
            update_hist(r_eyes,th_eyes,hist)

            # time-reversed events
            if not args.unidirectional:
                theta += .5*np.pi
                r_eyes,th_eyes = r_and_th_inds(dr1,theta,r2edges,thedges)
                update_hist(r_eyes,th_eyes,hist)

    print("{:d}/{:d}".format(cnf,cnfs[-1]),flush=True)

# normalised density of rearrangements at (r,theta) given rearrangement at origin in same rearrangement time interval
dV = .5*(r2edges[1:]-r2edges[:-1])*dth # pi r^2 * (dth/(2 pi))
gofr = .25*hist/(num_hops*dV.reshape(-1,1)) # mean density of hops (as fn of (r,theta))
gofr /= num_hops/(numTrajs*L**d) # normalise by the uniform density case

np.savez(outpath,redges=redges,thedges=thedges,gofr=gofr,hist=hist,numhops=num_hops,numTrajs=numTrajs,metadata=metadata)
