"""
hopsperrun.py: number of hops per time interval of length Dt
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

import numpy as np
from numba import njit

from swap_tools import PBC,get_xi_dyn,FixNumHops
from numba_tools import get_dp_com,parse_brackets
from nlist_tools import get_nlist_merged

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-t","--Dt_i",type=int,
                   help="Rearrangement interval Dt in MD simulation frame units.\
                         Default: 1000",
                   default=1000)
parser.add_argument("-c","--criterion",type=float,
                    help="A hop has occurred if (dr/rmsd_plat)**2 > criterion.\
                          Default: 5.",
                    default=5.)
parser.add_argument("-R","--cage_radius",type=float,
                    help="Calculate displacements relative to the motion of the centre of mass of particles within a distance R\
                          from each particle (excluding the particle).\
                          Default: None (get cage_radius from get_xi_dyn)",
                    default=None)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-V","--maxnumVs",type=int,
                    help='Maximum number of isoconfigurational trajectories per configuration to average over.\
                          Default: Use all available isoconfigurational trajectories.',
                    default=None)
parser.add_argument("-x","--fixnhops",type=float,
                    help="Temperature such that the number of hops is constrained to being less than\
                          that found for that temperature.\
                          Default: Don't fix the number of hops.",
                    default=None)
parser.add_argument("-s","--suffix",
                    help='Custom suffix to file name.\
                          Default: None (no extra suffix).',
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

T = args.temperature
N = args.num_particles
Dt_i = args.Dt_i
crit = args.criterion
R = get_xi_dyn(T) if args.cage_radius is None else args.cage_radius
maxnumVs = args.maxnumVs
T_fix,fixflag = args.fixnhops,(args.fixnhops is not None)

suffix = "_x{:.4f}".format(T_fix) if fixflag else ""
if args.suffix is not None:
    suffix += "_{}".format(args.suffix)
argstring = "T{:.4f}_N{:d}_t{:d}_c{:.3g}_R{:.3g}{}".format(T,N,Dt_i,crit,R,suffix)

platpath = '{}/T{:.4f}/N{:d}/pc_plateau.npz'.format(host_info.TwoTimeOut,T,N)
inpath = '{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.TwoTimeTrajs,N,T)
outpath = '{}/T{:.4f}/N{:d}/hopsperrun_{}.npz'.format(host_info.TwoTimeOut,T,N,argstring)
platdata = np.load(platpath,allow_pickle=True)

##########################

nhops = 0
numruns = 0

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

if fixflag:
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnfs[0])) if Vdir[:2]=='V-'])
    if maxnumVs is not None:
        Vs = Vs[:maxnumVs]
    fixer = FixNumHops(T_fix,T,N,Dt_i,crit,num_cnfs=cnfs.size,num_Vs=Vs.size)

for cnf_i,cnf in enumerate(cnfs):
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnf)) if Vdir[:2]=='V-'])
    if fixflag:
        Vs = Vs[:fixer.num_Vs(cnf_i)]
    elif maxnumVs is not None:
        Vs = Vs[:maxnumVs]

    numruns += Vs.size
    for V in Vs:
        Vpath = '{}/Cnf-{:d}/V-{:d}'.format(inpath,cnf,V)
        sys_params = np.load('{}/sys_params.npz'.format(Vpath),allow_pickle=True)
        d,L,diams = int(sys_params['d']),float(sys_params['L']),sys_params['diameter']
        assert d==2

        types,num_species = np.searchsorted(platdata['dedges'][:-1],diams)-1,platdata['dedges'].size-1
        plateaus = np.zeros(N)
        for i in range(platdata['dedges'].size-1):
            plateaus[types==i] = platdata['p_c'][i]
        pc2 = plateaus**2

        p0 = PBC(np.load('{}/quenched_position/0.npy'.format(Vpath)).astype(float),L)
        p1 = PBC(np.load('{}/quenched_position/{:d}.npy'.format(Vpath,Dt_i)).astype(float),L)

        nlist_c,nlist_inds_c = get_nlist_merged(p0,p1,L,R)

        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)

        nhops += np.sum(np.sum(dp**2,axis=-1)/pc2 > crit)

    print("{:d}/{:d}".format(cnf,cnfs[-1]),flush=True)

hopsperrun = float(nhops)/numruns
print(nhops,numruns)
print(hopsperrun)

np.savez(outpath,hopsperrun=hopsperrun,nhops=nhops,ntrajs=ntrajs,metadata=metadata)
