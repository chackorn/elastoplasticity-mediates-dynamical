"""
gofr.py: given a particle at the origin at time 0,
g(r) is the density of particles at r at time 0 (averaged over particles and ensembles),
non-dimensionalised by the particle density in the uniformly distributed case.
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

import numpy as np
from numba import njit

from numba_tools import parse_brackets
from swap_tools import get_dminmax,read_config_file

##### function defs #####

@njit
def update_hist(p,types,L,r2edges,hist):
    N,d = p.shape
    num_edges = r2edges.size
    for i in range(N-1):
        type_i = types[i]
        for j in range(i+1,N):
            type_j = types[j]

            dr2 = 0.
            for m in range(d):
                r_m = p[j,m]-p[i,m]
                if r_m < -L/2.:
                    r_m += L
                elif r_m >= L/2.:
                    r_m -= L
                dr2 += r_m*r_m

            if r2edges[0]<dr2 and dr2<r2edges[num_edges-1]:
                r_i = np.searchsorted(r2edges,dr2)-1
                hist[r_i,type_i] += 1
                hist[r_i,type_j] += 1
    return

@njit
def update_hist_types(types,hist):
    for type_i in types:
        hist[type_i] += 1
    return

#########################

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-r","--r_string",
                   help="Array of r bin edges.\
                         Input [r0,r1,num_rs] corresponds to array redges = np.linspace(r0,r1,num_rs,endpoint=True).\
                         Default: '[0.,10.,201]'",
                   default='[0.,10.,201]')
parser.add_argument("-b","--num_bins",type=int,
                    help="Number of bins into which to separate particle diameters.\
                          Default: 10",
                    default=10)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations.',
                    default=None)
args = parser.parse_args()

T = args.temperature
N = args.num_particles
redges = np.linspace(*parse_brackets(args.r_string,[float,float,int]),endpoint=True); r2edges=redges**2
dedges = np.logspace(*[np.log10(dm) for dm in get_dminmax()],args.num_bins+1)

inpath = '{}/configs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.UnquenchedConfigs,N,T)
outpath = '{}/T{:.4f}/N{:d}/gofr.npz'.format(host_info.TwoTimeOut,T,N)

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

##########################

N,d,L = None,None,None

hist_types = np.zeros(dedges.size-1,dtype=int) # num particles in diameter bin
hist = np.zeros((redges.size-1,dedges.size-1),dtype=int)

for cnf in cnfs:
    data = read_config_file('{}/Cnf-{:d}'.format(inpath,cnf))
    p,diams = data['ps'],data['diams']

    types = np.searchsorted(dedges[:-1],diams,side='right')-1
    update_hist_types(types,hist_types)

    if N is None:
        N,d = p.shape
        L = data['L']
    update_hist(p,types,L,r2edges,hist)

    print("{:d}/{:d}".format(cnf,cnfs[-1]),flush=True)

dV = np.pi*(r2edges[1:]-r2edges[:-1]) # area (2D volume) per r bin

gofr = np.sum(hist,axis=1)/(np.sum(hist_types)*dV) # mean density of neighbours (as fn of r)
gofr /= N/L**d # normalise by the uniform density case

gofr_binned = hist/np.outer(dV,hist_types)
gofr_binned /= N/L**d

np.savez(outpath,redges=redges,dedges=dedges,gofr=gofr,gofr_binned=gofr_binned,hist=hist,hist_types=hist_types,metadata=metadata)
