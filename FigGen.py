import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)

import numpy as np
from numba import njit
import pandas as pd

import string

from swap_tools import get_xi_dyn

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm

plt.rcParams.update({'text.usetex': True})
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{bm}']
subadj_default = {'left':0.,'right':1.,'bottom':0.,'top':1.}

def default_tick_params(axis,cbar=False):
    if not cbar:
        axis.tick_params(axis='both',which='both',direction='in',
                        left=True,right=True,bottom=True,top=True,
                        labelleft=True,labelsize=14)
    else:
        axis.tick_params(axis='both',which='both',direction='in',
                         left=False,right=False,bottom=False,top=False,
                         labelleft=False,labelright=True,labelsize=14)
    return

def ax_to_label(x,label,fmt='.3g'):
    return ['{}={{:{}}}'.format(label,fmt).format(x_i) for x_i in x]

figpath = '/home1/chackorn/Documents/EshelbyPaper/figs'
datpath = '/home1/chackorn/Documents/EshelbyPaper/data'

##################################

def fig1(fname):
    """
    fig:strain_polar
    """
    
    # function definitions #
    
    def wrap_z(z):
        return np.hstack((z,z[:,::-1],z,z[:,::-1],z[:,0].reshape(-1,1)))
    
    def get_data(T,qstring,nslices=None):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{T:.4f}_N10000_t10000_c5_R{R:.3g}_F3_a12'.format(T=T,R=get_xi_dyn(T))
        
        if nslices is None:
            if T>.1:
                suffix += '_x0.1000'
            data = np.load("{}/perturbed_fields_{}.npz".format(rootpath,suffix),allow_pickle=True)
            
            thedges = np.hstack([data['thedges'][:-1]+ang for ang in .5*np.pi*np.arange(4)]+[2.*np.pi])
            thmids = .5*(thedges[1:]+thedges[:-1])
            rmids = .5*(data['redges'][1:]+data['redges'][:-1])
            
            q = data[qstring]
        else:
            for slice_i in range(nslices):
                data = np.load("{}/perturbed_fields_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if slice_i==0:
                    thedges = np.hstack([data['thedges'][:-1]+ang for ang in .5*np.pi*np.arange(4)]+[2.*np.pi])
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    
                    hist = data['hist0']
                    q = data['hist0']*np.nan_to_num(data[qstring])
                else:
                    hist += data['hist0']
                    q += data['hist0']*np.nan_to_num(data[qstring])
            q /= hist.astype(q.dtype)
            
        return thedges,rmids,wrap_z(q)
    
    def plot_z(i,j,th,r,z,cmap,norm):
        ax[i,j].pcolormesh(th,np.log10(r),z,cmap=cmap,norm=norm)
        
        ax[i,j].set_ylim([np.log10(.3),np.log10(30)])
        ax[i,j].set_yticks(np.log10([.3,1,3,10,30]))
        ax[i,j].set_yticklabels(['',r'$1$',r'$3$',r'$10$',r'$30$'])
        
        ax[i,j].set_xticklabels([])
        
        plt.sca(ax[i,j]); plt.grid(True)
        default_tick_params(ax[i,j])
        return ax[i,j]
    
    def get_cbar(i,j,norm,cmap,ticks,label):
        x0,dx = (j*bw + j*wspace*bw + .5*ncbw)/fig_w,cbw/fig_w
        y0,dy = 1. - ((i+1)*bh + i*hspace*bh)/fig_h - .017,cbh/fig_h
        cax = fig.add_axes([x0,y0,dx,dy])
        cbar = mpl.colorbar.ColorbarBase(cax,norm=norm,cmap=cmap,orientation='horizontal',ticks=ticks)
        cbar.minorticks_off(); cbar.set_ticks(ticks)
        cbar.set_ticklabels([r'${:.3g}$'.format(tick) for tick in ticks])
        cax.text(.5,-2,label,transform=cax.transAxes,ha='center',va='top',fontsize=16)
        default_tick_params(cax,cbar=True)
        
        return cax
    
    def label_coords(axis):
        axis.annotate('',xy=(1.,1.05),xytext=(0.5,1.05),xycoords='axes fraction',textcoords='axes fraction',
                      arrowprops=dict(arrowstyle="->"))
        axis.text(1.,1.1,r'$\mathrm{extension}$',transform=axis.transAxes,ha='right',va='bottom',fontsize=16)
        axis.annotate('',xy=(-.05,1.),xytext=(-.05,.5),xycoords='axes fraction',textcoords='axes fraction',
                      arrowprops=dict(arrowstyle="->"))
        axis.text(-.1,1.,r'$\mathrm{compression}$',transform=axis.transAxes,ha='right',va='top',fontsize=16,rotation=90)
        return
    
    ########################

    bw,bh,cbw,cbh = 2,2,1.7,.1; ncbw = (bw-cbw)
    ncols,nrows = 2,3
    wspace,hspace=0.13,.33

    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows,subplot_kw={'projection':'polar'})
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)

    for j,T in enumerate([.1,.15]):
        nslices = 10 if T==.1 else 101
        
        """
        iso
        """

        cmap = cm.get_cmap('bwr')
        norm = mpl.colors.Normalize(vmin=-.015,vmax=.015)
        
        th,r,z = get_data(T,'iso',nslices=nslices)        
        plot_z(0,j,th,r,z,cmap,norm)
        
        ticks,label = [-.01,0,.01],r'$\gamma_\mathrm{iso}$'
        get_cbar(0,j,norm,cmap,ticks,label)
        
        figstr = '1a' if j==0 else '1b'
        zframe = pd.DataFrame(z,index=ax_to_label(r,'r'),columns=ax_to_label(th,'theta',fmt='.3f'))
        np.savez('{}/FigData/fig1/{}.npz'.format(datpath,figstr),
                 theta=th,r=r,isostrain=zframe)
        zframe.to_pickle('{}/FigData/fig1/{}.pkl'.format(datpath,figstr))
        
        
        """
        dev
        """
        
        cmap = cm.get_cmap('magma')
        
        if j==0:
            vmin,vmax = 3e-3,3e-1
            ticks = [.003,.03,0.3]
        if j==1:
            vmin,vmax = 1e-1,3e-1
            ticks = [.1,.2,.3]
            
        norm = mpl.colors.LogNorm(vmin=vmin,vmax=vmax)
        
        th,r,z = get_data(T,'dev')
        plot_z(1,j,th,r,z,cmap,norm)
        get_cbar(1,j,norm,cmap,ticks,r'$\gamma_\mathrm{dev}$')
        
        figstr = '1c' if j==0 else '1d'
        zframe = pd.DataFrame(z,index=ax_to_label(r,'r'),columns=ax_to_label(th,'theta',fmt='.3f'))
        np.savez('{}/FigData/fig1/{}.npz'.format(datpath,figstr),
                 theta=th,r=r,devstrain=zframe)
        zframe.to_pickle('{}/FigData/fig1/{}.pkl'.format(datpath,figstr))

        """
        exx
        """
        
        cmap = cm.get_cmap('bwr')
        
        if j==0:
            vmin,vmax = -.03,.03
            ticks = [-.03,0.,.03]
        if j==1:
            vmin,vmax = -.06,.06
            ticks = [-.06,0.,.06]
            
        norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
        
        th,r,z = get_data(T,'exx')
        plot_z(2,j,th,r,z,cmap,norm)
        get_cbar(2,j,norm,cmap,ticks,r'$\gamma_\mathrm{ext}$')
        
        figstr = '1e' if j==0 else '1f'
        zframe = pd.DataFrame(z,index=ax_to_label(r,'r'),columns=ax_to_label(th,'theta',fmt='.3f'))
        np.savez('{}/FigData/fig1/{}.npz'.format(datpath,figstr),
                 theta=th,r=r,extstrain=zframe)
        zframe.to_pickle('{}/FigData/fig1/{}.pkl'.format(datpath,figstr))
    
    label_coords(ax[0,0])
    
    for axis,char in zip(ax.reshape(-1),string.ascii_lowercase):
        axis.text(0.,1.,r'$(\mathrm{{{}}})$'.format(char),
                  transform=axis.transAxes,ha='left',va='top',fontsize=16)
    
    plt.savefig(fname,bbox_inches='tight')
        
    return

def fig2(fname):
    """
    fig:strain_decay
    """
    
    # function definitions #
    
    def get_data(T,nslices=None):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{:.4f}_N10000_t10000_c5_R{:.3g}_F3_k12'.format(T,get_xi_dyn(T))
        
        if nslices is None:
            if T > .1:
                suffix += '_x0.1000'
            data = np.load("{}/perturbed_fields_Fourier_{}.npz".format(rootpath,suffix),allow_pickle=True)
            rmids = .5*(data['redges'][1:]+data['redges'][:-1])
            g_tilde = data['g_tilde']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/perturbed_fields_Fourier_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    rmids = .5*(data['redges'][1:]+data['redges'][:-1])
                    hist = data['hist0']
                    iso,dev,exx = [np.nan_to_num(data[qstr])*data['hist0'].reshape((1,-1)) for qstr in ['iso','dev','exx']]
                else:
                    hist += data['hist0']
                    for q,qstr in [(iso,'iso'),(dev,'dev'),(exx,'exx')]:
                        q += np.nan_to_num(data[qstr])*data['hist0'].reshape((1,-1))
                    
        anmask = hist>0; nanmask = np.invert(anmask)
        for q in [iso,dev,exx]:
            q[:,anmask] /= hist[anmask].astype(q.dtype).reshape((1,-1))
            q[:,nanmask] = np.nan
            q[1:] *= 2. # convert complex Fourier coeffs c_k to real-space Fourier coeff c_k + c_{-k} for k>0
        
        return rmids,iso,dev,exx,anmask
    
    def get_segments(y):
        assert not np.any(np.isnan(y))

        sgns = y>=0.
        i_change = np.insert(np.arange(1,y.size)[np.invert(sgns[:-1]*sgns[1:])],0,0)
        sgns = 2*(sgns.astype(int))-1

        ret = []
        for i in range(len(i_change)-1):
            x0,x1 = i_change[i],i_change[i+1]+1
            sgn = sgns[x0]
            ret.append([x0,x1,sgn])
        ret.append([i_change[-1],y.size,sgns[-1]])

        return ret
    
    def loglog_signed(axis,x,y,c):
        linestyles = [':','-']
        
        for segment in get_segments(y):
            x0,x1,sgn = segment
            sgn_i = (1+sgn)//2
            axis.loglog(x[x0:x1],np.abs(y[x0:x1]),c=c,ls=linestyles[sgn_i])
            
        return axis
    
    ########################
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15) # colour based on T
    
    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])[::-1]
    
    bw,bh = 2,1.6
    ncols,nrows=2,2
    wspace,hspace=0.,0.

    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*wspace)*bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)
    
    z_2a,z_2b,z_2c,z_2d = (None for i in range(4))
    for T_i,T in enumerate(Ts):
        nslices = 101
        if T==.1:
            nslices = 10
        elif T==.105:
            nslices=100
        rmids,iso,dev,exx,anmask = get_data(T,nslices=nslices)
        
        # g_iso
        loglog_signed(ax[0,0],rmids[anmask],iso[0,anmask],cmap(norm(T)))
        loglog_signed(ax[1,0],rmids[anmask],iso[1,anmask],cmap(norm(T))) # c_k is complex Fourier coeff, factor 2 for real coeff
        
        # g_dev
        loglog_signed(ax[0,1],rmids[anmask],dev[0,anmask],cmap(norm(T)))
        
        # g_ext
        loglog_signed(ax[1,1],rmids[anmask],exx[2,anmask],cmap(norm(T)))
        
        if z_2a is None:
            z_2a,z_2b,z_2c,z_2d = (np.zeros((Ts.size,rmids.size)) for i in range(4))
        z_2a[T_i,:] = iso[0]
        z_2b[T_i,:] = dev[0]
        z_2c[T_i,:] = iso[1]
        z_2d[T_i,:] = exx[2]    
    
    for ix,(axis,char) in enumerate(zip(ax.reshape(-1),string.ascii_lowercase)):
        axis.text(.98,.95,r'$(\mathrm{{{}}})$'.format(char),transform=axis.transAxes,ha='right',va='top',fontsize=16)
        default_tick_params(axis)
        axis.set_xlim([8e-1,4e1])
        
        i = ix//2
        j = ix-2*i
        
        if j==1:
            axis.tick_params(labelleft=False,labelright=True)
        if i==0:
            axis.tick_params(labelbottom=False)
        else:
            axis.text(.36,-.01,r'$r$',transform=axis.transAxes,ha='center',va='top',fontsize=16)
    
    ax[0,0].semilogx(rmids[6:-12],6e-2*rmids[6:-12]**-3,'--k')
    ax[0,0].set_ylim([1e-7,2e-2])
    ax[0,0].text(-.05,.57,r'$\hat{\gamma}_{\mathrm{iso},0}$',transform=ax[0,0].transAxes,ha='right',va='center',fontsize=16,rotation=90)

    ax[0,1].loglog(rmids[18:-11],7e-1*rmids[18:-11]**-2,'--k')
    ax[0,1].set_ylim([3e-3,3e-1])
    ax[0,1].text(1.05,.51,r'$\hat{\gamma}_{\mathrm{dev},0}$',transform=ax[0,1].transAxes,ha='left',va='center',fontsize=16,rotation=90)
    
    ax[1,0].loglog(rmids[10:-10],8e-2*rmids[10:-10]**-2,'--k')
    ax[1,0].set_ylim([6e-7,2e-2])
    ax[1,0].text(-.05,.51,r'$\hat{\gamma}_{\mathrm{iso},2}$',transform=ax[1,0].transAxes,ha='right',va='center',fontsize=16,rotation=90)
    
    ax[1,1].loglog(rmids[10:-10],6e-1*rmids[10:-10]**-2,'--k')
    ax[1,1].set_ylim([2e-6,4e-2])
    ax[1,1].text(1.05,.62,r'$\hat{\gamma}_{\mathrm{ext},4}$',transform=ax[1,1].transAxes,ha='left',va='center',fontsize=16,rotation=90)
    
    plt.savefig(fname,bbox_inches='tight')
    
    
    
    for q,figstr,qname in zip([z_2a,z_2b,z_2c,z_2d],
                        ['2{}'.format(c) for c in string.ascii_lowercase[:4]],
                             ['gammahat_{}_{:d}'.format(key,k) for (key,k) in [('iso',0),('dev',0),('iso',2),('ext',4)]]):
        zframe = pd.DataFrame(q,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
        outdict = {'T':Ts,'r':rmids,qname:zframe}
        np.savez('{}/FigData/fig2/{}.npz'.format(datpath,figstr),**outdict)
        zframe.to_pickle('{}/FigData/fig2/{}.pkl'.format(datpath,figstr))
            
    return

def fig3(fname):
    """
    fig:ghop_polar
    """
    
    # function definitions #
    
    def get_data(T,nslices=None):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'T{T:.4f}_N10000_t10000_c5_R{R:.3g}_F3_a12'.format(T=T,R=get_xi_dyn(T))
        
        if nslices is None:
            data = np.load("{}/g_hop_normdense_{}.npz".format(rootpath,suffix),allow_pickle=True)
            rmids,r2edges = .5*(data['redges'][1:]+data['redges'][:-1]),data['redges']**2
            dth = data['thedges'][1]-data['thedges'][0]
            hist = data['hist']
            numhops,numTrajs = data['numhops'],2*data['numTrajs'] # fac 2 bc unsliced data from older code which 
                                                                  # forgot to count reversed trajectories
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_normdense_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    hist = data['hist']
                    numhops,numTrajs = data['numhops'],data['numTrajs']
                    rmids,r2edges = .5*(data['redges'][1:]+data['redges'][:-1]),data['redges']**2
                    dth = data['thedges'][1]-data['thedges'][0]
                else:
                    hist += data['hist']
                    numhops += data['numhops']
                    numTrajs += data['numTrajs']

        dV = .5*(r2edges[1:]-r2edges[:-1])*dth # area pi r^2 * (dth/(2 pi)) for disc of radius r
        g_hop = .25*hist/(numhops*dV.reshape(-1,1)) # mean density of hops (as fn of (r,theta))
        g_hop /= numhops/(numTrajs*100.**2) # normalise by the uniform density case
        
        return rmids,np.mean(g_hop,axis=1)
    
    def get_data_binctrl(T,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{T:.4f}_N10000_t10000_c5_R{R:.3g}_F3_a12'.format(T=T,R=get_xi_dyn(T))
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth = data['thedges'][1:]-data['thedges'][:-1]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth = data['thedges'][1:]-data['thedges'][:-1]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
        else:
            gtilde,alpha = np.zeros((1+bootstrap,*hist.shape)),np.zeros((1+bootstrap,rmids.size))
            gtilde[0,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha[0,:] = .5*np.sum((gtilde[0]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde[sample_i,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha[sample_i,:] = .5*np.sum((gtilde[sample_i]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            
        return thmids,rmids,gtilde,alpha

    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    def label_coords(axis):
        axis.annotate('',xy=(1.,-.13),xytext=(0.5,-.13),xycoords='axes fraction',textcoords='axes fraction',
                       arrowprops=dict(arrowstyle="->"))
        axis.text(1.,-.15,r'$\mathrm{extension}$',transform=axis.transAxes,ha='right',va='top',fontsize=16)
        axis.annotate('',xy=(-.05,1.),xytext=(-.05,.5),xycoords='axes fraction',textcoords='axes fraction',
                       arrowprops=dict(arrowstyle="->"))
        axis.text(-.1,1.,r'$\mathrm{compression}$',transform=axis.transAxes,ha='right',va='top',fontsize=16,rotation=90)
        return
    
    ########################
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15) # colour based on T
    
    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])[::-1]
    
    nbssamps = 1 # number of bootstrap sample g_tildes

    bw,bh,bh1 = 2,2,1.6
    ncols,nrows=2,2
    wspace,hspace=.05,-.13
    
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,subplot_kw={'projection':'polar'})
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=0.)
    ax2 = fig.add_axes([0.,-(bh1+hspace*bh)/fig_h,bw/fig_w,bh1/fig_h])
    ax3 = fig.add_axes([(1.+wspace)*bw/fig_w,-(bh1+hspace*bh)/fig_h,bw/fig_w,bh1/fig_h])
    
    alpha,ghop = None,None
    
    for T_i,T in enumerate(Ts):
        if T==.1:
            nslices = 10
        else:
            nslices=None
            
        th,r,g,a = get_data_binctrl(T,nslices=nslices,bootstrap=nbssamps)
        
        if alpha is None:
            alpha = np.zeros((Ts.size,a[0].size))

        ax2.loglog(r,a[0],c=cmap(norm(T))) # plot alpha
        alpha[T_i,:] = a[0]
        
        if T_i == Ts.size-1:
            zframe = pd.DataFrame(alpha,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(r,'r'))
            np.savez('{}/FigData/fig3/{}.npz'.format(datpath,'3c'),
                     T=Ts,r=r,mean_squared_anisotropy=zframe)
            zframe.to_pickle('{}/FigData/fig3/{}.pkl'.format(datpath,'3c'))
        
        if T==.1 or T==.15:
            j=0 if T==.1 else 1
            
            rticks = np.arange(11)
            rtickmask = np.zeros(g.shape[1],dtype=bool)
            for rtick in rticks[1:]:
                # find closest rbin centre to rtick
                r_i = np.searchsorted(r,rtick)
                if np.abs(r[r_i]-rtick) > np.abs(r[r_i-1]-rtick):
                    r_i -= 1
                rtickmask[r_i] = True

                ax[j].plot(th,rtick*g[0,r_i],c=cmap(norm(T)))    
                for sample_i in range(1,1+nbssamps):
                    ax[j].plot(th,rtick*g[sample_i,r_i],c=cmap(norm(T)),alpha=.1)
            
            label = 'a' if j==0 else 'b'
            ax[j].text(0.93,0.92,r'$(\mathrm{{{}}})$'.format(label),transform=ax[j].transAxes,ha='right',va='top',fontsize=16)
            
            dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(g.shape[0])]
            dfnames[0] = 'original data'
            
            zmultix = pd.concat([pd.DataFrame(g[sample_i,rtickmask],
                                              index=ax_to_label(rticks[1:],'r'),
                                              columns=ax_to_label(th,'theta',fmt='.3f'))
                                 for sample_i in range(g.shape[0])],
                                keys=tuple(dfnames))
            np.savez('{}/FigData/fig3/3{}.npz'.format(datpath,label),
                     theta=th,r=rticks[1:],ghop_tilde=zmultix)
            zmultix.to_pickle('{}/FigData/fig3/3{}.pkl'.format(datpath,label))


        if T==.105:
            nslices = 4

        r,g = get_data(T,nslices=nslices)
        if ghop is None:
            ghop = np.zeros((Ts.size,g.size))
        ax3.loglog(r,g,c=cmap(norm(T)))  # plot g_hop(r)
        ghop[T_i,:] = g
        ax3.set_ylim([1e0,1e3])
        
        if T_i == Ts.size-1:
            zframe = pd.DataFrame(ghop,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(r,'r'))
            np.savez('{}/FigData/fig3/{}.npz'.format(datpath,'3d'),
                     T=Ts,r=r,hop_pair_distribution=zframe)
            zframe.to_pickle('{}/FigData/fig3/{}.pkl'.format(datpath,'3d'))
    
    # format axes nicely #
    
    label_coords(ax[0])
    for axis in ax:
        axis.set_thetamin(0)
        axis.set_thetamax(90)
        
        plt.sca(axis); plt.grid(True)
        default_tick_params(axis)
        
        axis.set_xticklabels([])
        yticks = rticks
        yticklabels = [r'${:g}$'.format(r) for r in rticks]
        axis.set_yticks(yticks)
        axis.set_yticklabels(yticklabels)
        axis.set_ylim([0.,11.])

    for axis in [ax2,ax3]:
        axis.set_xlim([.7,40])
        axis.set_xlabel(r'$r$',fontsize=16,labelpad=-14)
        default_tick_params(axis)
        
    ax2.set_ylim([3e-7,1.3e-2])
    ax2.set_yticks([1e-6,1e-5,1e-4,1e-3,1e-2])
    ax2.text(-.12,.45,r'$\alpha$',transform=ax2.transAxes,ha='right',va='center',fontsize=16)    
    ax2.text(0.93,0.92,r'$(\mathrm{c})$',transform=ax2.transAxes,ha='right',va='top',fontsize=16)

    ax3.set_ylim([1e0,1e3])
    ax3.text(1.01,.5,r'$g_\mathrm{hop}$',transform=ax3.transAxes,ha='left',va='center',fontsize=16)
    ax3.text(0.93,0.92,r'$(\mathrm{d})$',transform=ax3.transAxes,ha='right',va='top',fontsize=16)
    ax3.tick_params(labelleft=False,labelright='True')
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def fig4(fname):
    """
    fig:crossover
    """
    
    # function definitions #
    
    def get_data(T,lb=2.5,ub=40,nslices=None):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'T{T:.4f}_N10000_t10000_c5_R{R:.3g}_F3_a12'.format(T=T,R=get_xi_dyn(T))
        
        if nslices is None:
            data = np.load("{}/g_hop_normdense_{}.npz".format(rootpath,suffix),allow_pickle=True)
            rmids,r2edges = .5*(data['redges'][1:]+data['redges'][:-1]),data['redges']**2
            dth,dr,dr2 = [q[1:]-q[:-1] for q in [data['thedges'],data['redges'],data['redges']**2]]
            hist = data['hist']
            numhops,numTrajs = data['numhops'],2*data['numTrajs'] # fac 2 bc unsliced data from older code which 
                                                                  # forgot to count reversed trajectories
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_normdense_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    hist = data['hist']
                    numhops,numTrajs = data['numhops'],data['numTrajs']
                    rmids,r2edges = .5*(data['redges'][1:]+data['redges'][:-1]),data['redges']**2
                    dth,dr,dr2 = [q[1:]-q[:-1] for q in [data['thedges'],data['redges'],data['redges']**2]]
                else:
                    hist += data['hist']
                    numhops += data['numhops']
                    numTrajs += data['numTrajs']

        dV = .5*np.outer(dr2,dth) # area pi r^2 * (dth/(2 pi)) for disc of radius r
        g_hop = .25*hist/(numhops*dV) # mean density of hops (as fn of (r,theta))
        g_hop /= numhops/(numTrajs*100.**2) # normalise by the uniform density case
        g_hop = np.mean(g_hop,axis=1)
        
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        I_hop = 2.*np.pi*np.sum(rmids[lb_i:ub_i]*(g_hop[lb_i:ub_i]-1.)*dr[lb_i:ub_i])
        
        return I_hop
    
    def get_data_binctrl(T,lb=2.5,ub=40,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{T:.4f}_N10000_t10000_c5_R{R:.3g}_F3_a12'.format(T=T,R=get_xi_dyn(T))
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
                    
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
        else:
            I_alpha = np.zeros(1+bootstrap)
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth,axis=1)/np.pi
            I_alpha[0] = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
                I_alpha[sample_i] = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
            
        return I_alpha

    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    def label_coords(axis):
        axis.annotate('',xy=(1.,-.13),xytext=(0.5,-.13),xycoords='axes fraction',textcoords='axes fraction',
                       arrowprops=dict(arrowstyle="->"))
        axis.text(1.,-.15,r'$\mathrm{extension}$',transform=axis.transAxes,ha='right',va='top',fontsize=16)
        axis.annotate('',xy=(-.05,1.),xytext=(-.05,.5),xycoords='axes fraction',textcoords='axes fraction',
                       arrowprops=dict(arrowstyle="->"))
        axis.text(-.1,1.,r'$\mathrm{compression}$',transform=axis.transAxes,ha='right',va='top',fontsize=16,rotation=90)
        return
    
    from swap_tools import T_MCT
    
    ########################
      
    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])
    
    nbssamps = 10 # number of bootstrap sample g_tildes
    I_alpha,I_hop = np.zeros((1+nbssamps,Ts.size)),np.zeros(Ts.size)

    bw,bh = 2,1.6
    ncols=2
    wspace=0.

    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols)
    fig.subplots_adjust(0,0,1,1,wspace=wspace)
    
    for T_i,T in enumerate(Ts):
        nslices = 10 if T==.1 else None
        I_alpha[:,T_i] = get_data_binctrl(T,nslices=nslices,bootstrap=nbssamps)

        if T==.105:
            nslices = 4
        I_hop[T_i] = get_data(T,nslices=nslices)

    ax[0].plot(1./Ts,I_alpha[0],'kx')
    for sample_i in range(1,nbssamps+1):
        ax[0].plot(1./Ts,I_alpha[sample_i],'kx',alpha=.2)
    
    ax[1].semilogy(1./Ts,I_hop,'kx')
    
    # format axes nicely #
    
    for axis,char in zip(ax,string.ascii_lowercase):
        axis.plot(np.repeat(1./T_MCT,10),np.linspace(0.,3000,10),':k')
        axis.set_xlabel(r'$1/T$',fontsize=16)
        axis.text(.15,.85,r'$(\mathrm{{{}}})$'.format(char),transform=axis.transAxes,ha='center',va='center',fontsize=16)
        default_tick_params(axis)

    ax[0].set_ylim([0.,.003])
    ax[0].text(-.1,.5,r'$I_\alpha$',transform=ax[0].transAxes,ha='right',va='center',fontsize=16)

    ax[1].set_ylim([100,3000])
    ax[1].tick_params(labelleft=False,labelright=True)
    ax[1].yaxis.set_label_position('right')
    ax[1].text(1.01,.5,r'$I_\mathrm{hop}$',transform=ax[1].transAxes,ha='left',va='center',fontsize=16)
    
    plt.savefig(fname,bbox_inches='tight')
    
    
    dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(I_alpha.shape[0])]
    dfnames[0] = 'original data'
    zframe = pd.DataFrame(I_alpha,index=dfnames,columns=ax_to_label(1./Ts,'1/T'))
    np.savez('{}/FigData/fig4/{}.npz'.format(datpath,'4a'),T=Ts,I_alpha=zframe)
    zframe.to_pickle('{}/FigData/fig4/{}.pkl'.format(datpath,'4a'))
    
    zframe = pd.Series(data=I_hop,index=ax_to_label(1./Ts,'1/T'))
    np.savez('{}/FigData/fig4/{}.npz'.format(datpath,'4b'),T=Ts,I_hop=zframe)
    zframe.to_pickle('{}/FigData/fig4/{}.pkl'.format(datpath,'4b'))
    
    return

def figS1(fname):
    """
    fig:xi_dyn
    """
    
    # function definitions #
    
    def OrnsteinZernicke_residual(xi,k,S4):
        return np.log(S4/S4[0]) + np.log(1.+(xi*k)**2)

    def get_xi_algo(ks,S4,k0=1.5):
        assert ks[0]==0
        assert S4.size==ks.size

        k_i0 = np.searchsorted(ks,k0)
        k0 = ks[k_i0]

        xi0 = np.sqrt(S4[0]/S4[k_i0] - 1.)/k0
        sol = least_squares(OrnsteinZernicke_residual,xi0,args=(ks[:k_i0],S4[:k_i0]))
        return sol.x[0]

    def get_xi_dyn_algo(T,N=10000,k0=1.5,retta=False):
        rootpath = '{}/T{:.4f}/N{:d}'.format(host_info.TwoTimeOut,T,N)

        data_ta = np.load('{}/ta_vs_R.npz'.format(rootpath),allow_pickle=True)
        data_S4 = np.load('{}/S4kt.npz'.format(rootpath),allow_pickle=True)

        assert data_ta['R'].size==data_S4['R'].size and np.all(data_ta['R']==data_S4['R'])
        Rs,tas,ks = data_ta['R'],data_ta['ta'],data_S4['k']

        ximin,minerr = np.inf,np.inf
        for R_i,(R,ta) in enumerate(zip(Rs,tas)):
            xi = get_xi_algo(ks,data_S4['S4'][R_i],k0)

            err = (R-xi)**2
            if err < minerr:
                minerr,ximin,tamin = err,xi,ta
        if retta:
            return ximin,tamin
        else:
            return ximin

    from scipy.optimize import least_squares
    from swap_tools import T_onset
    
    ########################
    
    Ts = np.array([.11,.115,.12,.13,.14,.15,.2])

    bw,bh = 4,3.5
    ncols = 2
    fig_w,fig_h = ncols*bw,bh

    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=2)
    fig.subplots_adjust(0,0,1,1,hspace=0.,wspace=0.)
    inax0 = fig.add_axes([.57*bw/fig_w,0.07*bh/fig_h,.4*bw/fig_w,.4*bh/fig_h])
    inax1 = fig.add_axes([(1.+.56)*bw/fig_w,0.1*bh/fig_h,.4*bw/fig_w,.4*bh/fig_h])
    
    # xi_dyn and tau_alpha #
    
    R_MWs,tas = np.zeros(Ts[1:].size),np.zeros(Ts[1:].size)
    for T_i,T in enumerate(Ts[1:]):
        R_MWs[T_i],tas[T_i] = get_xi_dyn_algo(T,retta=True)

    x_R = 1./Ts[1:]-1./T_onset         # linfit under constraint
    m_R = x_R@R_MWs / (x_R@x_R)    # that x_intercept
    c_R = -m_R/T_onset             # is 1/T_onset

    m_ta,c_ta = np.polyfit(1./Ts[1:-1],np.log(tas[:-1]),1)

    # xi_dyn
    ax[0].plot(1./Ts[1:],R_MWs,'x')
    ax[0].plot(1./Ts[1:],m_R/Ts[1:]+c_R,'--k')
    ax[0].set_ylabel(r'$\xi_\mathrm{dyn}$',fontsize=16)
    
    zframe = pd.Series(data=R_MWs,index=ax_to_label(1./Ts[1:],'1/T'))
    np.savez('{}/FigData/figS1/S1a.npz'.format(datpath),T=Ts,xi_dyn=zframe,linfit_gradient=m_R,linfit_yintercept=c_R)
    zframe.to_pickle('{}/FigData/figS1/S1a.pkl'.format(datpath))

    # tau_alpha
    ax[1].semilogy(1./Ts[1:],tas,'x')
    ax[1].plot(1./Ts[1:],np.exp(c_ta)*np.exp(m_ta/Ts[1:]),'--k')
    ax[1].set_ylabel(r'$\tau_\alpha$',fontsize=16)
    
    zframe = pd.Series(data=tas,index=ax_to_label(1./Ts[1:],'1/T'))
    np.savez('{}/FigData/figS1/S1b.npz'.format(datpath),T=Ts,tau_alpha=zframe,expfit_prefactor=np.exp(c_ta),expfit_growthrate=m_ta)
    zframe.to_pickle('{}/FigData/figS1/S1b.pkl'.format(datpath))

    for axis,char in zip(ax,string.ascii_lowercase):
        default_tick_params(axis)
        axis.set_xlabel(r'$1/T$',fontsize=16)
        axis.set_xlim([4.9,8.8])
        axis.text(0.03,.97,r'$(\mathrm{{{}}})$'.format(char),transform=axis.transAxes,ha='left',va='top',fontsize=16)
    
    ax[1].tick_params(labelleft=False,labelright=True)
    ax[1].yaxis.set_label_position('right')
    
    # S_4 #

    cmap = cm.get_cmap('magma_r')
    norm = mpl.colors.Normalize(vmin=.5,vmax=10)

    T = 0.115
    data = np.load('{}/T{:.4f}/N10000/S4kt.npz'.format(host_info.TwoTimeOut,T))
    data_R = np.load('{}/T{:.4f}/N10000/ta_vs_R.npz'.format(host_info.TwoTimeOut,T))
    ks,Rs = data['k'],data_R['R']

    k0 = 1.5 # k value used to obtain an initial xi value
    k_i0=np.searchsorted(ks,k0); k0 = ks[k_i0]

    R0 = 2 # R value used for obtaining xi, initially
    R_i0=np.searchsorted(Rs,R0); R0 = Rs[R_i0]

    z = None
    for R_i,R in enumerate(data_R['R']):
        ks,S4 = data['k'],data['S4'][R_i]
        inax0.semilogy(ks,S4/S4[0],c=cmap(norm(R)))
        
        if z is None:
            z = np.zeros((data_R['R'].size,ks.size))
        z[R_i,:] = S4/S4[0]

        if R_i==5:
            xi = get_xi_algo(ks,S4,1.5)
            inax0.semilogy(ks,1./(1+(xi*ks)**2),':k')

    default_tick_params(inax0)
    inax0.set_xlim([0.,6.5])
    inax0.text(.5,-.02,r'$k$',transform=inax0.transAxes,ha='center',va='top',fontsize=16)
    inax0.set_ylabel(r'$S_4/S_4 (0)$',fontsize=16,labelpad=-2)
    
    zframe = pd.DataFrame(z,index=ax_to_label(data_R['R'],'R'),columns=ax_to_label(ks,'k'))
    np.savez('{}/FigData/figS1/S1a_inset.npz'.format(datpath),
             R=data_R['R'],k=ks,S4_normed=zframe)
    zframe.to_pickle('{}/FigData/figS1/S1a_inset.pkl'.format(datpath))

    # tau_alpha(R_MW) #

    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.11,vmax=.2)

    z = None
    for T_i,T in enumerate(Ts):
        data_R = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/ta_vs_R.npz'.format(T))
        inax1.semilogy(data_R['R'],data_R['ta'],c=cmap(norm(T)))
        
        if z is None:
            z = np.zeros((Ts.size,data_R['R'].size))
        z[T_i,:] = data_R['ta']
    inax1.set_xlim([0,10])
    default_tick_params(inax1)
    inax1.text(.75,-.05,r'$R_{\mathrm{MW}}$',transform=inax1.transAxes,ha='center',va='top',fontsize=16)
    inax1.text(-.05,.46,r'$\tau_\alpha$',transform=inax1.transAxes,ha='right',va='center',fontsize=16,rotation=90)
    
    zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T'),columns=ax_to_label(data_R['R'],'R_MW'))
    np.savez('{}/FigData/figS1/S1b_inset.npz'.format(datpath),
             T=Ts,R=data_R['R'],tau_alpha=zframe)
    zframe.to_pickle('{}/FigData/figS1/S1b_inset.pkl'.format(datpath))
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS2(fname):
    """
    fig:gofr
    """
    
    from swap_tools import get_dminmax

    fig_w,fig_h = 5,3.5
    fig,ax = plt.subplots(figsize=[fig_w,fig_h])
    fig.subplots_adjust(0,0,1,1)
    inax = fig.add_axes([.47,.47,.5,.5])
    
    # g per T #
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.2)

    z = None
    Ts = np.array([0.1,0.105,0.11,0.115,0.12,0.13,0.14,0.15,0.2])
    for T_i,T in enumerate(Ts[::-1]):
        data = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/gofr.npz'.format(T))
        rmids = .5*(data['redges'][1:]+data['redges'][:-1])
        ax.plot(rmids,data['gofr'],c=cmap(norm(T)))
        
        if z is None:
            z = np.zeros((Ts.size,rmids.size))
        z[-1-T_i,:] = data['gofr']
    zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
    np.savez('{}/FigData/figS2/S2.npz'.format(datpath),
             T=Ts,r=rmids,gofr=zframe)
    zframe.to_pickle('{}/FigData/figS2/S2.pkl'.format(datpath))

    ax.set_xlim([0.,5.])
    ax.set_ylim([0.,3.])
    default_tick_params(ax)

    ax.set_xlabel(r'$r$',fontsize=16)
    ax.set_ylabel(r'$g$',fontsize=16)

    # g per diam #
    
    dmin,dmax = get_dminmax()
    cmap = cm.get_cmap('magma_r')
    norm = mpl.colors.LogNorm(vmin=dmin,vmax=dmax)

    T = 0.1
    data = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/gofr.npz'.format(T))
    rmids,dmids = .5*(data['redges'][1:]+data['redges'][:-1]),.5*(data['dedges'][1:]+data['dedges'][:-1])

    for diam_i,diam in enumerate(dmids[::-1]):
        inax.plot(rmids,data['gofr_binned'][:,-1-diam_i],c=cmap(norm(diam)))
    
    z = np.swapaxes(data['gofr_binned'],0,1)
    zframe = pd.DataFrame(z,index=ax_to_label(dmids,'diameter',fmt='.4g'),columns=ax_to_label(rmids,'r'))
    np.savez('{}/FigData/figS2/S2_inset.npz'.format(datpath),
             T='{:.3f}'.format(T),r=rmids,gofr=zframe)
    zframe.to_pickle('{}/FigData/figS2/S2_inset.pkl'.format(datpath))

    inax.set_xticks(np.arange(6))

    inax.set_xlim([0.,5.])
    inax.set_ylim([0.,3.7])
    default_tick_params(inax)

    inax.set_xlabel(r'$r$',fontsize=16)
    inax.set_ylabel(r'$g$',fontsize=16)

    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS3(fname):
    """
    fig:goftheta
    """
    
    # function definitions #
    
    def get_data(T,nslices=None):
        N,L,d = 10000,100.,2
        rootpath = '{}/T{:.4f}/N{:d}'.format(host_info.TwoTimeOut,T,N)
        suffix = 'normdense_T{:.4f}_N{:d}_t10000_c5_R{:.3g}_F3_a12'.format(T,N,get_xi_dyn(T))
        
        if nslices is None:
            if T>.1:
                suffix += '_x0.1000'
            data = np.load("{}/perturbed_fields_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dr2,dth = [q[1:]-q[:-1] for q in [data['redges']**2,data['thedges']]]
            rmids,thedges = .5*(data['redges'][1:]+data['redges'][:-1]),data['thedges']
            
            numhops = data['numhops']
            hist = data['hist0']
        else:
            for slice_i in range(nslices):
                data = np.load("{}/perturbed_fields_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if slice_i==0:
                    dr2,dth = [q[1:]-q[:-1] for q in [data['redges']**2,data['thedges']]]
                    rmids,thedges = .5*(data['redges'][1:]+data['redges'][:-1]),data['thedges']
                    
                    numhops = data['numhops']
                    hist = data['hist0']
                else:
                    numhops += data['numhops']
                    hist += data['hist0']
        dV = 2.*np.outer(dr2,dth) # pi r^2 * (dth/(2 pi)) times 4 (because of 4 quadrants)
        g = (hist/(numhops*dV))/(N/L**d)
            
        return rmids,thedges,g
    
    def get_Fdata(T,N=10000,nslices=None):
        rootpath = '{}/T{:.4f}/N{:d}'.format(host_info.TwoTimeOut,T,N)
        suffix = 'normdense_T{:.4f}_N{:d}_t10000_c5_R{:.3g}_F3_k12'.format(T,N,get_xi_dyn(T))
        
        if nslices is None:
            if T>.1:
                suffix += '_x0.1000'
            data = np.load("{}/perturbed_fields_Fourier_{}.npz".format(rootpath,suffix),allow_pickle=True)
            rmids = .5*(data['redges'][1:]+data['redges'][:-1])
            
            g = data['g0']
        else:
            for slice_i in range(nslices):
                data = np.load("{}/perturbed_fields_Fourier_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if slice_i==0:
                    rmids = .5*(data['redges'][1:]+data['redges'][:-1])
                    
                    hist = data['hist0']
                    g = data['g0']*(data['hist0']/data['g0'][0]).reshape((1,-1))
                else:
                    hist += data['hist0']
                    g += data['g0']*(data['hist0']/data['g0'][0]).reshape((1,-1))
        g /= hist.reshape((1,-1))
        g = np.nan_to_num(g)
        
        g[1:] *= 2. # convert complex Fourier coeffs c_k to real-space Fourier coeff c_k + c_{-k} for k>0
            
        return rmids,g
    
    def get_segments(y):
        assert not np.any(np.isnan(y))

        sgns = y>=0.
        i_change = np.insert(np.arange(1,y.size)[np.invert(sgns[:-1]*sgns[1:])],0,0)
        sgns = 2*(sgns.astype(int))-1

        ret = []
        for i in range(len(i_change)-1):
            x0,x1 = i_change[i],i_change[i+1]+1
            sgn = sgns[x0]
            ret.append([x0,x1,sgn])
        ret.append([i_change[-1],y.size,sgns[-1]])

        return ret
    
    def loglog_signed(axis,x,y,c,alpha=1.):
        linestyles = [':','-']
        
        for segment in get_segments(y):
            x0,x1,sgn = segment
            sgn_i = (1+sgn)//2
            axis.loglog(x[x0:x1],np.abs(y[x0:x1]),c=c,ls=linestyles[sgn_i],alpha=alpha)
            
        return axis
    
    ########################
    
    bw0,bw1,bh = 2,2.5,2
    ncols = 2
    wspace=1

    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw0,bh
    fig = plt.figure(figsize=[fig_w,fig_h])
    ax0 = fig.add_axes([0.,0.,bw0/fig_w,bh/fig_h],projection='polar')
    ax1 = fig.add_axes([(bw0+wspace)/fig_w,0.,bw1/fig_w,bh/fig_h])
    
    T,nslices = .1,10
    
    # polar plot #
    
    cmap = cm.get_cmap('bwr')
    norm = mpl.colors.LogNorm(vmin=.3,vmax=1./.3)

    rmids,thedges,g = get_data(T,nslices=nslices)
    ax0.pcolormesh(thedges,np.log10(rmids),g,cmap=cmap,norm=norm)
    
    thmids = .5*(thedges[1:]+thedges[1:])
    zframe = pd.DataFrame(g,index=ax_to_label(rmids,'r'),columns=ax_to_label(thmids,'theta',fmt='.3f'))
    np.savez('{}/FigData/figS3/S3a.npz'.format(datpath),
             T='{:.3f}'.format(T),r=rmids,theta=thmids,gofr=zframe)
    zframe.to_pickle('{}/FigData/figS3/S3a.pkl'.format(datpath))
    
    ax0.set_thetamin(0);  ax0.set_thetamax(90)
    ax0.set_xticklabels([])
    ax0.set_xlabel(r'$r$',fontsize=16,labelpad=15)
    
    yticks = [1,2,3,4,5]
    ax0.set_yticks(np.log10(yticks))
    ax0.set_yticklabels([r'${:d}$'.format(yval) for yval in yticks])
    ax0.set_ylim([np.log10(.3),np.log10(yticks[-1])])
    ax0.text(.9,.9,r'$\mathrm{(a)}$',transform=ax0.transAxes,ha='center',va='center',fontsize=16)
    
    plt.sca(ax0); plt.grid(True)
    default_tick_params(ax0)

    cax = fig.add_axes([-.03,0.,.02,1.])
    ticks = [.3,1,3]
    cbar = mpl.colorbar.ColorbarBase(cax,norm=norm,cmap=cmap,orientation='vertical',ticks=ticks)
    cbar.minorticks_off(); cbar.set_ticks(ticks)
    cbar.set_ticklabels([r'${:.3g}$'.format(tick) for tick in ticks])
    cax.text(-3,.5,r'$g (r, \theta )$',transform=cax.transAxes,ha='center',va='center',fontsize=16,rotation=90)
    default_tick_params(cax,cbar=True)
    cax.tick_params(labelleft=True,labelright=False)
    
    # Fourier coeffs #
    
    rmids,gtilde = get_Fdata(T,nslices=nslices)
    ks = 2*np.arange(1,gtilde.shape[0])
    for g_i,c in zip(gtilde[1:4],['C1','C2','C3']):
        loglog_signed(ax1,rmids,g_i,c)
    for g_i in gtilde[4:]:
        loglog_signed(ax1,rmids,g_i,'grey',alpha=.5)
        
    zframe = pd.DataFrame(gtilde[1:,:],index=ax_to_label(ks,'k',fmt='d'),columns=ax_to_label(rmids,'r'))
    np.savez('{}/FigData/figS3/S3b.npz'.format(datpath),
             T='{:.3f}'.format(T),k=ks,r=rmids,ghatofr=zframe)
    zframe.to_pickle('{}/FigData/figS3/S3b.pkl'.format(datpath))

    default_tick_params(ax1)
    ax1.set_xlim([.8,15])
    ax1.set_ylim([1e-5,.3])
    ax1.set_yticks([1e-5,1e-4,1e-3,1e-2,1e-1])
    ax1.set_xlabel(r'$r$',fontsize=16,labelpad=-2)
    ax1.set_ylabel(r'$\hat{\tilde{g}}_k$',fontsize=16)

    ax1.text(.9,.88,r'$\mathrm{(b)}$',transform=ax1.transAxes,ha='center',va='center',fontsize=16)
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS4(fname):
    """
    fig:rmsd
    """
    
    from swap_tools import get_dminmax
    
    fig,ax = plt.subplots(figsize=[5,3.5])
    fig.subplots_adjust(0,0,1,1)
    inax = fig.add_axes([.1,.45,.5,.5])
    
    # rmsd per T #
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.2)

    z = None
    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15,.2])
    for T_i,T in enumerate(Ts[::-1]):
        data = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/rmsd.npz'.format(T))
        ax.loglog(data['Dt'],data['rmsd_all'],c=cmap(norm(T)))
        
        if z is None:
            z = np.zeros((Ts.size,data['Dt'].size))
        elif data['Dt'].size > z.shape[1]:
            dummy = np.copy(z)
            z = np.zeros((Ts.size,data['Dt'].size))
            z[:,:dummy.shape[1]] = dummy
        z[-1-T_i,:] = data['rmsd_all']
    zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(data['Dt'],'Dt'))
    np.savez('{}/FigData/figS4/S4.npz'.format(datpath),
             T=Ts,Dt=data['Dt'],rmsd=zframe)
    zframe.to_pickle('{}/FigData/figS4/S4.pkl'.format(datpath))
    
    default_tick_params(ax)

    ax.set_xlim([1e-1,1e6])
    ax.set_ylim([4e-2,1e1])
    

    # T per diam #

    dmin,dmax = get_dminmax()
    cmap = cm.get_cmap('magma_r')
    norm = mpl.colors.LogNorm(vmin=dmin,vmax=dmax)

    T = 0.1
    data = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/rmsd.npz'.format(T))

    dmids = .5*(data['dedges'][1:]+data['dedges'][:-1])
    for diam_i,diam in enumerate(dmids[::-1]):
        inax.loglog(data['Dt'],data['rmsd'][:,-1-diam_i],c=cmap(norm(diam)))
        
    z = np.swapaxes(data['rmsd'],0,1)
    zframe = pd.DataFrame(z,index=ax_to_label(dmids,'diameter',fmt='.4g'),columns=ax_to_label(data['Dt'],'Dt'))
    np.savez('{}/FigData/figS4/S4_inset.npz'.format(datpath),
             T='{:.3f}'.format(T),Dt=data['Dt'],rmsd=zframe)
    zframe.to_pickle('{}/FigData/figS4/S4_inset.pkl'.format(datpath))

    ax.set_xlabel(r'$\Delta t$',fontsize=16)
    ax.set_ylabel(r'$\mathrm{RMSD}$',fontsize=16)

    default_tick_params(inax)

    inax.set_xlim([1e-1,1e6])
    inax.set_ylim([4e-2,1e0])

    inax.text(.43,-.02,r'$\Delta t$',transform=inax.transAxes,ha='center',va='top',fontsize=16)
    inax.text(-.05,.65,r'$\mathrm{RMSD}$',transform=inax.transAxes,ha='right',va='center',fontsize=16,rotation=90)
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS5(fname):
    """
    fig:dr2corr
    """
    
    # function definitions #
    
    @njit
    def get_pc2_numba(pc2_in,types,pvals):
        for i,type_i in enumerate(types):
            pc2_in[i] = pvals[type_i]**2
        return

    def get_pc2(types,pvals):
        pc2 = np.zeros(types.size)
        get_pc2_numba(pc2,types,pvals)
        return pc2
    
    def linefit(x,y,nu=1.):
        minres = np.inf
        for i,x_i in enumerate(x):
            x_tmp,y_tmp = np.delete(x,i),np.delete(y,i)
            w = np.abs(x_tmp-x_i)**-nu
            m_tmp,c_tmp = np.polyfit(x_tmp,y_tmp,1,w=w)

            res = np.sum((w*(y_tmp-(m_tmp*x_tmp+c_tmp)))**2)
            if res < minres:
                m,c = m_tmp,c_tmp
                minres = res
        return m,c

    def get_xi(rmids,corr):
        num = corr.shape[0]
        xi = np.zeros(num)
        for Dt_i in range(num):
            y = np.log(corr[Dt_i])
            anmask = np.isfinite(y)
            x_an,y_an = rmids[anmask],y[anmask]
            m,c = linefit(x_an,y_an)
            xi[Dt_i] = -1./m
        return xi
    
    def get_s(diams,L,L_fig):
        """
        get value of s parameter to python's scatter function,
        corresponding to the diameter of the circle as measured in "points"
        """

        ms = 72*L_fig*diams/L   # 72 points per inch, L_fig inches per figure, particle diameter diams/L as fraction of L_fig
        s = ms**2
        return s

    from scipy.signal import savgol_filter
    from swap_tools import get_tau_alpha,PBC
    from nlist_tools import get_nlist_merged,get_dp_com
    
    ########################
    
    wspace_inches,hspace_inches = .1,.1
    inbw,inbh = 2.,2.
    bw,bh = 4.5,2.*inbh+hspace_inches
    cbw,cwspace = .15,.05

    fig_w,fig_h = bw+inbw+wspace_inches,bh
    fig = plt.figure(figsize=[fig_w,fig_h])
    fig.subplots_adjust(0,0,1,1)
    ax0 = fig.add_axes([0.,0.,bw/fig_w,bh/fig_h])
    ax1 = fig.add_axes([(bw+wspace_inches)/fig_w,(inbh+hspace_inches)/fig_h,inbw/fig_w,inbh/fig_h])
    ax2 = fig.add_axes([(bw+wspace_inches)/fig_w,0.,inbw/fig_w,inbh/fig_h])
    inax = fig.add_axes([.25*bw/fig_w,.46*bh/fig_h,.5*bw/fig_w,.5*bh/fig_h])
    
    # xi_displacement vs Dt #
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15)

    Ts = np.array([0.1,.105,.11,.115,.12,.13,.14,.15])
    z = None
    Dt = np.zeros(0)
    for T_i,T in enumerate(Ts):
        c = cmap(norm(T))

        data = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/dr2corr_safety_plat.npz'.format(T),allow_pickle=True)
        r = .5*(data['redges'][1:]+data['redges'][:-1])

        xi = get_xi(r,data['corr'])
        if Dt.size < data['Dt'].size:
            Dt = data['Dt']

        ax0.semilogx(data['Dt'],xi,c=c,alpha=.2)
        ax0.semilogx(data['Dt'],savgol_filter(xi,31,3),c=c)
        
        if z is None:
            z,zsmooth = np.zeros((Ts.size,Dt.size)),np.zeros((Ts.size,Dt.size))
        elif Dt.size > z.shape[1]:
            dummy = np.copy(z)
            z = np.zeros((Ts.size,Dt.size))
            z[:,:dummy.shape[1]] = dummy
            
            dummy = np.copy(zsmooth)
            zsmooth = np.zeros((Ts.size,Dt.size))
            zsmooth[:,:dummy.shape[1]] = dummy
        z[T_i,:data['Dt'].size] = xi
        zsmooth[T_i,:data['Dt'].size] = savgol_filter(xi,31,3)
        
    zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(Dt,'Dt'))
    zsmoothframe = pd.DataFrame(zsmooth,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(Dt,'Dt'))
    np.savez('{}/FigData/figS5/S5a.npz'.format(datpath),
             T=Ts,Dt=Dt,corrlen=zframe,corrlen_smooth=zsmoothframe)
    zframe.to_pickle('{}/FigData/figS5/S5a.pkl'.format(datpath))
    zsmoothframe.to_pickle('{}/FigData/figS5/S5a_smooth.pkl'.format(datpath))

    default_tick_params(ax0)
    ax0.text(.02*inbw/bw,1.-.08*inbh/bh,r'$(\mathrm{a})$',transform=ax0.transAxes,ha='left',va='top',fontsize=16)
    ax0.set_xlim([1e-1,1e6])
    ax0.set_ylim([2,6.3])
    ax0.set_yticks([2,3,4,5,6])
    ax0.set_xlabel(r'$\Delta t$',fontsize=16)
    ax0.set_ylabel(r'$\xi_\mathrm{c}$',fontsize=16)
    ax0.tick_params(axis='x',pad=7)
    
    # displacement correlation vs r #
    
    cmap = cm.get_cmap('magma')
    norm = mpl.colors.LogNorm(vmin=1e-1,vmax=3e6)

    T = 0.1

    data = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N10000/dr2corr_safety_plat.npz'.format(T),allow_pickle=True)
    r = .5*(data['redges'][1:]+data['redges'][:-1])

    Dt_is = np.arange(0,data['Dt'].size,30)
    expfit_prefactor,expfit_growthrate = np.zeros(Dt_is.size),np.zeros(Dt_is.size)
    for i,Dt_i in enumerate(Dt_is):
        c = cmap(norm(data['Dt'][Dt_i]))

        inax.semilogy(r,data['corr'][Dt_i,:],c=c)

        y = np.log(data['corr'][Dt_i])
        anmask = np.isfinite(y)    
        m,c_coeff = linefit(r[anmask],y[anmask])
        inax.semilogy(r,np.exp(c_coeff)*np.exp(m*r),c=c,ls=':',alpha=.5)
        
        expfit_prefactor[i] = np.exp(c_coeff)
        expfit_growthrate[i] = m
    zframe = pd.DataFrame(data['corr'][Dt_is],index=ax_to_label(data['Dt'][Dt_is],'Dt'),columns=ax_to_label(r,'r'))
    zframe_pf = pd.Series(data=expfit_prefactor,index=ax_to_label(data['Dt'][Dt_is],'Dt'))
    zframe_gr = pd.Series(data=expfit_growthrate,index=ax_to_label(data['Dt'][Dt_is],'Dt'))
    np.savez('{}/FigData/figS5/S5a_inset.npz'.format(datpath),
             Dt=data['Dt'][Dt_is],r=r,
             corr=zframe,expfit_prefactor=zframe_pf,expfit_growthrate=zframe_gr)
    zframe.to_pickle('{}/FigData/figS5/S5a_inset.pkl'.format(datpath))
    zframe_pf.to_pickle('{}/FigData/figS5/S5a_inset_prefactor.pkl'.format(datpath))
    zframe_gr.to_pickle('{}/FigData/figS5/S5a_inset_growthrate.pkl'.format(datpath))

    default_tick_params(inax)
    inax.set_xlim([0.,15])
    inax.set_ylim([1e-3,1e0])
    inax.set_xlabel(r'$r$',fontsize=16,labelpad=-15)
    inax.set_ylabel(r'$\mathrm{Corr}$',fontsize=16,labelpad=-15)
    
    ### overlap and hop scatter plots ###

    T = 0.12
    Dt = 100 # rearrangement interval
    tau_alpha,R = get_tau_alpha(T),get_xi_dyn(T)

    rootpath = '/home1/chackorn/avalanches/LinTimeTrajs/trajs_EQ_poly_SS_2D_N10000_T{:.4f}/Cnf-1'.format(T)

    sys_params = np.load('{}/sys_params.npz'.format(rootpath),allow_pickle=True)
    d,N,L = [typ(sys_params[s]) for (s,typ) in zip(['d','N','L'],[int,int,float])]
    step,time,diams = [sys_params[s] for s in ['step','time','diameter']]

    Dt_i = step[np.searchsorted(time,Dt,side='right')-1]
    ta_i = Dt_i*int(round(tau_alpha/Dt))

    for axis in [ax1,ax2]:
        axis.set_xticks([])
        axis.set_yticks([])
        axis.set_xlim([-L/2.,L/2.])
        axis.set_ylim([-L/2.,L/2.])

    # overlap #

    cmap = cm.get_cmap('Greys')
    norm = mpl.colors.LogNorm(vmin=3e-2,vmax=3e0)

    p0 = np.load('{}/quenched_position/0.npy'.format(rootpath)).astype(float)
    p1 = np.load('{}/quenched_position/{:d}.npy'.format(rootpath,ta_i)).astype(float)

    nlist_c,nlist_inds_c = get_nlist_merged(p0,p1,L,R)
    dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)

    ax1.scatter(p0[:,0],p0[:,1],c=np.sqrt(np.sum(dp**2,axis=1)),cmap=cmap,norm=norm,s=get_s(diams,L,inbw))

    ax1.text(.02,.92,r'$(\mathrm{b})$',transform=ax1.transAxes,ha='left',va='top',fontsize=16)

    cax = fig.add_axes([1.+(cwspace/fig_w),(inbh+hspace_inches)/fig_h,cbw/fig_w,inbh/fig_h])
    cbar = mpl.colorbar.ColorbarBase(cax,norm=norm,cmap=cmap,orientation='vertical')
    cax.text(2.2,0.5,r'$\Delta r$',transform=cax.transAxes,ha='left',va='center',rotation=90,fontsize=16)
    default_tick_params(cax,cbar=True)
    
    np.savez('{}/FigData/figS5/S5b.npz'.format(datpath),
             x=p0[:,0],y=p0[:,1],diameter=diams,L=L,displacement_magnitude=np.sqrt(np.sum(dp**2,axis=1)))

    # hops #

    cmap = cm.get_cmap('plasma')
    norm = mpl.colors.Normalize(vmin=0,vmax=1.)

    platdata = np.load('/home1/chackorn/avalanches/TwoTimeOut/T{:.4f}/N{:d}/pc_plateau.npz'.format(T,N),allow_pickle=True)
    types,pvals = np.searchsorted(platdata['dedges'][:-1],diams,side='right')-1,platdata['p_c']
    pc2 = get_pc2(types,pvals)

    t,x,y,diameter = ([] for i in range(4))
    for i,t_i in enumerate(range(0,ta_i+1,Dt_i)):
        p0 = np.load('{}/quenched_position/{:d}.npy'.format(rootpath,t_i)).astype(float)
        p1 = np.load('{}/quenched_position/{:d}.npy'.format(rootpath,t_i+Dt_i)).astype(float)

        nlist_c,nlist_inds_c = get_nlist_merged(p0,p1,L,R)
        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)

        mask = np.sum(dp**2,axis=1)/pc2 > 5.
        ax2.scatter(p0[mask,0],p0[mask,1],c=np.repeat(t_i/ta_i,np.sum(mask)),cmap=cmap,norm=norm,s=get_s(diams[mask],L,inbw))
        
        nhoppers = np.sum(mask)
        t.extend(list(np.repeat(1e-2*t_i,np.sum(nhoppers))))
        x.extend(list(p0[mask,0]))
        y.extend(list(p1[mask,1]))
        diameter.extend(list(diams[mask]))
    
    t,x,y,diameter = (np.array(lst) for lst in (t,x,y,diameter))
    np.savez('{}/FigData/figS5/S5c.npz'.format(datpath),
             t=t,x=x,y=y,diameter=diameter,L=L)

    ax2.text(.02,.92,r'$(\mathrm{c})$',transform=ax2.transAxes,ha='left',va='top',fontsize=16)

    cax = fig.add_axes([1.+(cwspace/fig_w),0.,cbw/fig_w,inbh/fig_h])
    cbar = mpl.colorbar.ColorbarBase(cax,norm=norm,cmap=cmap,orientation='vertical')
    cbar.set_ticks([0.,.5,1.])
    cax.text(1.7,0.75,r'$t / \tau_\alpha$',transform=cax.transAxes,ha='left',va='center',rotation=90,fontsize=16)
    default_tick_params(cax,cbar=True)
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS6(fname):
    """
    fig:Dt
    """
    
    # function definitions #
    
    def get_data_binctrl(T,t,lb=2.5,ub=40,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{:.4f}_N10000_t{:d}_c5_R{:.3g}_F3_a12'.format(T,t,get_xi_dyn(T))
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
                    
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
        else:
            gtilde = np.zeros((1+bootstrap,*hist.shape))
            alpha =np.zeros((1+bootstrap,rmids.size))
            I_alpha = np.zeros(1+bootstrap)
            
            gtilde[0,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha[0,:] = .5*np.sum((gtilde[0]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha[0] = np.sum(alpha[0,lb_i:ub_i]*dr[lb_i:ub_i])
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde[sample_i,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha[sample_i,:] = .5*np.sum((gtilde[sample_i]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
                I_alpha[sample_i] = np.sum(alpha[sample_i,lb_i:ub_i]*dr[lb_i:ub_i])
            
        return rmids,alpha,I_alpha
    
    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    from swap_tools import T_MCT
    
    ########################
    
    bw,bh = 2.4,1.7
    ncols,nrows = 2,2
    wspace,hspace = 0.,0.2
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh

    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)

    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])[::-1]
    
    nbssamps = 10 # number of bootstrap sample g_tildes
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15)

    for j,(t,lab0,lab1) in enumerate(zip([1000,10000],['a','b'],['c','d'])):
        I_alpha = np.zeros((1+nbssamps,Ts.size))
        z = None
        for T_i,T in enumerate(Ts):
            nslices = None if T>.1 else 10
            rmids,alpha,I_alpha[:,T_i] = get_data_binctrl(T,t,nslices=nslices,bootstrap=nbssamps)
            ax[0,j].loglog(rmids,alpha[0],c=cmap(norm(T)))
            
            if z is None:
                z = np.zeros((Ts.size,rmids.size))
            z[T_i,:] = alpha[0]

        ax[0,j].set_xlabel(r'$r$',fontsize=16,labelpad=-15)
        ax[0,j].set_xlim([.7,40])
        ax[0,j].text(.96,.95,r'$(\mathrm{{{}}})$'.format(lab0),transform=ax[0,j].transAxes,ha='right',va='top',fontsize=16)
        ax[0,j].set_ylim([3e-7,2e-2])
        ax[0,j].set_yticks([1e-6,1e-5,1e-4,1e-3,1e-2])
        ax[0,j].text(.5,.85,r'$\Delta t = 10^{:g}$'.format(np.log10(1e-2*t)),transform=ax[0,j].transAxes,ha='center',va='center',fontsize=16)
        
        zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
        np.savez('{}/FigData/figS6/S6{}.npz'.format(datpath,lab0),
                 T=Ts,r=rmids,mean_squared_anisotropy=zframe)
        zframe.to_pickle('{}/FigData/figS6/S6{}.pkl'.format(datpath,lab0))

        ax[1,j].plot(1./Ts,I_alpha[0],'kx')
        for sample_i in range(1,1+nbssamps):
            ax[1,j].plot(1./Ts,I_alpha[sample_i],'kx',alpha=.2)
        
        ax[1,j].text(0.55,-.03,r'$1/T$',transform=ax[1,j].transAxes,ha='center',va='top',fontsize=16)
        ax[1,j].set_xlim([6.5,10.2])
        ax[1,j].text(.96,.05,r'$(\mathrm{{{}}})$'.format(lab1),transform=ax[1,j].transAxes,ha='right',va='bottom',fontsize=16)
        
        dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(I_alpha.shape[0])]
        dfnames[0] = 'original data'
        zframe = pd.DataFrame(I_alpha,index=dfnames,columns=ax_to_label(1./Ts,'1/T'))
        np.savez('{}/FigData/figS6/S6{}.npz'.format(datpath,lab1),T=Ts,I_alpha=zframe)
        zframe.to_pickle('{}/FigData/figS6/S6{}.pkl'.format(datpath,lab1))

    for j,(yl,yu) in enumerate([(0.,.017),(0.,.003)]):
        ax[1,j].plot(np.repeat(1./T_MCT,10),np.linspace(yl,yu,10),':k')
        ax[1,j].set_ylim([yl,yu])

    for axis in ax.reshape(-1):
        default_tick_params(axis)
    ax[0,0].text(-.1,.64,r'$\alpha$',transform=ax[0,0].transAxes,ha='right',va='center',fontsize=16)
    ax[0,1].tick_params(labelleft=False)
    
    ax[1,0].text(-.09,.42,r'$I_\alpha$',transform=ax[1,0].transAxes,ha='right',va='center',fontsize=16)
    ax[1,0].tick_params(labelleft=True,labelright=False)
    ax[1,1].tick_params(which='both',labelleft=False,labelright=True)

    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS7(fname):
    """
    fig:xiFL
    """
    
    # function definitions #
    
    def get_data_binctrl(T,F,lb=2.5,ub=40,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{:.4f}_N10000_t10000_c5_R{:.3g}_F{:.3g}_a12'.format(T,get_xi_dyn(T),F)
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
                    
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
        else:
            gtilde = np.zeros((1+bootstrap,*hist.shape))
            alpha =np.zeros((1+bootstrap,rmids.size))
            I_alpha = np.zeros(1+bootstrap)
            
            gtilde[0,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha[0,:] = .5*np.sum((gtilde[0]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha[0] = np.sum(alpha[0,lb_i:ub_i]*dr[lb_i:ub_i])
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde[sample_i,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha[sample_i,:] = .5*np.sum((gtilde[sample_i]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
                I_alpha[sample_i] = np.sum(alpha[sample_i,lb_i:ub_i]*dr[lb_i:ub_i])
            
        return rmids,alpha,I_alpha
    
    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    from swap_tools import T_MCT
    
    ########################
    
    bw,bh = 2.4,1.7
    ncols,nrows = 3,2
    wspace,hspace = 0.,0.2
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh

    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)

    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])[::-1]
    
    nbssamps = 10 # number of bootstrap sample g_tildes
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15)

    for j,(F,lab0,lab1) in enumerate(zip([1.5,3,6],['a','b','c'],['d','e','f'])):
        I_alpha = np.zeros((1+nbssamps,Ts.size))
        
        z = None
        for T_i,T in enumerate(Ts):
            nslices = None if T>.1 else 10
            rmids,alpha,I_alpha[:,T_i] = get_data_binctrl(T,F,nslices=nslices,bootstrap=nbssamps)
            ax[0,j].loglog(rmids,alpha[0],c=cmap(norm(T)))
            
            if z is None:
                z = np.zeros((Ts.size,rmids.size))
            z[T_i,:] = alpha[0]

        ax[0,j].set_xlabel(r'$r$',fontsize=16,labelpad=-15)
        ax[0,j].set_xlim([.7,40])
        ax[0,j].text(.96,.95,r'$(\mathrm{{{}}})$'.format(lab0),transform=ax[0,j].transAxes,ha='right',va='top',fontsize=16)
        ax[0,j].set_ylim([3e-7,2e-2])
        ax[0,j].set_yticks([1e-6,1e-5,1e-4,1e-3,1e-2])
        ax[0,j].text(.5,.85,r'$\xi_\mathrm{{FL}} = {:g}$'.format(F),transform=ax[0,j].transAxes,ha='center',va='center',fontsize=16)
        
        zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
        np.savez('{}/FigData/figS7/S7{}.npz'.format(datpath,lab0),
                 T=Ts,r=rmids,mean_squared_anisotropy=zframe)
        zframe.to_pickle('{}/FigData/figS7/S7{}.pkl'.format(datpath,lab0))

        ax[1,j].plot(1./Ts,I_alpha[0],'kx')
        for sample_i in range(1,1+nbssamps):
            ax[1,j].plot(1./Ts,I_alpha[sample_i],'kx',alpha=.2)
        
        ax[1,j].text(0.55,-.03,r'$1/T$',transform=ax[1,j].transAxes,ha='center',va='top',fontsize=16)
        ax[1,j].set_xlim([6.5,10.2])
        ax[1,j].text(.96,.05,r'$(\mathrm{{{}}})$'.format(lab1),transform=ax[1,j].transAxes,ha='right',va='bottom',fontsize=16)
        
        dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(I_alpha.shape[0])]
        dfnames[0] = 'original data'
        zframe = pd.DataFrame(I_alpha,index=dfnames,columns=ax_to_label(1./Ts,'1/T'))
        np.savez('{}/FigData/figS7/S7{}.npz'.format(datpath,lab1),T=Ts,I_alpha=zframe)
        zframe.to_pickle('{}/FigData/figS7/S7{}.pkl'.format(datpath,lab1))

    for j in range(3):
        yl,yu = 0.,.003
        ax[1,j].set_ylim([yl,yu])
        ax[1,j].plot(np.repeat(1./T_MCT,10),np.linspace(yl,yu,10),':k')

    for axis in ax.reshape(-1):
        default_tick_params(axis)
    ax[0,0].text(-.1,.64,r'$\alpha$',transform=ax[0,0].transAxes,ha='right',va='center',fontsize=16)
    ax[0,1].tick_params(labelleft=False)
    
    ax[1,0].text(-.09,.48,r'$I_\alpha$',transform=ax[1,0].transAxes,ha='right',va='center',fontsize=16)
    for j in range(1,3):
        ax[0,j].tick_params(which='both',labelleft=False)
        ax[1,j].tick_params(which='both',labelleft=False)

    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS8(fname):
    """
    fig:disjoint
    """
    
    # function definitions #
    
    def get_data_binctrl(T,disjoint,lb=2.5,ub=40,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        djstring = 'disjoint_' if disjoint else ''
        suffix = 'normdense_{}T{:.4f}_N10000_t10000_c5_R{:.3g}_F3_a12'.format(djstring,T,get_xi_dyn(T))
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
                    
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
        else:
            gtilde = np.zeros((1+bootstrap,*hist.shape))
            alpha =np.zeros((1+bootstrap,rmids.size))
            I_alpha = np.zeros(1+bootstrap)
            
            gtilde[0,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha[0,:] = .5*np.sum((gtilde[0]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha[0] = np.sum(alpha[0,lb_i:ub_i]*dr[lb_i:ub_i])
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde[sample_i,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha[sample_i,:] = .5*np.sum((gtilde[sample_i]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
                I_alpha[sample_i] = np.sum(alpha[sample_i,lb_i:ub_i]*dr[lb_i:ub_i])
            
        return rmids,alpha,I_alpha
    
    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    from swap_tools import T_MCT
    
    ########################
    
    bw,bh = 2.4,1.7
    ncols,nrows = 2,2
    wspace,hspace = 0.,0.2
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh

    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)

    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])[::-1]
    
    nbssamps = 10 # number of bootstrap sample g_tildes
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15)

    for j,(disjoint,lab0,lab1) in enumerate(zip([False,True],['a','b'],['c','d'])):
        I_alpha = np.zeros((1+nbssamps,Ts.size))
        
        z = None
        for T_i,T in enumerate(Ts):
            nslices = None if T>.1 else 10
            rmids,alpha,I_alpha[:,T_i] = get_data_binctrl(T,disjoint,nslices=nslices,bootstrap=nbssamps)

            ax[0,j].loglog(rmids,alpha[0],c=cmap(norm(T)))
            
            if z is None:
                z = np.zeros((Ts.size,rmids.size))
            z[T_i,:] = alpha[0]

        ax[0,j].set_xlabel(r'$r$',fontsize=16,labelpad=-15)
        ax[0,j].set_xlim([.7,40])
        ax[0,j].text(.96,.95,r'$(\mathrm{{{}}})$'.format(lab0),transform=ax[0,j].transAxes,ha='right',va='top',fontsize=16)
        ax[0,j].set_ylim([3e-7,2e-2])
        ax[0,j].set_yticks([1e-6,1e-5,1e-4,1e-3,1e-2])
        
        zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
        np.savez('{}/FigData/figS8/S8{}.npz'.format(datpath,lab0),
                 T=Ts,r=rmids,mean_squared_anisotropy=zframe)
        zframe.to_pickle('{}/FigData/figS8/S8{}.pkl'.format(datpath,lab0))

        ax[1,j].plot(1./Ts,I_alpha[0],'kx')
        for sample_i in range(1,1+nbssamps):
            ax[1,j].plot(1./Ts,I_alpha[sample_i],'kx',alpha=.2)
        
        ax[1,j].text(0.55,-.03,r'$1/T$',transform=ax[1,j].transAxes,ha='center',va='top',fontsize=16)
        ax[1,j].set_xlim([6.5,10.2])
        ax[1,j].text(.96,.05,r'$(\mathrm{{{}}})$'.format(lab1),transform=ax[1,j].transAxes,ha='right',va='bottom',fontsize=16)
        
        dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(I_alpha.shape[0])]
        dfnames[0] = 'original data'
        zframe = pd.DataFrame(I_alpha,index=dfnames,columns=ax_to_label(1./Ts,'1/T'))
        np.savez('{}/FigData/figS8/S8{}.npz'.format(datpath,lab1),T=Ts,I_alpha=zframe)
        zframe.to_pickle('{}/FigData/figS8/S8{}.pkl'.format(datpath,lab1))

    for j,(yl,yu) in enumerate([(0.,.0035),(0.,.0017)]):
        ax[1,j].plot(np.repeat(1./T_MCT,10),np.linspace(yl,yu,10),':k')
        ax[1,j].set_ylim([yl,yu])

    for axis in ax.reshape(-1):
        default_tick_params(axis)
        
    ax[0,0].text(.5,.85,r'$\mathrm{shared}$',transform=ax[0,0].transAxes,ha='center',va='center',fontsize=16)
    ax[0,0].text(-.1,.64,r'$\alpha$',transform=ax[0,0].transAxes,ha='right',va='center',fontsize=16)
    
    ax[0,1].text(.5,.85,r'$\mathrm{disjoint}$',transform=ax[0,1].transAxes,ha='center',va='center',fontsize=16)
    ax[0,1].tick_params(labelleft=False)
    
    ax[1,0].text(-.09,.42,r'$I_\alpha$',transform=ax[1,0].transAxes,ha='right',va='center',fontsize=16)
    ax[1,0].tick_params(labelleft=True,labelright=False)
    ax[1,1].tick_params(which='both',labelleft=False,labelright=True)

    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS9(fname):
    """
    fig:noise_floor
    """

    # function definitions #
    
    def get_data(T,nslices=None):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{:.4f}_N10000_t10000_c5_R{:.3g}_F3_k12'.format(T,get_xi_dyn(T))
        
        if nslices is None:
            if T > .1:
                suffix += '_x0.1000'
            data = np.load("{}/perturbed_fields_Fourier_{}.npz".format(rootpath,suffix),allow_pickle=True)
            rmids = .5*(data['redges'][1:]+data['redges'][:-1])
            g_tilde = data['g_tilde']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/perturbed_fields_Fourier_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    rmids = .5*(data['redges'][1:]+data['redges'][:-1])
                    hist = data['hist0']
                    iso,dev,exx,exy = [np.nan_to_num(data[qstr])*data['hist0'].reshape((1,-1)) for qstr in ['iso','dev','exx','exy']]
                else:
                    hist += data['hist0']
                    for q,qstr in [(iso,'iso'),(dev,'dev'),(exx,'exx'),(exy,'exy')]:
                        q += np.nan_to_num(data[qstr])*data['hist0'].reshape((1,-1))
                    
        anmask = hist>0; nanmask = np.invert(anmask)
        for q in [iso,dev,exx,exy]:
            q[:,anmask] /= hist[anmask].astype(q.dtype).reshape((1,-1))
            q[:,nanmask] = np.nan
            q[1:] *= 2. # convert complex Fourier coeffs c_k to real-space Fourier coeff c_k + c_{-k} for k>0
        
        return rmids[anmask],iso[:,anmask],dev[:,anmask],exx[:,anmask],exy[:,anmask]
    
    def get_segments(y):
        assert not np.any(np.isnan(y))

        sgns = y>=0.
        i_change = np.insert(np.arange(1,y.size)[np.invert(sgns[:-1]*sgns[1:])],0,0)
        sgns = 2*(sgns.astype(int))-1

        ret = []
        for i in range(len(i_change)-1):
            x0,x1 = i_change[i],i_change[i+1]+1
            sgn = sgns[x0]
            ret.append([x0,x1,sgn])
        ret.append([i_change[-1],y.size,sgns[-1]])

        return ret
    
    def loglog_signed(axis,x,y,c,alpha=1.):
        linestyles = [':','-']
        
        for segment in get_segments(y):
            x0,x1,sgn = segment
            sgn_i = (1+sgn)//2
            axis.loglog(x[x0:x1],np.abs(y[x0:x1]),c=c,ls=linestyles[sgn_i],alpha=alpha)
            
        return axis
    
    ########################
    
    bw = 2
    bh = 1.6
    ncols,nrows=2,2
    wspace,hspace=0.,0.

    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*wspace)*bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)
    
    T = .1
    rmids,iso,dev,exx,exy = get_data(T,nslices=10)
    
    for axis,q,qname,char in zip(ax.reshape(-1),[iso,dev,exx,exy],['iso','dev','exx','exy'],['a','b','c','d']):
        ks = 2*np.arange(q.shape[0],dtype=int)
        for q_i,c in zip(q[0:3],['C0','C1','C2']):
            loglog_signed(axis,rmids,q_i,c)
        for q_i in q[4:]:
            loglog_signed(axis,rmids,q_i,'grey',alpha=.5)
        
        zframe = pd.DataFrame(q,index=ax_to_label(ks,'k',fmt='d'),columns=ax_to_label(rmids,'r'))
        outdict = {'k':ks,'r':rmids,'gammahat_{}'.format(qname):zframe}
        np.savez('{}/FigData/figS9/S9{}.npz'.format(datpath,char),
                 **outdict)
        zframe.to_pickle('{}/FigData/figS9/S9{}.pkl'.format(datpath,char))
        
        default_tick_params(axis)
        axis.text(.98,.95,r'$(\mathrm{{{}}})$'.format(char),transform=axis.transAxes,ha='right',va='top',fontsize=16)

        for i in range(2):
            ax[i,1].tick_params(labelleft=False,labelright=True)
            ax[i,1].yaxis.set_label_position('right')
        for j in range(2):
            ax[0,j].tick_params(labelbottom=False)
            ax[1,j].set_xlabel(r'$r$',fontsize=16)

        ax[0,0].set_ylim([2e-7,2e-2])
        ax[0,1].set_ylim([2e-7,3e-1])
        ax[1,0].set_ylim([2e-7,1e-1])
        ax[1,1].set_ylim([2e-7,2e-3])

        ax[0,0].set_ylabel(r'$\gamma_\mathrm{iso}$',fontsize=16)
        ax[0,1].set_ylabel(r'$\gamma_\mathrm{dev}$',fontsize=16)
        ax[1,0].set_ylabel(r'$\gamma_\mathrm{ext}$',fontsize=16)
        ax[1,1].set_ylabel(r'$\gamma_\mathrm{flow}$',fontsize=16)
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS10(fname):
    """
    fig:bootstrap
    """
    
    # function definitions #
    
    def get_data_binctrl(T,lb=2.5,ub=40,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{:.4f}_N10000_t10000_c5_R{:.3g}_F3_a12'.format(T,get_xi_dyn(T))
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
                    
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
        else:
            gtilde = np.zeros((1+bootstrap,*hist.shape))
            alpha =np.zeros((1+bootstrap,rmids.size))
            
            gtilde[0,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha[0,:] = .5*np.sum((gtilde[0]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde[sample_i,:,:] = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha[sample_i,:] = .5*np.sum((gtilde[sample_i]-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            
        return rmids,alpha
    
    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    from swap_tools import T_MCT
    
    ########################
    
    bw,bh = 2.4,1.7
    ncols = 2
    wspace,hspace = 0.,0.2
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,bh

    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)

    Ts = np.array([.1,.15])
    
    nbssamps = 10 # number of bootstrap sample g_tildes
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15)

    for j,(T,char) in enumerate(zip(Ts,string.ascii_lowercase)):
        nslices = None if T>.1 else 10
        rmids,alpha = get_data_binctrl(T,nslices=nslices,bootstrap=nbssamps)

        dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(alpha.shape[0])]
        dfnames[0] = 'original data'

        zframe = pd.DataFrame(alpha,index=dfnames,columns=ax_to_label(rmids,'r'))
        np.savez('{}/FigData/figS10/S10{}.npz'.format(datpath,char),
                 r=rmids,mean_squared_anisotropy=alpha)
        zframe.to_pickle('{}/FigData/figS10/S10{}.pkl'.format(datpath,char))

        ax[j].loglog(rmids,alpha[0],c=cmap(norm(T)))
        for sample_i in range(1,1+nbssamps):
            ax[j].plot(rmids,alpha[sample_i],c=cmap(norm(T)),alpha=.2)

        default_tick_params(ax[j])
        ax[j].set_xlabel(r'$r$',fontsize=16,labelpad=-15)
        ax[j].set_xlim([.7,40])
        ax[j].set_ylim([3e-7,2e-2])
        ax[j].set_yticks([1e-6,1e-5,1e-4,1e-3,1e-2])
        ax[j].text(.5,.95,r'$T={:.3f}$'.format(T),transform=ax[j].transAxes,ha='center',va='top',fontsize=16)
        ax[j].text(.96,.95,r'$(\mathrm{{{}}})$'.format(char),transform=ax[j].transAxes,ha='right',va='top',fontsize=16)
    ax[0].text(-.1,.64,r'$\alpha$',transform=ax[0].transAxes,ha='right',va='center',fontsize=16)
    ax[1].tick_params(labelleft=False)

    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS11(fname):
    """
    fig:lbound
    """
    
    # function definitions #
    
    def get_data_binctrl(T,lb=2.5,ub=40,nslices=None,bootstrap=0):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        suffix = 'normdense_T{:.4f}_N10000_t10000_c5_R{:.3g}_F3_a12'.format(T,get_xi_dyn(T))
        if T > .1:
            suffix = 'binctrl_{}_x0.1000'.format(suffix)
        
        if nslices is None:
            data = np.load("{}/g_hop_{}.npz".format(rootpath,suffix),allow_pickle=True)
            dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
            thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
            hist = data['hist']
        else:
            hist = None
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if hist is None:
                    dth,dr = [data[s][1:]-data[s][:-1] for s in ['thedges','redges']]
                    thmids,rmids = [.5*(data[s][1:]+data[s][:-1]) for s in ['thedges','redges']]
                    hist = data['hist']
                else:
                    hist += data['hist']
                    
        lb_i,ub_i = np.searchsorted(rmids,[lb,ub])
        if bootstrap == 0:
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
            I_alpha = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
        else:
            I_alpha = np.zeros(1+bootstrap)
            gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
            alpha = .5*np.sum((gtilde-1.)**2*dth,axis=1)/np.pi
            I_alpha[0] = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
            
            for sample_i in range(1,1+bootstrap):
                hist = bootstrap_sample(hist)    
                gtilde = hist/np.mean(hist,axis=1).reshape((-1,1))
                alpha = .5*np.sum((gtilde-1.)**2*dth.reshape(1,-1),axis=1)/np.pi
                I_alpha[sample_i] = np.sum(alpha[lb_i:ub_i]*dr[lb_i:ub_i])
            
        return rmids,I_alpha
    
    def bootstrap_sample(hist):
        assert np.issubdtype(hist.dtype,np.integer)
        
        nrbins,nthbins = hist.shape
        sample = np.zeros(hist.shape) # output sample g_tilde
        
        for r_i in range(nrbins):
            N_i = np.sum(hist[r_i]) # pop. in bin r_i
            if N_i > 0:
                th_inds = np.random.choice(nthbins,N_i,p=hist[r_i]/float(N_i))
                sample[r_i,:] = np.histogram(th_inds,bins=nthbins,range=(-.5,nthbins-.5))[0]
        
        return sample.astype(hist.dtype)

    from swap_tools import T_MCT
    
    ########################
    
    bw,bh = 2.4,1.7
    ncols,nrows = 3,1
    wspace,hspace = 0.,0.
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh

    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)

    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])[::-1]
    
    nbssamps = 10 # number of bootstrap sample g_tildes
    
    for j,(lb,label) in enumerate(zip([2.5,5,10],['a','b','c'])):
        I_alpha = np.zeros((1+nbssamps,Ts.size))
        for T_i,T in enumerate(Ts):
            nslices = None if T>.1 else 10
            rmids,I_alpha[:,T_i] = get_data_binctrl(T,lb=lb,nslices=nslices,bootstrap=nbssamps)

        ax[j].plot(1./Ts,I_alpha[0],'kx')
        for sample_i in range(1,1+nbssamps):
            ax[j].plot(1./Ts,I_alpha[sample_i],'kx',alpha=.2)
        
        ax[j].semilogy(np.repeat(1./T_MCT,10),np.linspace(1e-10,1e3,10,endpoint=True),':k')
        ax[j].text(0.55,-.03,r'$1/T$',transform=ax[j].transAxes,ha='center',va='top',fontsize=16)
        ax[j].set_xlim([6.5,10.2])
        ax[j].set_ylim([1e-5,1e-2])
        ax[j].set_yticks([1e-5,1e-4,1e-3,1e-2])
        ax[j].text(.96,.05,r'$(\mathrm{{{}}})$'.format(label),transform=ax[j].transAxes,ha='right',va='bottom',fontsize=16)
        default_tick_params(ax[j])

        ax[j].text(.3,.85,r'$r_0={:g}$'.format(lb),transform=ax[j].transAxes,ha='center',va='center',fontsize=16)
        
        dfnames = ['bootstrapped sample {:d}'.format(i) for i in range(I_alpha.shape[0])]
        dfnames[0] = 'original data'
        zframe = pd.DataFrame(I_alpha,index=dfnames,columns=ax_to_label(1./Ts,'1/T'))
        np.savez('{}/FigData/figS11/S11{}.npz'.format(datpath,label),T=Ts,I_alpha=zframe)
        zframe.to_pickle('{}/FigData/figS11/S11{}.pkl'.format(datpath,label))

    ax[0].text(-.05,.5,r'$I_\alpha$',transform=ax[0].transAxes,ha='right',va='center',fontsize=16)
    for j in range(1,3):
        ax[j].tick_params(labelleft=False)

    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS12(fname):
    """
    fig:rhop
    """
    
    # function definitions #
       
    import scipy.special

    ########################
    
    bw,bh = 2,1.6
    ncols=2
    wspace=0.05

    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols)
    fig.subplots_adjust(0,0,1,1,wspace=wspace)

    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])
    rho_hops,r_hops = np.zeros(Ts.size),np.zeros(Ts.size)

    # raw data #
    
    L=100.
    for T_i,T in enumerate(Ts):
        rootpath = '{}/T{:.4f}/N10000'.format(host_info.TwoTimeOut,T)
        nhops_T = np.load('{}/hopsperrun_T{:.4f}_N10000_t10000_c5_R{:.3g}.npz'.format(rootpath,T,get_xi_dyn(T)))['hopsperrun']
        
        rho_hops[T_i] = nhops_T/(L**2) #  density of hops
        r_hops[T_i] = scipy.special.gamma(3/2)/np.sqrt(np.pi*rho_hops[T_i]) # mean min. distance between hops

    ax[0].semilogy(1./Ts,rho_hops,'kx')
    ax[1].semilogy(1./Ts,r_hops,'kx')

    #  linear fits #
    m,c = np.polyfit(1./Ts,np.log(rho_hops),1)
    A,T_ast = np.exp(c),-m
    ax[0].semilogy(1./Ts,A*np.exp(-T_ast/Ts))
    ax[1].semilogy(1./Ts,scipy.special.gamma(3/2)*np.exp(.5*T_ast/Ts)/np.sqrt(np.pi*A))

    zframe = pd.Series(data=rho_hops,index=ax_to_label(1./Ts,'1/T'))
    np.savez('{}/FigData/figS12/S12a.npz'.format(datpath),T=Ts,rho_hop=zframe,expfit_prefactor=A,expfit_decayrate=T_ast)
    zframe.to_pickle('{}/FigData/figS12/S12a.pkl'.format(datpath))

    zframe = pd.Series(data=r_hops,index=ax_to_label(1./Ts,'1/T'))
    np.savez('{}/FigData/figS12/S12b.npz'.format(datpath),T=Ts,r_hop=zframe,expfit_prefactor=scipy.special.gamma(3/2)/np.sqrt(np.pi*A),expfit_growthrate=.5*T_ast)
    zframe.to_pickle('{}/FigData/figS12/S12b.pkl'.format(datpath))

    for j in range(2):
        ax[j].set_xlim([6.5,10.2])
        ax[j].set_xticks([7,8,9,10])
        default_tick_params(ax[j])

    ax[0].set_ylim([1e-3,1.5e-1])
    ax[1].set_ylim([1e0,1.3e1])
    ax[1].tick_params(which='both',labelleft=False,labelright='True')

    ax[0].text(-.01,.71,r'$\rho_\mathrm{hop}$',transform=ax[0].transAxes,ha='right',va='center',fontsize=16)
    ax[1].text(1.01,.5,r'$r_\mathrm{hop}$',transform=ax[1].transAxes,ha='left',va='center',fontsize=16)

    ax[0].text(.54,-.01,r'$1/T$',transform=ax[0].transAxes,ha='center',va='top',fontsize=16)
    ax[1].text(.54,-.01,r'$1/T$',transform=ax[1].transAxes,ha='center',va='top',fontsize=16)

    ax[0].text(.05,.05,r'$(\mathrm{a})$',transform=ax[0].transAxes,ha='left',va='bottom',fontsize=16)
    ax[1].text(.95,.05,r'$(\mathrm{b})$',transform=ax[1].transAxes,ha='right',va='bottom',fontsize=16)
    
    
    plt.savefig(fname,bbox_inches='tight')
    
    return

def figS13(fname):
    """
    fig:ghop_Fourier
    """
    
    # function definitions #
    
    def get_data(T,N=10000,nslices=None):
        rootpath = '{}/T{:.4f}/N{:d}'.format(host_info.TwoTimeOut,T,N)
        suffix = 'normdense_T{:.4f}_N{:d}_t10000_c5_R{:.3g}_F3_k12'.format(T,N,get_xi_dyn(T))
        
        if nslices is None:
            if T>.1:
                suffix += '_x0.1000'
            data = np.load("{}/g_hop_Fourier_{}.npz".format(rootpath,suffix),allow_pickle=True)
            rmids = .5*(data['redges'][1:]+data['redges'][:-1])
            
            g = data['gofr']/data['gofr'][0].reshape((1,-1))
        else:
            for slice_i in range(nslices):
                data = np.load("{}/g_hop_Fourier_{}_slice{:d}.npz".format(rootpath,suffix,slice_i),allow_pickle=True)
                if slice_i==0:
                    rmids = .5*(data['redges'][1:]+data['redges'][:-1])
                    
                    hist = data['hist']
                    g = np.nan_to_num(data['g_tilde'])*data['hist'].reshape((1,-1))
                else:
                    hist += data['hist']
                    g += np.nan_to_num(data['g_tilde'])*data['hist'].reshape((1,-1))
                    
        nanmask = hist==0; anmask = np.invert(nanmask)
        g[:,anmask] /= hist[anmask].reshape((1,-1))
        g[:,nanmask] = 0.
        
        g[1:,anmask] *= 2. # convert complex Fourier coeffs c_k to real-space Fourier coeff c_k + c_{-k} for k>0
        
        return rmids,g
    
    def get_segments(y):
        assert not np.any(np.isnan(y))

        sgns = y>=0.
        i_change = np.insert(np.arange(1,y.size)[np.invert(sgns[:-1]*sgns[1:])],0,0)
        sgns = 2*(sgns.astype(int))-1

        ret = []
        for i in range(len(i_change)-1):
            x0,x1 = i_change[i],i_change[i+1]+1
            sgn = sgns[x0]
            ret.append([x0,x1,sgn])
        ret.append([i_change[-1],y.size,sgns[-1]])

        return ret
    
    def loglog_signed(axis,x,y,c,alpha=1.):
        linestyles = [':','-']
        
        for segment in get_segments(y):
            x0,x1,sgn = segment
            sgn_i = (1+sgn)//2
            axis.loglog(x[x0:x1],np.abs(y[x0:x1]),c=c,ls=linestyles[sgn_i],alpha=alpha)
            
        return axis
    
    def label_coords(axis):
        axis.annotate('',xy=(1.,-.01),xytext=(0.5,-.01),xycoords='axes fraction',textcoords='axes fraction',
                       arrowprops=dict(arrowstyle="->"))
        axis.text(1.,-.02,r'$\mathrm{extension}$',transform=inax.transAxes,ha='right',va='top',fontsize=16)
        axis.annotate('',xy=(-.01,1.),xytext=(-.01,.5),xycoords='axes fraction',textcoords='axes fraction',
                       arrowprops=dict(arrowstyle="->"))
        axis.text(-.02,1.,r'$\mathrm{compression}$',transform=inax.transAxes,ha='right',va='top',fontsize=16,rotation=90)
        return
    
    ########################
    
    bw,bh = 2.4,1.7
    inbw = 3.2
    ncols,nrows = 2,2
    wspace,hspace,inwspace = 0.,0.,.15
    
    fig_w,fig_h = (ncols+(ncols-1)*wspace)*bw,(nrows+(nrows-1)*hspace)*bh
    fig,ax = plt.subplots(figsize=[fig_w,fig_h],ncols=ncols,nrows=nrows)
    fig.subplots_adjust(0,0,1,1,wspace=wspace,hspace=hspace)
    inax = fig.add_axes([1+inwspace*bw/fig_w,.5-.5*inbw/fig_h,inbw/fig_w,inbw/fig_h],projection='polar')
    
    # Fourier coeffs: T=0.1 and 0.15 #
    
    for j,T in enumerate([.1,.15]):
        nslices = 10
        char = 'a' if j==0 else 'b'
        rmids,gtilde = get_data(T,nslices=nslices)
        ks = 2*np.arange(1,gtilde.shape[0])

        for g_i,c in zip(gtilde[1:3],['C1','C2']):
            loglog_signed(ax[0,j],rmids[g_i!=0.],g_i[g_i!=0.],c)
        for g_i in gtilde[3:]:
            loglog_signed(ax[0,j],rmids[g_i!=0.],g_i[g_i!=0.],'grey',alpha=.5)

        zframe = pd.DataFrame(gtilde[1:,:],index=ax_to_label(ks,'k',fmt='d'),columns=ax_to_label(rmids,'r'))
        np.savez('{}/FigData/figS13/S13{}.npz'.format(datpath,char),
                 T='{:.3f}'.format(T),k=ks,r=rmids,ghatofr=zframe)
        zframe.to_pickle('{}/FigData/figS13/S13{}.pkl'.format(datpath,char))
        
        default_tick_params(ax[0,j])
        ax[0,j].set_xlim([.8,15])
        ax[0,j].set_ylim([1e-5,.3])
        ax[0,j].set_yticks([1e-4,1e-3,1e-2,1e-1])
        ax[0,j].text(.5,.91,r'$T={:.3f}$'.format(T),transform=ax[0,j].transAxes,ha='center',va='center',fontsize=16)
    ax[0,0].set_ylabel(r'$\hat{\tilde{g}}_k$',fontsize=16)
    ax[0,1].tick_params(labelleft=False)
    
    # Fourier coeffs: k=2 and 4 #
    
    cmap = cm.get_cmap('cividis')
    norm = mpl.colors.LogNorm(vmin=.1,vmax=.15)
    
    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])
    zk2,zk4 = None,None
    for T_i,T in enumerate(Ts):
        nslices = 4 if T==.105 else 10
        rmids,gtilde = get_data(T,nslices=nslices)

        if zk2 is None:
            zk2,zk4 = (np.zeros((Ts.size,rmids.size)) for z_i in range(2))
        zk2[T_i,:] = gtilde[1,:]
        zk4[T_i,:] = gtilde[2,:]

        for j,k in enumerate([2,4]):
            loglog_signed(ax[1,j],rmids[gtilde[k//2]!=0.],gtilde[k//2,gtilde[k//2]!=0.],cmap(norm(T)))

    zframe = pd.DataFrame(zk2,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
    np.savez('{}/FigData/figS13/S13c.npz'.format(datpath),
             T=Ts,r=rmids,ghoptildehat_2=zframe)
    zframe.to_pickle('{}/FigData/figS13/S13c.pkl'.format(datpath))

    zframe = pd.DataFrame(zk4,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(rmids,'r'))
    np.savez('{}/FigData/figS13/S13d.npz'.format(datpath),
             T=Ts,r=rmids,ghoptildehat_4=zframe)
    zframe.to_pickle('{}/FigData/figS13/S13d.pkl'.format(datpath))
    

    for j,k in enumerate([2,4]):
        default_tick_params(ax[1,j])
        ax[0,j].set_xlim([.8,15])
        ax[1,j].set_ylim([1e-6,1e0])
        ax[1,j].text(.5,.9,r'$k={:d}$'.format(k),transform=ax[1,j].transAxes,ha='center',va='center',fontsize=16)
        ax[1,j].text(.5,-.01,r'$r$',transform=ax[1,j].transAxes,ha='center',va='top',fontsize=16)
    ax[1,0].text(-.01,.5,r'$\hat{\tilde{g}}_{\mathrm{hop},k}$',transform=ax[1,0].transAxes,ha='right',va='center',fontsize=16)
    ax[1,j].tick_params(labelleft=False)


    # polar plots #
    
    rticks = np.arange(1,11)
    rticklabels = [r'${:g}$'.format(r) for r in rticks]
    rticklabels[::2] = np.repeat('',len(rticklabels[::2]))
    ths = np.linspace(0.,2.*np.pi,500,endpoint=False)
    Ts = np.array([.1,.105,.11,.115,.12,.13,.14,.15])

    z = np.zeros((Ts.size,ths.size))

    for T_i,T in enumerate(Ts):
        nslices = 4 if T==.105 else 10
        rmids,gtildehat = get_data(T,nslices=nslices)
        for r in rticks:
            r_i = np.searchsorted(rmids,r)
            if np.abs(rmids[r_i]-r) > np.abs(rmids[r_i-1]-r):
                r_i -= 1

            gtilde = np.zeros(ths.size)
            for k_i in range(3):
                gtilde += gtildehat[k_i,r_i]*np.cos(2.*k_i*ths)

            z[T_i,:] = gtilde

            inax.plot(ths,r*gtilde,c=cmap(norm(T)),lw=2)

    zframe = pd.DataFrame(z,index=ax_to_label(Ts,'T',fmt='.3f'),columns=ax_to_label(ths,'theta'))
    np.savez('{}/FigData/figS13/S13e.npz'.format(datpath),
             T=Ts,theta=ths,ghoptilde_smooth=z)
    zframe.to_pickle('{}/FigData/figS13/S13e.pkl'.format(datpath))
            
    plt.sca(inax); plt.grid(True)
    default_tick_params(inax)
    inax.set_axisbelow(False)
    inax.set_xticklabels([])
    inax.set_yticks(rticks)
    inax.set_yticklabels(rticklabels)
    label_coords(inax)
    
    for axis,char in zip(ax.reshape(-1),string.ascii_lowercase):
        axis.text(.95,.95,r'$(\mathrm{{{}}})$'.format(char),transform=axis.transAxes,ha='right',va='top',fontsize=16)
    inax.text(1.,1.,r'$(\mathrm{e})$',transform=inax.transAxes,ha='right',va='top',fontsize=16)
    
    plt.savefig(fname,bbox_inches='tight')

    return

if __name__ == '__main__':
    fs = [fig1,fig2,fig3,fig4,figS1,figS2,figS3,figS4,figS5,figS6,figS7,figS8,figS9,figS10,figS11,figS12,figS13]
    names = ['strain_polar.pdf',
             'strain_decay.pdf',
             'ghop_polar.pdf',
             'crossover.pdf',
             'xi_dyn.pdf',
             'gofr.pdf',
             'goftheta.pdf',
             'rmsd.pdf',
             'dr2corr.pdf',
             'Dt.pdf',
             'xiFL.pdf',
             'disjoint.pdf',
             'noise_floor.pdf',
             'bootstrap.pdf',
             'lbound.pdf',
             'rhop.pdf',
             'ghop_Fourier.pdf']

    for i,(f,name) in enumerate(zip(fs,names)):
        label = 'fig{:d}'.format(i+1) if i<4 else 'figS{:d}'.format(i-3)
        try:
            f(name)
            print(label,flush=True)
        except:
            print("{} failed.".format(label),flush=True)
