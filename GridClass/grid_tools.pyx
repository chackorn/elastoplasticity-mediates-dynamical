# distutils: language = c++
# distutils: sources = GridClass.cpp
# cython: language_level=3

import numpy as np
cimport numpy as np

# misc. functions #

def array_of_lists(shape):
    ret = np.empty(shape,dtype=object)
    for index in np.ndindex(shape):
        ret[index] = []
    return ret

def PBC(dp,L):
    dp[dp<-0.5*L] += L
    dp[dp>=0.5*L] -= L
    return dp

def get_dminmax(retcoeff=False):
    # based on the values dbar=1, dmin/dmax=0.449 and sqrt(d2bar-dbar2)/dbar2 = 0.23 given in the report
    A = -(1+0.23**2)/np.log(0.449)
    dmax = A*(1./0.449 - 1)
    dmin = 0.449*dmax
    if retcoeff:
        return (A,dmin,dmax)
    else:
        return (dmin,dmax)

from GridClass cimport GridClass as CPPGridClass

cdef class GridClass:
    cdef CPPGridClass *c_grid

    def __cinit__(self, int N, int d, double L, double cutoff,normed=False):
        if not normed:
            self.c_grid = new CPPGridClass(N,d,L,cutoff,int(L/cutoff),0)
        else:
            self.c_grid = new CPPGridClass(N,d,L,cutoff,int(L/(get_dminmax()[1]*cutoff)),1)

    def assign_to_boxes(self,ps=None):
        if not ps is None:
            self.ps_memview = np.ravel(ps+.5*self.c_grid.L)
            self.c_grid.assign_to_boxes(&self.ps_memview[0]);
        else:
            self.c_grid.assign_to_boxes()

    def get_nlist(self,int i):
        nlist = np.zeros(self.c_grid.get_z(i),dtype=np.int32)
        cdef int[::1] nlist_memview = nlist
        self.c_grid.get_nlist(i,&nlist_memview[0])
        return nlist

    def nlist_global(self):
        cdef int i

        cdef int z
        cdef int z_tot = 0
        for i in range(self.c_grid.N):
            z_tot += self.c_grid.get_z(i)
            
        nlist = np.zeros(z_tot,dtype=np.int32)
        cdef int[::1] nlist_memview = nlist

        nlist_inds = np.zeros(self.c_grid.N+1,dtype=np.int32)

        nlist_inds[0] = 0
        for i in range(self.c_grid.N):
            z = self.c_grid.get_z(i)
            nlist_inds[i+1] = nlist_inds[i]+z
            if z>0: # need this to avoid issues in case the last few particles are neighbourless
                self.c_grid.get_nlist(i,&nlist_memview[nlist_inds[i]])
                
        return (nlist,nlist_inds)

    def fetch_E(self):
        E = np.zeros((self.c_grid.N,self.c_grid.d,self.c_grid.d))
        cdef double[::1] E_memview = np.ravel(E)
        self.c_grid.fetch_E(&E_memview[0])
        return E

    def calc_E(self,p1):  # position at time t1
        cdef double[::1] p1_memview = np.ravel(p1)
        E = np.zeros((self.c_grid.N,self.c_grid.d,self.c_grid.d))
        cdef double[::1] E_memview = np.ravel(E)
        self.c_grid.calc_E(&p1_memview[0],&E_memview[0])
        return E

    def calc_D2min(self,p1):  # position at time t1
        cdef double[::1] p1_memview = np.ravel(p1)
        D2min = np.zeros(self.c_grid.N)
        cdef double[::1] D2min_memview = D2min
        self.c_grid.calc_D2min(&p1_memview[0],&D2min_memview[0])
        return D2min

    def get_CR_displacement(self,dp):
        dp = PBC(dp,self.L)
        cdef double[::1] dp_memview = np.ravel(dp)
        dp_CR = np.zeros(self.c_grid.N*self.c_grid.d)
        cdef double[::1] dp_CR_memview = dp_CR
        self.c_grid.get_CR_displacement(&dp_memview[0],&dp_CR_memview[0])
        return dp_CR.reshape((self.c_grid.N,self.c_grid.d))

    def find_local_extrema(self,f):
        cdef double[::1] f_memview = f
        min_mask = np.ones(self.c_grid.N,dtype=np.int32)
        max_mask = np.ones(self.c_grid.N,dtype=np.int32)
        cdef int[::1] min_mask_memview = min_mask
        cdef int[::1] max_mask_memview = max_mask
        self.c_grid.find_local_extrema(&f_memview[0],&min_mask_memview[0],&max_mask_memview[0])
        return (min_mask.astype(bool),max_mask.astype(bool))

    cdef double[::1] ps_memview,diams_memview

    # Attribute access
    @property
    def N(self):
        return self.c_grid.N
    @N.setter
    def N(self,N):
        self.c_grid.N = N

    @property
    def d(self):
        return self.c_grid.d
    @d.setter
    def d(self,d):
        self.c_grid.d = d

    @property
    def L(self):
        return self.c_grid.L
    @L.setter
    def L(self,L):
        self.c_grid.L = L

    @property
    def cutoff(self):
        return self.c_grid.cutoff
    @cutoff.setter
    def cutoff(self,cutoff):
        self.c_grid.cutoff = cutoff

    @property
    def position(self):
        return np.asarray(self.ps_memview).reshape((self.N,self.d))-.5*self.c_grid.L
    @position.setter
    def position(self,ps):
        self.ps_memview = np.ravel(ps+.5*self.c_grid.L)
        self.c_grid.set_position(&self.ps_memview[0])

    @property
    def diameter(self):
        return np.asarray(self.diams_memview)
    @diameter.setter
    def diameter(self,diams):
        self.diams_memview = np.ravel(diams)
        self.c_grid.set_diameter(&self.diams_memview[0])

    def __dealloc__(self):
        del self.c_grid
