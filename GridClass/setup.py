from distutils.core import setup,Extension
from Cython.Build import cythonize
import numpy

#setup(
#    ext_modules=[
#        Extension("grid_tools", ["grid_tools.cpp"],
#                  include_dirs=[numpy.get_include()]),
#    ],
#)
setup(
    ext_modules=cythonize("grid_tools.pyx"),
    include_dirs=[numpy.get_include(),"/opt/packages/eigen/3.3.7/include/eigen3"]
)
