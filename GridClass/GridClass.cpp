#include "GridClass.h"
#include <stdlib.h>
#include <cmath>
#include <Eigen/SVD>

using namespace std;
using namespace Eigen;

namespace grid_tools {
  int int_pow(int i, int d) {  // integer power for small exponents d
    int ret = 1;
    for (int n=0;n<d;n++) ret *= i;
    return ret;
  }

  // ParticleClass //
  particle::particle(int i_in,double diam_in,vector<double> r_in) :
                     d(r_in.size()),i(i_in),diam(diam_in),original(this),r(r_in),z(0),
                     rijrij(d*d),sijrij(d*d),E(d*d) {}
  particle::particle(particle* p_ptr) :   // copy constructor for cloning //
                     d(p_ptr->d),i(p_ptr->i),diam(p_ptr->diam),original(p_ptr),r(p_ptr->r),z(p_ptr->z),
                     rijrij(d*d),sijrij(d*d),E(d*d) {}
  void particle::get_E() {
    // We want to solve a system E*A = B for X, so solve A^T E^T = B^T for E^T, then transpose //
    MatrixXd A_T(d,d),B_T(d,d);
    for (int m=0;m<d;++m) {
      for (int n=0;n<d;++n) {
        A_T(m,n) = rijrij[d*n+m]; B_T(m,n) = sijrij[d*n+m];
      }
    }

    JacobiSVD<MatrixXd> svd(A_T,ComputeFullU|ComputeFullV);
    MatrixXd E_T = svd.solve(B_T);

    for (int m=0;m<d;++m) {
      for (int n=0;n<d;++n) {
        E[d*m+n] = E_T(n,m);
      }
    }
  }
  int particle::id() const { return i; }
  double particle::diameter() const { return diam; }
  particle::~particle(){}

  // BoxClass //
  box::box(){}
  box::~box(){}

  // GridClass //
  int GridClass::i_from_vec(vector<int>& in_vec) {
    int ret = 0;
    for (int m=0;m<d;++m) ret += int_pow(bpr+2,m)*in_vec[m];
    return ret;
  }
  void GridClass::vec_from_i(int i,vector<int>& in_vec) { // box coords in [i,j,k] format given unrolled coord
    for (int m=0;m<d;++m) in_vec[m] = i/int_pow(bpr+2,m) % (bpr+2);
    return;
  }
  void GridClass::pos_from_vec(vector<int>& in_vec,vector<double>& in_pos) { // get position of corner of box
    for (int m=0;m<d;++m) in_pos[m] = bw*in_vec[m];
    return;
  }
  GridClass::GridClass(int N_in, int d_in, double L_in, double cutoff_in, int bpr_in, int nflag_in) :
                       N(N_in),d(d_in),L(L_in),cutoff(cutoff_in),cut_sq(cutoff_in*cutoff_in),normed_flag(nflag_in),bpr(bpr_in),
                       bw(L/bpr_in),boxes(int_pow(bpr_in+2,d_in)),index_vector(d_in) {

    //assert system large enough for box method //
    if (bpr < 3){
      cout << "ERROR: need at least 3 boxes per row for box method to work." << endl;
      exit(1);
    }

    // loop over box indices //
    for (size_t i=0;i<boxes.size();++i) {
      vec_from_i(i,index_vector);
      vector<double> p1(d); pos_from_vec(index_vector,p1); // position of corner of box i

      // see if i corresp. to inner (genuine) or outer (clone) box //
      bool inflag = true;
      for (int m=0;m<d;++m) {
        if ((index_vector[m]==0)||(index_vector[m]==bpr+1)) {
          inflag = false;
          break;
        }
      }

      if (inflag) {
        inners.push_back(&boxes[i]);

        // populate neighbour list of genuine box i //
        for (int j=0;j<(int_pow(3,d)-1)/2;++j) { // only need to consider half the neighbours
          vector<int> shifted_vector(index_vector);
          for (int m=0;m<d;++m)
            shifted_vector[m] += 1-(static_cast<int>(j/int_pow(3,m)) % 3);  // index offset
          boxes[i].nlist.push_back(&boxes[i_from_vec(shifted_vector)]);
        }
      }

      else if (index_vector.back() > 0){ // don't need to clone bottom plane/line
        outers.push_back(&boxes[i]);
        at_xedge.push_back((index_vector[0]==0)||(index_vector[0]==bpr+1));

        // get index of box that this clone is cloning //
        vector<int> index_vector_0(index_vector); // stores index vector of original vector
        for (int m=0;m<d;m++)
          index_vector_0[m] = ((((index_vector_0[m]-1) % bpr) + bpr) % bpr) + 1;
        originals.push_back(&boxes[i_from_vec(index_vector_0)]);

        // calculate shift vectors for outer boxes (what pos of copied particles must be shifted by)
        vector<double> p0(d); pos_from_vec(index_vector_0,p0); // position of corner of original box
        vector<double> shift(d);
        for (int m=0;m<d;++m) {
          shift[m]=p1[m]-p0[m];
        }
        cshifts.push_back(shift);
      }

    }

    // initialise position vector (of vectors) //
    for (int i=0;i<N;++i) {
      vector<double> r(d);
      particles.push_back(particle(i,1.,r));
    }

    // fix "original" pointer which has been dereferenced due to push_back //
    for (vector<particle>::iterator pit=particles.begin();pit!=particles.end();++pit)
      pit->original = &(*pit);

  }
  void GridClass::pair_up(particle* p1,particle* p2) {
    // populate neighbour lists //
    double dr_m,dr_sq=0.;
    for (int m=0;m<d;++m) {
      dr_m = p2->r[m]-p1->r[m];
      dr_sq += dr_m*dr_m;
    }
    if (dr_sq<cut_sq) {     // one-sided pairing to halve looping over neighbours
      p1->original->nlist.push_back(p2); p2->original->mlist.push_back(p1);
      ++(p1->original->z); ++(p2->original->z);
    }
  }
  void GridClass::pair_up_normed(particle* p1,particle* p2) {
    // populate neighbour lists //
    double dr_m,dr_sq=0.;
    double denom = .5*(p1->diam+p2->diam)*(1.-0.2*fabs(p2->diam-p1->diam)); // normalisation factor
    for (int m=0;m<d;++m) {
      dr_m = p2->r[m]-p1->r[m];
      dr_sq += dr_m*dr_m;
    }
    dr_sq /= denom*denom;
    if (dr_sq<cut_sq) {     // one-sided pairing to halve looping over neighbours
      p1->original->nlist.push_back(p2); p2->original->mlist.push_back(p1);
      ++(p1->original->z); ++(p2->original->z);
    }
  }
  void GridClass::set_position(const double *r){
    // set positions //
    for (int i=0;i<N;++i)
      for (int m=0;m<d;++m)
        particles[i].r[m] = r[d*i+m];
  }
  void GridClass::get_position(double *r){
    for (int i=0;i<N;++i)
      for (int m=0;m<d;++m)
        r[d*i+m] = particles[i].r[m];
  }
  void GridClass::set_diameter(const double *diams){
    for (int i=0;i<N;++i)
      particles[i].diam = diams[i];
  }
  void GridClass::assign_to_boxes(){
    void (GridClass::*pu_ptr)(particle*,particle*); // pointer to pair_up function (either normed or not normed) //
    pu_ptr = (normed_flag==0) ? &GridClass::pair_up : &GridClass::pair_up_normed;

    // clear clones and boxes for fresh assignment //
    clones.clear();
    for (vector<box>::iterator bit=boxes.begin();bit!=boxes.end();++bit)
      bit->plist.clear();

    // assign particles to inner (original) boxes //
    for (vector<particle>::iterator pit=particles.begin();pit!=particles.end();++pit) {
      for (int m=0;m<d;++m)
        index_vector[m] = 1+static_cast<int>(pit->r[m]/bw);
      int box_index = i_from_vec(index_vector);
      pit->bx = &boxes[box_index];
      boxes[box_index].plist.push_back(&(*pit));
    }

    // make clones //
    for (size_t j=0;j<originals.size();++j) {
      for (vector<particle*>::iterator ptrpit=originals[j]->plist.begin();
                                       ptrpit!=originals[j]->plist.end();++ptrpit) {
        clones.push_back(particle(*ptrpit));
        for (int m=0;m<d;++m)
          clones.back().r[m] += cshifts[j][m];
      }
    }

    // assign clones to outer (clone) boxes //
    for (vector<particle>::iterator cit=clones.begin();cit!=clones.end();++cit) {
      for (int m=0;m<d;++m)
        index_vector[m] = 1+static_cast<int>(floor(cit->r[m]/bw));
      int box_index = i_from_vec(index_vector);
      cit->bx = &boxes[box_index];
      boxes[box_index].plist.push_back(&(*cit));
    }

    // set neighbour lists //
    for (vector<box*>::iterator ibit=inners.begin();ibit!=inners.end();++ibit) {    // iterate over particles in same box
      box* bit = *ibit;                                                             //  without double-counting
      if (bit->plist.size()==0)
        continue;      // skip box if empty to avoid issues with plist.end()-1
      for (vector<particle*>::iterator pit=bit->plist.begin();pit!=bit->plist.end()-1;++pit)
        for (vector<particle*>::iterator p2it=pit+1;p2it!=bit->plist.end();++p2it)
          (this->*pu_ptr)(*pit,*p2it);
    }

    for (vector<box*>::iterator ibit=inners.begin();ibit!=inners.end();++ibit) {    // iterate over particles in neighbouring boxes
      box* bit = *ibit;
      for (vector<particle*>::iterator pit=bit->plist.begin();pit!=bit->plist.end();++pit)
        for (vector<box*>::iterator nit=bit->nlist.begin();nit!=bit->nlist.end();++nit)
          for (vector<particle*>::iterator p2it=(*nit)->plist.begin();p2it!=(*nit)->plist.end();++p2it)
            (this->*pu_ptr)(*pit,*p2it);
    }
  }
  void GridClass::assign_to_boxes(const double *r) {
    set_position(r);
    assign_to_boxes();
  }
  size_t GridClass::get_z(const int i) { return particles[i].z; }
  void GridClass::get_nlist(const int i,int* n) {
    for (size_t j=0;j<particles[i].nlist.size();++j)
      n[j] = particles[i].original->nlist[j]->id();
    for (size_t j=0;j<particles[i].original->mlist.size();++j)
      n[particles[i].original->nlist.size()+j] = particles[i].original->mlist[j]->id();
  }
  void GridClass::calc_E(const double* r1,double* E_out) { // r1 is position at time t + Delta t
    double rij_m,sij_m,rij_n,rijrij_mn,sijrij_mn; // rij is vector from i to j a time t. sij is same at time t + Delta t

    // calculate rijrij and sijrij from originals //
    int i,j; vector<particle>::iterator p1; particle* p2;
    for(i=0,p1=particles.begin();p1!=particles.end();++i,++p1) {
      for (vector<particle*>::iterator p2it=p1->nlist.begin();p2it!=p1->nlist.end();++p2it) {
        p2 = *p2it;  j=p2->id();
        for (int m=0;m<d;++m) {
          rij_m = p2->r[m]-p1->r[m];
          sij_m = r1[d*j+m]-r1[d*i+m]; if (sij_m<-.5*L) sij_m += L; else if (sij_m>=.5*L) sij_m -= L;

          for (int n=0;n<d;++n) {
            rij_n = p2->r[n]-p1->r[n];

            rijrij_mn = rij_m*rij_n; sijrij_mn = sij_m*rij_n;

            p1->rijrij[d*m+n] += rijrij_mn; p2->original->rijrij[d*m+n] += rijrij_mn;
            p1->sijrij[d*m+n] += sijrij_mn; p2->original->sijrij[d*m+n] += sijrij_mn;
          }
        }
      }
    }

    // calculate E for each particle //
    vector<particle>::iterator pit;
    for(i=0,pit=particles.begin();pit!=particles.end();++i,++pit) {
      pit->get_E();
      for (int m=0;m<d;++m)
        for (int n=0;n<d;++n)
          E_out[d*d*i+d*m+n] = pit->E[d*m+n];
    }
  }
  void GridClass::fetch_E(double* E_out) {
    // fetch E for each particle (without re-calculating it) //
    int i;
    vector<particle>::iterator pit;
    for(i=0,pit=particles.begin();pit!=particles.end();++i,++pit)
      for (int m=0;m<d;++m)
        for (int n=0;n<d;++n)
          E_out[d*d*i+d*m+n] = pit->E[d*m+n];
  }
  void GridClass::calc_D2min(const double* r1,double* D2min) { // r1 is position at time t + Delta t
    double rij_m,sij_m,rij_n,rijrij_mn,sijrij_mn; // rij is vector from i to j a time t. sij is same at time t + Delta t

    // calculate rijrij and sijrij from originals //
    int i,j; vector<particle>::iterator p1; particle* p2;
    for(i=0,p1=particles.begin();p1!=particles.end();++i,++p1) {
      for (vector<particle*>::iterator p2it=p1->nlist.begin();p2it!=p1->nlist.end();++p2it) {
        p2 = *p2it;  j=p2->id();
        for (int m=0;m<d;++m) {
          rij_m = p2->r[m]-p1->r[m];
          sij_m = r1[d*j+m]-r1[d*i+m]; if (sij_m<-.5*L) sij_m += L; else if (sij_m>=.5*L) sij_m -= L;

          for (int n=0;n<d;++n) {
            rij_n = p2->r[n]-p1->r[n];

            rijrij_mn = rij_m*rij_n; sijrij_mn = sij_m*rij_n;

            p1->rijrij[d*m+n] += rijrij_mn; p2->original->rijrij[d*m+n] += rijrij_mn;
            p1->sijrij[d*m+n] += sijrij_mn; p2->original->sijrij[d*m+n] += sijrij_mn;
          }
        }
      }
    }

    // calculate E for each particle //
    for (vector<particle>::iterator pit=particles.begin();pit!=particles.end();++pit)
      pit->get_E();

    double Di_m,Dj_m;
    for(i=0,p1=particles.begin();p1!=particles.end();++i,++p1) {
      Di_m=Dj_m=0;
      for (vector<particle*>::iterator p2it=p1->nlist.begin();p2it!=p1->nlist.end();++p2it) {
        p2 = *p2it;  j=p2->id();
        for (int m=0;m<d;++m) {
          rij_m = p2->r[m]-p1->r[m];
          sij_m = r1[d*j+m]-r1[d*i+m]; if (sij_m<-.5*L) sij_m += L; else if (sij_m>=.5*L) sij_m -= L;

          Di_m = sij_m;  Dj_m = -sij_m;
          for (int n=0;n<d;++n) {
            rij_n = p2->r[n]-p1->r[n];
            Di_m -= p1->E[d*m+n]*rij_n;  Dj_m -= p2->original->E[d*m+n]*(-rij_n);
          }
          D2min[i] += Di_m*Di_m;  D2min[j] += Dj_m*Dj_m;
        }
      }
    }

    // rescale D2min by number of neighbours //
    for(i=0,p1=particles.begin();p1!=particles.end();++i,++p1)  D2min[i] /= p1->z;

  }

  void GridClass::get_CR_displacement(const double* dr,double* dr_CR) {
    for (int i=0;i<N*d;++i) dr_CR[i] = 0.;

    int i,j; vector<particle>::iterator p1; particle* p2;
    for(i=0,p1=particles.begin();p1!=particles.end();++i,++p1) {
      for (vector<particle*>::iterator p2it=p1->nlist.begin();p2it!=p1->nlist.end();++p2it) {
        p2 = *p2it;  j = p2->id();
        for (int m=0;m<d;++m) {
          dr_CR[d*i+m] += dr[d*j+m]; dr_CR[d*j+m] += dr[d*i+m];
        }
      }
    }

    for (int i=0;i<N;++i)
      for (int m=0;m<d;++m)
        dr_CR[d*i+m] = dr[d*i+m]-dr_CR[d*i+m]/particles[i].z;
    
  }
  void GridClass::find_local_extrema(const double* field,int* minima,int* maxima) {
    int i,j; vector<particle>::iterator p1; particle* p2;
    for(i=0,p1=particles.begin();p1!=particles.end();++i,++p1) {
      for (vector<particle*>::iterator p2it=p1->nlist.begin();p2it!=p1->nlist.end();++p2it) {
        p2 = *p2it;  j = p2->id();

        if (field[i] < field[j]) {
          maxima[i] = 0;
          minima[j] = 0;
        }
        else if (field[i] > field[j]) {
          minima[i] = 0;
          maxima[j] = 0;
        }
      }
    }
  }
  GridClass::~GridClass() {}
}
