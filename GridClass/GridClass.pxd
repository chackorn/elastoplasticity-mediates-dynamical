cdef extern from "GridClass.h" namespace "grid_tools":
  cdef cppclass GridClass:
    GridClass(int,int,double,double,int,int) except +
    int N,d
    double L,cutoff
    void set_position(const double*)
    void get_position(double*)
    void set_diameter(const double*)
    void assign_to_boxes()
    void assign_to_boxes(const double*)
    size_t get_z(const int)
    void get_nlist(const int,int*)
    void calc_E(const double*,double*)
    void fetch_E(double*)
    void calc_D2min(const double*,double*)
    void get_CR_displacement(const double*,double*)
    void find_local_extrema(const double*,int*,int*)
