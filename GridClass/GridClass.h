#ifndef GRIDCLASS_H
#define GRIDCLASS_H

#include <vector>
#include <iostream>

int int_pow(int i, int d); // integer power for small exponents d

namespace grid_tools {
  class box;

  class particle{
  private:
    int d; // num dimensions
    int i; // particle id
    double s,s0,s1,s2,coeff; // dummy variables
    void pbc_pos(double&); // periodic shift of particle position
  public:
    particle(int,double,std::vector<double>);
    particle(particle*);
    double diam; // particle diameter
    int z; // coordination number
    std::vector<double> r; // position vector
    std::vector<double> rijrij;
    std::vector<double> sijrij;
    std::vector<double> E;
    box* bx; // pointer to box containing this particle
    std::vector<particle*> nlist; // list of neighbouring particles
    std::vector<particle*> mlist; // mirror list of neighbouring particles (such that full nlist = nlist+mlist)
    particle* original; // pointer to original version of this particle (or itself, if not a clone)
    void get_E();
    int id() const;
    double diameter() const;
    ~particle();
  };

  class box{
  public:
    box();
    std::vector<particle*> plist; // pointers to particles in box
    std::vector<box*> nlist; // list of neighbouring boxes
    std::vector<std::vector<double> > nshifts;
    ~box();
  };

  class GridClass {
  private:
    int normed_flag;

    std::vector<int> index_vector;
    int i_from_vec(std::vector<int>&);
    void vec_from_i(int i,std::vector<int>&);
    void pos_from_vec(std::vector<int>&,std::vector<double>&);

    std::vector<box> boxes;  // boxes in full grid
    std::vector<box*> inners; // pointers to inner boxes
    std::vector<box*> outers; // pointers to outer (clone) boxes
    std::vector<box*> originals; // pointer to original box for each clone box
    std::vector<std::vector<double> > cshifts; // shift vectors for each clone box for pbc and L-E
    std::vector<particle> clones;
    std::vector<bool> at_xedge;
    
    std::vector<particle> particles;
    void pair_up(particle* p1,particle* p2);
    void pair_up_normed(particle* p1,particle* p2);
  public:
    GridClass(int N, int d, double L, double cutoff, int bpr, int normed_flag);
    int N,d,bpr;
    double L,cutoff,cut_sq,bw;
    void set_position(const double*);
    void get_position(double*);
    void set_diameter(const double*);
    void assign_to_boxes();
    void assign_to_boxes(const double*);
    size_t get_z(const int);
    void get_nlist(const int,int*);
    void calc_E(const double*,double*);
    void fetch_E(double*);
    void calc_D2min(const double*,double*);
    void get_CR_displacement(const double*,double*);
    void find_local_extrema(const double*,int*,int*);
    ~GridClass();
  };
}

#endif
