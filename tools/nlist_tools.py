"""
nlist_tools.py: improve flexibility and robustness by writing functions
to handle neighbour lists that are independent of the grid class
(for now, still dependent on grid class when generating the neighbour lists, though)
"""

import host_info
import sys; sys.path.extend(host_info.import_paths)
import grid_tools
from numba_tools import get_dp_com

import numpy as np
from numba import njit

### PBC functions ###

def PBC(dp,L): # PBCs for a 2D array
    dp[dp<-L/2.] += L
    dp[dp>=L/2.] -= L
    return dp

@njit
def PBC_i(dp,L):
    d = dp.size
    for m in range(d):
        if dp[m] < -L/2.:
            dp[m] += L
        elif dp[m] >= L/2.:
            dp[m] -= L
    return

#####################


### nlist manipulation functions ###

def get_nlist(p,L,R):
    N,d = p.shape
    grid = grid_tools.GridClass(N,d,L,R); grid.assign_to_boxes(p)
    return grid.nlist_global() # returns (nlist,nlist_inds)

## merge nlists (numba + wrapper)

@njit
def merge_nlists_numba(nlist0,nlist_inds0,nlist1,nlist_inds1,nlist,nlist_inds):
    for i in range(nlist_inds.size-1):
        z0,z1 = nlist_inds0[i+1]-nlist_inds0[i],nlist_inds1[i+1]-nlist_inds1[i]
        nlist_inds[i+1] = nlist_inds[i]+z0+z1

        nlist0_i = nlist0[nlist_inds0[i]:nlist_inds0[i+1]]
        nlist1_i = nlist1[nlist_inds1[i]:nlist_inds1[i+1]]

        nlist[nlist_inds[i]:nlist_inds[i]+z0] = nlist0_i
        nlist[nlist_inds[i]+z0:nlist_inds[i+1]] = nlist1_i

        nlist_i = np.unique(nlist[nlist_inds[i]:nlist_inds[i+1]])

        nlist_inds[i+1] = nlist_inds[i]+nlist_i.size
        nlist[nlist_inds[i]:nlist_inds[i+1]] = nlist_i

    return

def merge_nlists(nlist0,nlist_inds0,nlist1,nlist_inds1):
    """
    merge two neighbourlists into one
    """
    merged_nlist,merged_nlist_inds = np.zeros(nlist0.size+nlist1.size,dtype=int),np.zeros(nlist_inds0.size,dtype=int)
    merge_nlists_numba(nlist0,nlist_inds0,nlist1,nlist_inds1,merged_nlist,merged_nlist_inds)
    return (merged_nlist[:merged_nlist_inds[-1]],merged_nlist_inds)

##

@njit
def shrink_nhood_numba(p,L,R2,nlist0,nlist_inds0,nlist,nlist_inds):
    for i in range(nlist_inds.size-1):
        nlist_inds[i+1] = nlist_inds[i]
        for n_i in range(nlist_inds0[i],nlist_inds0[i+1]):
            j = nlist0[n_i]
            dp = p[j]-p[i]; PBC_i(dp,L)
            dist2 = np.sum(dp**2)
            if dist2 < R2:
                nlist[nlist_inds[i+1]] = j
                nlist_inds[i+1] += 1

    return

def shrink_nhood(p,L,R,nlist0,nlist_inds0):
    """
    Given nlists for a neighbourhood R0, get nlists for a neighbourhood R<R0
    """
    nlist,nlist_inds = np.zeros(nlist0.size,dtype=int),np.zeros(nlist_inds0.size,dtype=int)
    shrink_nhood_numba(p,L,R*R,nlist0,nlist_inds0,nlist,nlist_inds)
    return (nlist[:nlist_inds[-1]],nlist_inds)

##

def get_nlist_merged(p0,p1,L,R):
    """
    Get and merge neighbour lists for configurations p0 and p1
    """
    nlist0,nlist_inds0 = get_nlist(p0,L,R)
    nlist1,nlist_inds1 = get_nlist(p1,L,R)
    return merge_nlists(nlist0,nlist_inds0,nlist1,nlist_inds1)

####################################


### calc E ###

def calc_E(p0,dp,L,nlist,nlist_inds):
    """
    Calculate best-fit displacement gradient tensor E,
    with particle neighbourhoods defined in (nlist,nlist_inds)
    """

    N,d = p0.shape

    p1 = PBC(p0+dp,L)

    rijrij,sijrij,E = np.zeros((N,d,d)),np.zeros((N,d,d)),np.zeros((N,d,d))
    rijrij_and_sijrij(p0,p1,L,nlist,nlist_inds,rijrij,sijrij)

    E = np.swapaxes(np.linalg.solve(rijrij,np.swapaxes(sijrij,1,2)),1,2)

    return E

@njit
def rijrij_and_sijrij(p0,p1,L,nlist,nlist_inds,rijrij,sijrij):
    N,d = p0.shape[0],p0.shape[1]
    for i in range(N):
        for n_i in range(nlist_inds[i],nlist_inds[i+1]):
            j = nlist[n_i]

            if j > i:
                rij = p0[j]-p0[i]; PBC_i(rij,L) # disp vector from i to neighbour j at time t0
                sij = p1[j]-p1[i]; PBC_i(sij,L) # disp vector from i to neighbour j at time t1

                for m in range(d):

                    # get rijrij (upper triangle) #
                    rijrij_mm = rij[m]*rij[m]
                    rijrij[i,m,m] += rijrij_mm; rijrij[j,m,m] += rijrij_mm
                    for n in range(m+1,d):
                        rijrij_mn = rij[m]*rij[n]
                        rijrij[i,m,n] += rijrij_mn; rijrij[j,m,n] += rijrij_mn

                    # get sijrij #
                    for n in range(d):
                        sijrij_mn = sij[m]*rij[n]
                        sijrij[i,m,n] += sijrij_mn; sijrij[j,m,n] += sijrij_mn

        # fill rijrij (lower triangle) #
        for m in range(1,d):
            for n in range(m):
                rijrij[i,m,n] = rijrij[i,n,m]

    return

#

def calc_E_normed(p0,dp,L,nlist,nlist_inds,density=False):
    """
    Calculate best-fit displacement gradient tensor E,
    with particle neighbourhoods defined in (nlist,nlist_inds),
    but weight by either the distance r from central particle i
    (when density=False) or by r**(d+1)
    """

    N,d = p0.shape

    p1 = PBC(p0+dp,L)

    rijrij,sijrij,E = np.zeros((N,d,d)),np.zeros((N,d,d)),np.zeros((N,d,d))
    rijrij_and_sijrij_normed(p0,p1,L,nlist,nlist_inds,rijrij,sijrij,density)

    E = np.swapaxes(np.linalg.solve(rijrij,np.swapaxes(sijrij,1,2)),1,2)

    return E

@njit
def rijrij_and_sijrij_normed(p0,p1,L,nlist,nlist_inds,rijrij,sijrij,density):
    N,d = p0.shape[0],p0.shape[1]
    for i in range(N):
        for n_i in range(nlist_inds[i],nlist_inds[i+1]):
            j = nlist[n_i]

            if j > i:
                rij = p0[j]-p0[i]; PBC_i(rij,L) # disp vector from i to neighbour j at time t0
                sij = p1[j]-p1[i]; PBC_i(sij,L) # disp vector from i to neighbour j at time t1

                Z = np.sqrt(np.sum(rij**2))
                if density:
                    Z *= Z**(d/2.)

                rij /= Z
                sij /= Z

                for m in range(d):

                    # get rijrij (upper triangle) #
                    rijrij_mm = rij[m]*rij[m]
                    rijrij[i,m,m] += rijrij_mm; rijrij[j,m,m] += rijrij_mm
                    for n in range(m+1,d):
                        rijrij_mn = rij[m]*rij[n]
                        rijrij[i,m,n] += rijrij_mn; rijrij[j,m,n] += rijrij_mn

                    # get sijrij #
                    for n in range(d):
                        sijrij_mn = sij[m]*rij[n]
                        sijrij[i,m,n] += sijrij_mn; sijrij[j,m,n] += sijrij_mn

        # fill rijrij (lower triangle) #
        for m in range(1,d):
            for n in range(m):
                rijrij[i,m,n] = rijrij[i,n,m]

    return

##############
