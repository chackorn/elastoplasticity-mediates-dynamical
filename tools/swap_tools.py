"""
Miscellaneous functions designed specifically to help with 2d swap systems
"""

import numpy as np
import host_info
import sys; sys.path.extend(host_info.import_paths)
from numba_tools import njit,array_of_lists

def rotation_matrix(th):
    c,s = np.cos(th),np.sin(th)
    return np.array([[c,-s],[s,c]])

def PBC(dp,L):
    dp[dp<-0.5*L] += L
    dp[dp>=0.5*L] -= L
    return dp

def get_dminmax(retcoeff=False):
    # based on the values dbar=1, dmin/dmax=0.449 and sqrt(d2bar-dbar2)/dbar2 = 0.23 given in the report
    A = -(1+0.23**2)/np.log(0.449)
    dmax = A*(1./0.449 - 1)
    dmin = 0.449*dmax
    if retcoeff:
        return (A,dmin,dmax)
    else:
        return (dmin,dmax)

T_onset = .250*(1./1.01)**6

@njit
def get_potential_energy(p,diams,L,nlist,nlist_inds,r_cut=1.25,eps=.2):
    # get swap potential energy field
    N,d = p.shape[0],p.shape[1]

    r_cut2 = r_cut*r_cut
    c_0,c_2,c_4 = -28*r_cut**-12,48*r_cut**-14,-21*r_cut**-16

    ret = np.zeros(N)
    for i in range(N):
        num_neighbs = nlist_inds[i+1]-nlist_inds[i]
        for n_i in range(nlist_inds[i],nlist_inds[i+1]):
            j = nlist[n_i]

            rij = p[j]-p[i]
            for m in range(d):
                if rij[m] < -L/2.:
                    rij[m] += L
                elif rij[m] >= L/2.:
                    rij[m] -= L

            rij2 = np.sum(rij**2)
            dij = .5*(diams[i]+diams[j])*(1.-eps*abs(diams[i]-diams[j]))
            r2 = rij2/(dij*dij)

            if r2 < r_cut2:
                r4=r2*r2; rneg12=1./(r4*r4*r4);
                ret[i] += rneg12 + c_0 + c_2*r2 + c_4*r4
    ret /= 2. # divide by 2 so sum over particle potential e yields system potential e

    return ret

@njit
def get_stress(p,diams,L,nlist,nlist_inds,r_cut=1.25,eps=.2):
    # get swap potential energy field
    N,d = p.shape[0],p.shape[1]

    r_cut2 = r_cut*r_cut
    c_0,c_2,c_4 = -28*r_cut**-12,48*r_cut**-14,-21*r_cut**-16

    ret = np.zeros((N,d,d))
    for i in range(N):
        num_neighbs = nlist_inds[i+1]-nlist_inds[i]
        for n_i in range(nlist_inds[i],nlist_inds[i+1]):
            j = nlist[n_i]

            rij = p[j]-p[i]
            for m in range(d):
                if rij[m] < -L/2.:
                    rij[m] += L
                elif rij[m] >= L/2.:
                    rij[m] -= L

            rij2 = np.sum(rij**2)
            dij = .5*(diams[i]+diams[j])*(1.-eps*abs(diams[i]-diams[j]))
            r2 = rij2/(dij*dij) 

            if r2 < r_cut2:
                r = np.sqrt(r2)
                r3=r2*r; rneg13=1./(r*r3*r3*r3);
                coeff = (-12*rneg13 + 2*c_2*r + 4*c_4*r3)*r/rij2
                for m1 in range(d):
                    for m2 in range(d):
                        ret[i,m1,m2] +=  coeff*rij[m1]*rij[m2]
    ret *= .5*N/L**d # mult by half num density so average over particle stress yields system stress

    return ret

def get_tau_alpha(T):
    eps = 1e-4
    if np.abs((T-0.3)/0.3)<eps:
        t_a = 7.58
    elif np.abs((T-0.2)/0.2)<eps:
        t_a = 27.54
    elif T < 0.2:
        t_a = 7.71641848227137e-06*np.exp(2.6200334860148335/T)
    else:
        print("Error: tau_alpha not known for T not in {0.12,0.15}")
        raise ValueError
    return t_a

def get_a_ast(T):
    eps = 1e-4
    if np.abs((T-0.3)/0.3)<eps:
        a_ast = 0.2872984833353664
    elif np.abs((T-0.2)/0.2)<eps:
        a_ast = 0.26101572156825364
    elif np.abs((T-0.15)/0.15)<eps:
        a_ast = 0.3311311214825912
    elif np.abs((T-0.14)/0.14)<eps:
        a_ast = 0.3019951720402017
    elif np.abs((T-0.13)/0.13)<eps:
        a_ast = 0.3019951720402017
    elif np.abs((T-0.12)/0.12)<eps:
        a_ast = 0.2754228703338167
    elif np.abs((T-0.115)/0.115)<eps:
        a_ast = 0.3019951720402016
    elif np.abs((T-0.11)/0.11)<eps:
        a_ast = 0.19054607179632474
    elif np.abs((T-0.105)/0.105)<eps:
        a_ast = 0.15848931924611134
    elif np.abs((T-0.1)/0.1)<eps:
        a_ast = 0.20892961308540398
    else:
        print("Error: a_ast not known for T not in {0.12,0.15}")
        raise ValueError
    return a_ast

def get_cage_R(T):
    eps = 1e-4
    if np.abs((T-0.3)/0.3)<eps:
        R = 1.
    elif np.abs((T-0.2)/0.2)<eps:
        R = 1.5
    elif np.abs((T-0.15)/0.15)<eps:
        R = 2.
    elif np.abs((T-0.14)/0.14)<eps:
        R = 2.5
    elif np.abs((T-0.13)/0.13)<eps:
        R = 2.5
    elif np.abs((T-0.12)/0.12)<eps:
        R = 3.
    elif np.abs((T-0.115)/0.115)<eps:
        R = 3.5
    elif np.abs((T-0.11)/0.11)<eps:
        R = 4.
    elif np.abs((T-0.105)/0.105)<eps:
        R = 4.5
    elif np.abs((T-0.1)/0.1)<eps: # this one is fake...
        R = 5.
    else:
        print("Error: cage_R not known for T not in {0.12,0.15}")
        raise ValueError
    return R

def get_xi_dyn(T):
    if T>=T_onset:
        return 0.
    else:
        m,c = 2.153735467848861,-9.144934392742554
        return m*(1./T - 1./T_onset)

@njit
def get_pc2_numba(pc2_in,types,pvals):
    for i,type_i in enumerate(types):
        pc2_in[i] = pvals[type_i]**2
    return

def get_pc2(types,pvals):
    pc2 = np.zeros(types.size)
    get_pc2_numba(pc2,types,pvals)
    return pc2

def time_to_pid_based(data):
    """
    Inverse procedure to pid_to_time_based
    """

    outdict = dict(data)

    pids = np.empty(0,dtype=int)
    for pidlist in data['pid']:
        pidlist = np.array(pidlist)
        pids = np.unique(np.append(pids,pidlist.astype(int)))
    outdict['pid'] = pids

    steps,times = array_of_lists(pids.size),array_of_lists(pids.size)
    for step,time,pidlist in zip(data['step'],data['time'],data['pid']):
        pidlist = np.array(pidlist,dtype=int)
        for pid_index in np.searchsorted(pids,pidlist):
            steps[pid_index].append(step); times[pid_index].append(time)
    for i in range(pids.size):
        steps[i],times[i] = np.array(steps[i]),np.array(times[i])
    outdict['step'],outdict['time'] = steps,times

    return outdict

def pid_to_time_based(data):
    """
    Convert from format where 'pid' lists all particles and 'time' lists all event times for each particle
    to format where 'time' lists all times in which an event occurs, and 'pid' lists all particles involved
    at the given time
    """

    outdict = dict(data)

    steps,times = np.empty(0,dtype=int),np.empty(0,dtype=int)
    for steplist,timelist in zip(data['step'],data['time']):
        steps,unique_indices = np.unique(np.append(steps,steplist),return_index=True)
        times = np.append(times,timelist)[unique_indices]
    outdict['step'],outdict['time'] = steps,times

    pids = array_of_lists(steps.size)
    for pid,steplist in zip(data['pid'],data['step']):
        for step_index in np.searchsorted(steps,steplist):
            pids[step_index].append(pid)
    for i in range(steps.size):
        pids[i] = np.array(pids[i])
    outdict['pid'] = pids

    return outdict

def read_config_file(inpath):
    with open(inpath,'r') as f:
        N = int(next(f))
        L = float(next(f))

    inarr = np.loadtxt(inpath,skiprows=2)
    ps,diams = inarr[:,:2],inarr[:,-1]
    ps -= L/2.

    assert (N==ps.shape[0]) and (N==diams.size)
    return {"ps" : ps, "diams" : diams, "L" : L}

def read_xyz_file(inpath):
    assert inpath[-4:]=='.xyz'
    return np.genfromtxt(inpath,skip_header=2,usecols=(1,2))

class FixNumHops:
    def __init__(self,T_x,T,N,t,c,num_cnfs=202,num_Vs=400,xx=False):
        self.Tx = T_x
        self.T = T

        nhops_Tx = np.load('{root}/T{T:.4f}/N{N:d}/hopsperrun_T{T:.4f}_N{N:d}_t{t:d}_c{c:.3g}_R{R:.3g}.npz'.format(
                           root=host_info.TwoTimeOut,T=T_x,N=N,t=t,c=c,R=get_xi_dyn(T_x)))['hopsperrun']
        nhops_T = np.load('{root}/T{T:.4f}/N{N:d}/hopsperrun_T{T:.4f}_N{N:d}_t{t:d}_c{c:.3g}_R{R:.3g}.npz'.format(
                          root=host_info.TwoTimeOut,T=T,N=N,t=t,c=c,R=get_xi_dyn(T)))['hopsperrun']
        frac = nhops_Tx/nhops_T
        if xx:
            assert nhops_Tx > 1
            frac *= (nhops_Tx-1)/(nhops_T-1)

        ntrajs = frac*num_cnfs*num_Vs
        self.quotient = int(ntrajs/num_cnfs)
        self.remainder = ntrajs - num_cnfs*self.quotient

    def num_Vs(self,cnf_i):
        return self.quotient+1 if cnf_i < self.remainder else self.quotient
