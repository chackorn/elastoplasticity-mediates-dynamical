import os
import subprocess
import shlex
import numpy as np
import host_info

class FIREClass:
    def __init__(self,diams,L,etol=0.,ftol=1e-8,maxiter=10000,maxeval=10000,outpath=None):
        self.L = L
        self.diams = diams
        self.N = diams.size

        self.etol = etol
        self.ftol = ftol
        self.maxiter = maxiter
        self.maxeval = maxeval

        if outpath is None:
            cwd = os.getcwd()
            if 'SLURM_ARRAY_JOB_ID' in os.environ and 'SLURM_ARRAY_TASK_ID' in os.environ:
                outpath = "{}/tmp_{}_{}".format(cwd,os.environ['SLURM_ARRAY_JOB_ID'],os.environ['SLURM_ARRAY_TASK_ID'])
            elif 'SLURM_JOB_ID' in os.environ:
                outpath = "{}/tmp_{}".format(cwd,os.environ['SLURM_JOB_ID'])
            else:
                outpath = "{}/tmp_{:012d}".format(cwd,np.random.randint(int(1e12)))
        self.outpath = outpath
        subprocess.call(shlex.split("mkdir -p {}".format(self.outpath)))
####

    def __del__(self):
        subprocess.call(shlex.split("rm -r {}".format(self.outpath)))
####

    def init_config_from_data(self,ps):
        vs = np.zeros((self.N,2))

        xlo,ylo,zlo = np.repeat(-self.L/2.,3)
        xhi,yhi,zhi = np.repeat(self.L/2.,3)

        pos_string = ""
        for i,(diam,p) in enumerate(zip(self.diams,ps)):
            pos_string += "  {atom_id:d} {atom_type:d} {diam:g} {x:g} {y:g} {z:g} 0 0 0\n".format(atom_id=1+i,atom_type=1,diam=diam,
                                                                                                  x=p[0],y=p[1],z=0.)

        vel_string = ""
        for i,v in enumerate(vs):
            vel_string += "  {atom_id:d} {vx:g} {vy:g} {vz:g}\n".format(atom_id=1+i,vx=v[0],vy=v[1],vz=0.)

        argdict = {'N':self.N,'xlo':xlo,'ylo':ylo,'zlo':zlo,'xhi':xhi,'yhi':yhi,'zhi':zhi,'pos_string':pos_string,'vel_string':vel_string}

        with open("{outpath}/quench_config.txt".format(outpath=self.outpath),'w') as f:
            f.write("""\
# Input datafile for 2D polydisperse system

{N:d} atoms
1 atom types

{xlo:g} {xhi:g} xlo xhi
{ylo:g} {yhi:g} ylo yhi
{zlo:g} {zhi:g} zlo zhi

Masses

1 1 # type_I mass

Pair Coeffs # lj/swap

1 1.0 1.0 2.017 1.25 0.2

Atoms # charge

{pos_string}

Velocities

{vel_string}""".format(outpath=self.outpath,**argdict))
####

    def in_swap_from_data(self):
        with open("{outpath}/quench.swap".format(outpath=self.outpath),'w') as f:
            f.write("""\
atom_modify map array
  
dimension 2
variable        skin equal 0.2

units           lj
atom_style      charge
boundary        p p p
pair_style      lj/swap 2.02

read_data       quench_config.txt
reset_timestep  1         # don't dump initial config

pair_coeff      1 1 1.0 1.0 2.017 1.25 0.2

neighbor        ${{skin}} bin
neigh_modify    every 1 delay 0 check yes

timestep        0.1

dump            xyz1 all xyz {maxiter:d} {outpath}/quench_dump.xyz

min_style       fire
minimize        {etol:g} {ftol:g} {maxiter:d} {maxeval:d}

""".format(outpath=self.outpath,etol=self.etol,ftol=self.ftol,maxiter=self.maxiter,maxeval=self.maxeval))
####

    def quench(self,ps):
        self.init_config_from_data(ps)
        self.in_swap_from_data()

        with open("{outpath}/quench.swap".format(outpath=self.outpath)) as g:
            subprocess.call(shlex.split("{} -log none -screen none".format(host_info.lammps)),stdin=g,cwd=self.outpath)
        
        return np.genfromtxt("{outpath}/quench_dump.xyz".format(outpath=self.outpath),skip_header=2,usecols=tuple(np.arange(1,ps.shape[-1]+1)))
