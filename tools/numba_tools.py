import numpy as np
from numba import njit

####

def array_of_lists(shape): # not a numba tool, but...
    ret = np.empty(shape,dtype=object)
    for index in np.ndindex(shape):
        ret[index] = []
    return ret

@njit
def nlist_global_numba(p,R2,L):
    N,d = p.shape[0],p.shape[1]
    nlists = [[-1] for i in range(N)]
    zlist = np.array([0 for i in range(N)])
    
    for i in range(N):
        for j in range(i+1,N):
            r2 = 0.
            for m in range(d):
                r_m = p[j,m]-p[i,m]
                if r_m < -L/2.:
                    r_m += L
                if r_m >= L/2.:
                    r_m -= L
                r2 += r_m*r_m
            if r2 < R2:
                nlists[i].append(j); nlists[j].append(i)
                zlist[i] += 1; zlist[j] += 1
    return ([x[1:] for x in nlists],zlist)

def nlist_global_naive(p,R,L):
    N = p.shape[0]
    
    nlist_unravelled,zlist = nlist_global_numba(p,R**2,L)
    nlist_inds = np.zeros(N+1,dtype=int)
    nlist_inds[1:] = np.cumsum(zlist)

    nlist = np.zeros(np.sum(zlist),dtype=int)
    for i in range(N):
        nlist[nlist_inds[i]:nlist_inds[i+1]] = nlist_unravelled[i]
        
    return (nlist,nlist_inds)

##

@njit
def get_dp_com(dp,nlist,nlist_inds):
    N,d = dp.shape[0],dp.shape[1]
    ret = np.zeros(dp.shape)
    for i in range(nlist_inds.size-1):
        num_neighbs = nlist_inds[i+1]-nlist_inds[i]
        if num_neighbs>0:
            for m in range(d):
                mean = 0.
                for n_i in range(nlist_inds[i],nlist_inds[i+1]):
                    j = nlist[n_i]
                    mean += dp[j,m]
                mean /= num_neighbs
                ret[i,m] += mean
    return ret

##

@njit
def get_tuples(nlist,nlist_inds,pids):
    tuples = []
    for i in range(nlist_inds.size-1):
        for n_i in range(nlist_inds[i],nlist_inds[i+1]):
            j = nlist[n_i]
            tuples.append((pids[i],pids[j]))
    return tuples

##

@njit
def indices_to_mask_numba(indices,mask):
    for i in indices:
        mask[i] = 1
    return

def indices_to_mask(indices,N):
    mask = np.zeros(N,dtype=int)
    indices_to_mask_numba(indices,mask)
    return mask.astype(bool)

##

@njit
def mask_to_indices_numba(mask):
    indices = []
    for i in range(mask.size):
        if mask[i] != 0:
            indices.append(i)
    return indices

def mask_to_indices(mask):
    indices = mask_to_indices_numba(mask.astype(int))
    return np.array(indices)

##

@njit
def PBC_i(dp_i,L):
    d = dp_i.size
    for m in range(d):
        if dp_i[m]<-L/2.:
            dp_i[m] += L
        elif dp_i[m]>=L/2.:
            dp_i[m] -= L
    return dp_i

@njit
def calc_D2min(p1,p0,E,nlist,nlist_inds,L):
    N,d = p0.shape
    D2min = np.zeros(N)
    for i in range(N):
        num_neighbs = nlist_inds[i+1]-nlist_inds[i]
        if num_neighbs>0:
            for n_i in range(nlist_inds[i],nlist_inds[i+1]):
                j = nlist[n_i]
                rij0,rij1 = PBC_i(p0[j]-p0[i],L),PBC_i(p1[j]-p1[i],L)
                D = rij1 - E[i]@rij0
                D2min[i] += D@D
            D2min[i] /= num_neighbs
    return D2min

##

def update_hist(*args):
    assert len(args) > 1
    hist = args[-1]
    indices = np.ravel_multi_index(args[:-1],hist.shape) # indices in a flattened array
    hist += update_hist_numba(indices,np.zeros(hist.size,dtype=hist.dtype)).reshape(hist.shape)
    return

@njit
def update_hist_numba(indices,hist):
    for index in indices:
        hist[index] += 1
    return hist

def update_field(*args):
    assert len(args) > 2
    field_in,field_out = args[-2],args[-1]
    indices = np.ravel_multi_index(args[:-2],field_out.shape) # indices in a flattened array
    field_out += update_field_numba(indices,field_in,np.zeros(field_out.size,dtype=field_out.dtype)).reshape(field_out.shape)
    return

@njit
def update_field_numba(indices,field_in,field_out):
    for i,index in enumerate(indices):
        field_out[index] += field_in[i]
    return field_out

##

@njit
def correlate_field(field,p,L,redges2,hist,corr):
    """
    field is a length N array of floats (scalar field values)
    p is a shape (N,d) array of floats (particle position values)
    L is a float (box width)
    redges2 is a length M+1 array of floats (squared r bin edges)
    hist is a length N array of ints
    corr is a length N array of floats: < field(r)*field(r+dr) >_r
    """
    
    N,d = p.shape
    
    for i in range(N-1):
        for j in range(i+1,N):
            # squared separation between particles i and j #
            rij2 = 0.
            for m in range(d):
                rij_m = p[j,m]-p[i,m]
                if rij_m < -.5*L:       #
                    rij_m += L          # enforce
                if rij_m >= .5*L:       # PBCs
                    rij_m -= L          #
                rij2 += rij_m*rij_m
            
            if (redges2[0]<=rij2) and (rij2<redges2[-1]):
                rbin = np.searchsorted(redges2,rij2,side='right')-1
                hist[rbin] += 1
                corr[rbin] += field[i]*field[j]
    
    return

##

def parse_brackets(instring,types):
    assert instring[0]=='[' and instring[-1]==']'
    args = instring[1:-1].split(',')
    assert len(args)==len(types)
    return [typ(arg) for (arg,typ) in zip(args,types)]
