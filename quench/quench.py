import os
import subprocess
import shlex
import argparse
import sys
import host_info

import numpy as np
from numba import njit
import h5py

sys.path.extend(host_info.import_paths)
from grid_tools import PBC
import LAMMPS_FIRE

##### read input #####

# argparse block

parser = argparse.ArgumentParser()
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-C","--Cnf",type=int,
                    help='Index of configuration for the given temperature to use.\
                          Default: 1',
                    default=1)
parser.add_argument("-i","--inpath",
                    help='Path to directory containing trajectory file.\
                          Default: "{}/trajs_EQ_poly_SS_2D_N{{N:d}}_T{{T:.4f}}/Cnf-{{Cnf:d}}"'.format(host_info.TwoTimeTrajs),
                    default=None)
parser.add_argument("-o","--outpath",
                    help='Destination folder for generated files.\
                          Default: "{inpath}/quenched_position"',
                    default=None)
parser.add_argument("-e","--etol",type=float,
                    help="etol argument for LAMMPS FIRE algorithm. \
                          Default: 0.",
                    default=0.)
parser.add_argument("-f","--ftol",type=float,
                    help="ftol argument for LAMMPS FIRE algorithm. \
                          Default: 1e-8",
                    default=1e-8)
parser.add_argument("-E","--maxeval",type=int,
                    help="maxeval argument for LAMMPS FIRE algorithm. \
                          Default: 3000",
                    default=3000)
parser.add_argument("-I","--maxiter",type=int,
                    help="maxiter argument for LAMMPS FIRE algorithm. \
                          Default: 3000",
                    default=3000)
parser.add_argument("-u","--update",action='store_true',
                   help="This option sets a flag to only quench frames in {inpath}/position that don't have a corresponding\
                         frame in {inpath}/quenched_position.")
                   
args = parser.parse_args()

# unpack args

T = args.temperature
N = args.num_particles
Cnf = args.Cnf

if args.inpath is None:
    inpath = "{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}/Cnf-{:d}".format(host_info.TwoTimeTrajs,N,T,Cnf)
else:
    inpath = os.path.abspath(os.path.expanduser(args.inpath))

outpath = args.outpath
if outpath==None:
      outpath = "{}/quenched_position".format(inpath)

######################

sys_params = np.load('{}/sys_params.npz'.format(inpath))
d,N,L,steps,diams = int(sys_params['d']),int(sys_params['N']),float(sys_params['L']),sys_params['step'],sys_params['diameter']
quencher = LAMMPS_FIRE.FIREClass(diams,L,etol=args.etol,ftol=args.ftol,maxiter=args.maxiter,maxeval=args.maxeval)

if args.update and os.path.isdir(outpath):
    preexisting = np.sort([int(framefile[:-4]) for framefile in os.listdir(outpath) if framefile[-4:]==".npy"])
    if np.all(np.isin(preexisting,steps)): # no extra files in preexisting to indicate that preexisting is from a different run
        for step in steps:
            try: # see if every npy file that would be produced exists and can be read without error (in case of corruption)
                np.load("{}/{:d}.npy".format(outpath,step))
            except:
                break
        else: # if for loop above is never broken out of
            del quencher
            sys.exit("Exiting: Update flag set and output already exists.")

subprocess.call(shlex.split("rm -fr {}".format(outpath)))
subprocess.call(shlex.split("mkdir -p {}".format(outpath)))

for step in steps:
    p = np.load("{}/position/{:d}.npy".format(inpath,step))
    p = PBC(quencher.quench(p),L)

    np.save("{}/{:d}.npy".format(outpath,step),p.astype(np.single))

##### cleanup #####
del quencher
