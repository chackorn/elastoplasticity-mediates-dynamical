"""
perturbed_fields_Fourier.py: get Fourier series expansion coefficients for
change in fields across an event between times
t0=0,t1=Dt (default 10), as a function of distance and angle from the event,
in an Eulerian frame.
rij determined separately at times t0 and t1.
use pre-event quenched configurations for two-time quantities.
time-symmetric neighbourhoods (TS for ``time symmetric'') when calculating dp_com
and the local displacement gradient
and time-symmetric local displacement gradient
(average of forward grad u and reversed - grad u)
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

import numpy as np
from numba import njit

import grid_tools
from swap_tools import PBC,get_xi_dyn,get_potential_energy,get_stress,FixNumHops
from numba_tools import get_dp_com,mask_to_indices,parse_brackets,update_hist
from nlist_tools import get_nlist,merge_nlists,shrink_nhood
from nlist_tools import calc_E as calc_E_unnorm
from nlist_tools import calc_E_normed

##### function defs #####

def r_inds(r,r2edges):
    r2 = np.sum(r**2,axis=1)
    mask = (r2edges[0]<r2)*(r2<r2edges[-1]); r2=r2[mask]
    return [np.searchsorted(r2edges,r2,side='right')-1,mask]

@njit
def update_Fourier(r_indices,ths,field_in,field_out):
    num_coeffs = field_out.shape[0]
    for i,r_i in enumerate(r_indices):
        field_out[0,r_i] += field_in[i]
        for k_i in range(1,num_coeffs):
            field_out[k_i,r_i] += field_in[i]*np.cos(2.*k_i*ths[i])
    return field_out

#########################

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-r","--r_string",
                   help="Array of r bin edges.\
                         Input [r0,r1,num_rs] corresponds to array ayes = np.logspace(r0,r1,num_rs).\
                         Default: None (np.logspace(log10(0.5),log10(40),51))",
                   default=None)
parser.add_argument("-t","--Dt_i",type=int,
                   help="Rearrangement interval Dt in MD simulation frame units.\
                         Default: 10000",
                   default=10000)
parser.add_argument("-c","--criterion",type=float,
                    help="A hop has occurred if (dr/rmsd_plat)**2 > criterion.\
                          Default: 5.",
                    default=5.)
parser.add_argument("-R","--cage_radius",type=float,
                    help="Calculate displacements relative to the motion of the centre of mass of particles within a distance R\
                          from each particle (excluding the particle).\
                          Default: None (get cage_radius from get_xi_dyn)",
                    default=None)
parser.add_argument("-F","--FL_radius",type=float,
                    help="Calculate best-fit displacement gradient tensor for particles within distance FL_radius\
                          from each particle (excluding the particle).\
                          Default: 3.",
                    default=3.)
parser.add_argument("-k","--num_coeffs",type=int,
                   help="Find Fourier coefficients c_0,c_2,...,c_k where k=2*(num_coeffs-1).\
                         Default: 12",
                   default=12)
parser.add_argument("-u","--unidirectional",action='store_true',
                    help="This option sets a flag to only consider events in one direction in time.")
parser.add_argument("-p","--post",action='store_true',
                    help="This option sets a flag to use relative positions at t1 instead of t1.")
parser.add_argument("-b","--backwards",action='store_true',
                    help="This option sets a flag to use t0=Dt and t1=0 instead of the converse.")
parser.add_argument("-z","--norm",action='store_true',
                    help="This option sets a flag to use calc_E_normed with density=False instead of calc_E.")
parser.add_argument("-Z","--normdense",action='store_true',
                    help="This option sets a flag to use calc_E_normed with density=True instead of calc_E.")
parser.add_argument("-x","--fixnumhops",type=float,
                    help="Temperature such that the number of hops is constrained to being less than\
                          that found for that temperature.\
                          Default: Don't fix the number of hops.",
                    default=None)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations.',
                    default=None)
parser.add_argument("-V","--maxnumVs",type=int,
    		    help='Maximum number of isoconfigurational trajectories per configuration to average over.\
    		          Default: Use all available isoconfigurational trajectories.',
    		    default=None)
parser.add_argument("-s","--suffix",
                    help='Custom suffix to file name.\
                          Default: None (no extra suffix).',
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

T = args.temperature
N = args.num_particles
Dt_i = args.Dt_i
crit = args.criterion
R_c = args.cage_radius if args.cage_radius is not None else get_xi_dyn(T)
R_D = args.FL_radius
num_coeffs,ks = args.num_coeffs,2*np.arange(args.num_coeffs)
T_fix,fixflag = args.fixnumhops,(args.fixnumhops is not None)
maxnumVs = args.maxnumVs

prefix = ""
if args.normdense:
    prefix += "normdense_"
elif args.norm:
    prefix += "norm_"
if args.unidirectional:
    prefix += "unidir_"
if args.post:
    prefix += "post_"
if args.backwards:
    prefix += "reversed_" # I avoid calling it args.reversed because seq.reversed() is a standard python function
suffix = "_x{:.4f}".format(T_fix) if fixflag else ""
if args.suffix is not None:
    suffix += "_{}".format(args.suffix)
argstring = "{}T{:.4f}_N{:d}_t{:d}_c{:.3g}_R{:.3g}_F{:.3g}_k{:d}{}".format(prefix,T,N,Dt_i,crit,R_c,R_D,num_coeffs,suffix)

platpath = '{}/T{:.4f}/N{:d}/pc_plateau.npz'.format(host_info.TwoTimeOut,T,N)
inpath = '{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.TwoTimeTrajs,N,T)
outpath = '{}/T{:.4f}/N{:d}/perturbed_fields_Fourier_{}.npz'.format(host_info.TwoTimeOut,T,N,argstring)
platdata = np.load(platpath,allow_pickle=True)

rarg0,rarg1,rarg2 = [np.log10(0.5),np.log10(40.),51] if args.r_string is None else parse_brackets(args.r_string,[float,float,int])
redges=np.logspace(rarg0,rarg1,rarg2); r2edges=redges**2

if args.normdense:
    def calc_E(p0,dp,L,nlist,nlist_inds):
        return calc_E_normed(p0,dp,L,nlist,nlist_inds,density=True)
elif args.norm:
    def calc_E(p0,dp,L,nlist,nlist_inds):
        return calc_E_normed(p0,dp,L,nlist,nlist_inds,density=False)
else:
    calc_E = calc_E_unnorm

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

if fixflag:
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnfs[0])) if Vdir[:2]=='V-'])
    if maxnumVs is not None:
        Vs = Vs[:maxnumVs]
    fixer = FixNumHops(T_fix,T,N,Dt_i,crit,num_cnfs=cnfs.size,num_Vs=Vs.size)

##########################

##### array prep #####

# Note:
# For input fields (e.g. p0,V0),
# A0 means the value of A at time t=0, A1 that of A at time t=Dt
# however, for output/binned fields (e.g. hist0,V0_r)
# A0 means the value of A before (after if args.post==True) the rearrangement,
# A1 means that of A after (before if args.post==True) the rearrangement.
# (This is important to note when averaging over trajectories backwards in time)

hist0,hist1 = np.zeros(redges.size-1,dtype=int),np.zeros(redges.size-1,dtype=int) # hist for pre- and post-hop positions
zero_arr = np.zeros((num_coeffs,redges.size-1)) # initial array to make initialisation blocks below cleaner

# one-time quantities #
g0_r,g1_r,dg_out                         =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # g: pair distn (NOT g_hop)
S0_r,S1_r,dS_out                         =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # S: softness
V0_r,V1_r,dV_out                         =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # V: potential energy
press0_r,press1_r,dpress_out             =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # press: pressure -T:I
devstress0_r,devstress1_r,ddevstress_out =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # devstress: sqrt(2 T':T')
sxx0_r,sxx1_r,dsxx_out                   =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # sxx: T':e_ext e_ext
sxy0_r,sxy1_r,dsxy_out                   =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # sxy: T':e_ext e_com
syy0_r,syy1_r,dsyy_out                   =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # syy: T':e_com e_com

# two-time quantities #
iso_out,dev_out         = np.copy(zero_arr),np.copy(zero_arr)                   # iso: isotropic strain E:I;  dev: deviatoric strain sqrt(2 E':E')
exx_out,exy_out,eyy_out = np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # exx: shear strain E':e_ext e_ext
msd_out,msrd_out        = np.copy(zero_arr),np.copy(zero_arr)                   # ms(r)d: mean squared (relative) displacement

######################

nhops = 0 # number of hops (needed for g(r,theta))
for cnf_i,cnf in enumerate(cnfs):
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnf)) if Vdir[:2]=='V-'])
    if fixflag:
        Vs = Vs[:fixer.num_Vs(cnf_i)]
    elif maxnumVs is not None:
        Vs = Vs[:maxnumVs]

    for V in Vs:
        Vpath = '{}/Cnf-{:d}/V-{:d}'.format(inpath,cnf,V)
        sys_params = np.load('{}/sys_params.npz'.format(Vpath),allow_pickle=True)
        d,L,diams = int(sys_params['d']),float(sys_params['L']),sys_params['diameter']
        assert d==2

        types,num_species = np.searchsorted(platdata['dedges'][:-1],diams,side='right')-1,platdata['dedges'].size-1
        plateaus = np.zeros(N)
        for i in range(platdata['dedges'].size-1):
            plateaus[types==i] = platdata['p_c'][i]
        pc2 = plateaus**2

        p0 = PBC(np.load('{}/quenched_position/0.npy'.format(Vpath)).astype(float),L)
        p1 = PBC(np.load('{}/quenched_position/{:d}.npy'.format(Vpath,Dt_i)).astype(float),L)
        if args.backwards:
            p0,p1 = p1,p0

        # prep. cage-relative stuff #
        nlist_c0,nlist_inds_c0 = get_nlist(p0,L,R_c)
        nlist_c1,nlist_inds_c1 = get_nlist(p1,L,R_c)
        nlist_c,nlist_inds_c = merge_nlists(nlist_c0,nlist_inds_c0,nlist_c1,nlist_inds_c1)

        nlist_D0,nlist_inds_D0 = get_nlist(p0,L,R_D)
        nlist_D1,nlist_inds_D1 = get_nlist(p1,L,R_D)
        nlist_D,nlist_inds_D = merge_nlists(nlist_D0,nlist_inds_D0,nlist_D1,nlist_inds_D1)

        # calc. one-time quantities #

        # potential energy and stress
        nlist_i0,nlist_inds_i0 = shrink_nhood(p0,L,1.25,nlist_D0,nlist_inds_D0)   # ''interaction grid'':
        nlist_i1,nlist_inds_i1 = shrink_nhood(p1,L,1.25,nlist_D1,nlist_inds_D1)   # smallest grid that captures all interacting pairs

        V0 = get_potential_energy(p0,diams,L,nlist_i0,nlist_inds_i0)
        V1 = get_potential_energy(p1,diams,L,nlist_i1,nlist_inds_i1)

        Stress0 = get_stress(p0,diams,L,nlist_i0,nlist_inds_i0)
        Stress1 = get_stress(p1,diams,L,nlist_i1,nlist_inds_i1)

        Stress0_trace = np.trace(Stress0,axis1=1,axis2=2)
        Stress1_trace = np.trace(Stress1,axis1=1,axis2=2)

        Stress0_detraced = Stress0-np.multiply.outer(Stress0_trace,np.identity(d))/d
        Stress1_detraced = Stress1-np.multiply.outer(Stress1_trace,np.identity(d))/d

        press0,press1 = -Stress0_trace/d,-Stress1_trace/d
        dev_stress0,dev_stress1 = np.sqrt(2)*np.linalg.norm(Stress0_detraced,axis=(1,2)),np.sqrt(2)*np.linalg.norm(Stress1_detraced,axis=(1,2))

        # calc. two-time quantities #

        # displacement
        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)
        dp2=np.sum(dp**2,axis=1)

        # strain
        F = .5*(calc_E(p0,dp,L,nlist_D,nlist_inds_D)-calc_E(p1,-dp,L,nlist_D,nlist_inds_D)) # deformation gradient tensor F,
        E = .5*(F+np.swapaxes(F,1,2)) # linear strain tensor E                              # antisym w.r.t. time reversal

        E_trace = np.trace(E,axis1=1,axis2=2)
        E_detraced = E-np.multiply.outer(E_trace,np.identity(d))/d
        
        iso_strain = E_trace
        dev_strain = np.sqrt(2)*np.linalg.norm(E_detraced,axis=(1,2))

        # identify and loop over events #

        hopmask = dp2/pc2 > crit
        if not np.any(hopmask):
            continue

        hoppers = mask_to_indices(hopmask)
        nhops += hoppers.size
        if not args.unidirectional:
            nhops += hoppers.size

        lambda_plus = .5*(iso_strain[hopmask]+dev_strain[hopmask]) # largest eigenvalue
        thetas = np.arctan((lambda_plus-E[hopmask,0,0])/E[hopmask,0,1]) # angle of eigvec corresp. largest eigval

        for i,theta in zip(hoppers,thetas):
            # forwards in time #
            e_ext = np.array([np.cos(theta),np.sin(theta)])
            e_com = np.array([-e_ext[1],e_ext[0]])

            rij0,rij1 = PBC(p0-p0[i].reshape((1,d)),L),PBC(p1-p1[i].reshape((1,d)),L)
            drij=PBC(rij1-rij0,L); drij2=np.sum(drij**2,axis=1)

            if not args.post:                                 # separate dr0 and dr1
                dr0,dr1 = rij0,PBC(p1-p0[i].reshape((1,d)),L) # for Eulerian flow
            else:                                             # coordinates
                dr0,dr1 = PBC(p0-p1[i].reshape((1,d)),L),rij1 #

            r_eyes0,mask0 = r_inds(dr0,r2edges);  update_hist(r_eyes0,hist0)
            r_eyes1,mask1 = r_inds(dr1,r2edges);  update_hist(r_eyes1,hist1)

            th0,th1 = np.arctan2(np.abs(dr0[mask0]@e_com),np.abs(dr0[mask0]@e_ext)),np.arctan2(np.abs(dr1[mask1]@e_com),np.abs(dr1[mask1]@e_ext))

            # calc. rel. quantities
            s_xx0,s_xx1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_ext),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_ext)
            s_xy0,s_xy1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_com)
            s_yy0,s_yy1 = np.einsum('i,...ij,j',e_com,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_com,Stress1_detraced[mask1],e_com)
            e_xx = np.einsum('i,...ij,j',e_ext,E_detraced[mask0],e_ext)
            e_xy = np.einsum('i,...ij,j',e_ext,E_detraced[mask0],e_com)
            e_yy = np.einsum('i,...ij,j',e_com,E_detraced[mask0],e_com)

            # one-time quantities
            npairs0,npairs1 = np.sum(mask0),np.sum(mask1)
            update_Fourier(r_eyes0,th0,np.repeat(.5/np.pi,npairs0),g0_r); update_Fourier(r_eyes1,th1,np.repeat(.5/np.pi,npairs1),g1_r)
            update_Fourier(r_eyes0,th0,V0[mask0],V0_r);                   update_Fourier(r_eyes1,th1,V1[mask1],V1_r)
            update_Fourier(r_eyes0,th0,press0[mask0],press0_r);           update_Fourier(r_eyes1,th1,press1[mask1],press1_r)
            update_Fourier(r_eyes0,th0,dev_stress0[mask0],devstress0_r);  update_Fourier(r_eyes1,th1,dev_stress1[mask1],devstress1_r)
            update_Fourier(r_eyes0,th0,s_xx0,sxx0_r);                     update_Fourier(r_eyes1,th1,s_xx1,sxx1_r)
            update_Fourier(r_eyes0,th0,s_xy0,sxy0_r);                     update_Fourier(r_eyes1,th1,s_xy1,sxy1_r)
            update_Fourier(r_eyes0,th0,s_yy0,syy0_r);                     update_Fourier(r_eyes1,th1,s_yy1,syy1_r)

            # two-time quantities
            update_Fourier(r_eyes0,th0,dp2[mask0],msd_out);               update_Fourier(r_eyes0,th0,drij2[mask0],msrd_out)
            update_Fourier(r_eyes0,th0,iso_strain[mask0],iso_out);        update_Fourier(r_eyes0,th0,dev_strain[mask0],dev_out)
            update_Fourier(r_eyes0,th0,e_xx,exx_out); update_Fourier(r_eyes0,th0,e_xy,exy_out); update_Fourier(r_eyes0,th0,e_yy,eyy_out)

            ## backwards in time #
            if not args.unidirectional:
                theta += .5*np.pi
                e_ext,e_com = e_com,e_ext

                if not args.post:
                    # dr0 (dr1) here is after- (before-) hop relative position (Euler coords)
                    dr0,dr1 = PBC(p0-p1[i].reshape((1,d)),L),rij1
                else:
                    dr0,dr1 = rij0,PBC(p1-p0[i].reshape((1,d)),L)

                r_eyes0,mask0 = r_inds(dr0,r2edges);  update_hist(r_eyes0,hist1) # updates hist1 because hist1 is hist of post-hop rij
                r_eyes1,mask1 = r_inds(dr1,r2edges);  update_hist(r_eyes1,hist0) # updates hist0 because hist0 is hist of pre-hop rij
                                                                                 # (reversed if args.post == True)
                th0 = np.arctan2(np.abs(dr0[mask0]@e_com),np.abs(dr0[mask0]@e_ext))
                th1 = np.arctan2(np.abs(dr1[mask1]@e_com),np.abs(dr1[mask1]@e_ext))

                # calc. rel. quantities
                s_xx0,s_xx1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_ext),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_ext)
                s_xy0,s_xy1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_com)
                s_yy0,s_yy1 = np.einsum('i,...ij,j',e_com,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_com,Stress1_detraced[mask1],e_com)
                e_xx = np.einsum('i,...ij,j',e_ext,-E_detraced[mask1],e_ext)
                e_xy = np.einsum('i,...ij,j',e_ext,-E_detraced[mask1],e_com)
                e_yy = np.einsum('i,...ij,j',e_com,-E_detraced[mask1],e_com)

                # one-time quantities
                npairs0,npairs1 = np.sum(mask0),np.sum(mask1)
                update_Fourier(r_eyes0,th0,np.repeat(.5/np.pi,npairs0),g1_r); update_Fourier(r_eyes1,th1,np.repeat(.5/np.pi,npairs1),g0_r);
                update_Fourier(r_eyes0,th0,V0[mask0],V1_r);                   update_Fourier(r_eyes1,th1,V1[mask1],V0_r)
                update_Fourier(r_eyes0,th0,press0[mask0],press1_r);           update_Fourier(r_eyes1,th1,press1[mask1],press0_r)
                update_Fourier(r_eyes0,th0,dev_stress0[mask0],devstress1_r);  update_Fourier(r_eyes1,th1,dev_stress1[mask1],devstress0_r)
                update_Fourier(r_eyes0,th0,s_xx0,sxx1_r);                     update_Fourier(r_eyes1,th1,s_xx1,sxx0_r)
                update_Fourier(r_eyes0,th0,s_xy0,sxy1_r);                     update_Fourier(r_eyes1,th1,s_xy1,sxy0_r)
                update_Fourier(r_eyes0,th0,s_yy0,syy1_r);                     update_Fourier(r_eyes1,th1,s_yy1,syy0_r)

                # two-time quantities
                update_Fourier(r_eyes1,th1,dp2[mask1],msd_out);               update_Fourier(r_eyes1,th1,drij2[mask1],msrd_out)
                update_Fourier(r_eyes1,th1,-iso_strain[mask1],iso_out);       update_Fourier(r_eyes1,th1,dev_strain[mask1],dev_out)
                update_Fourier(r_eyes1,th1,e_xx,exx_out); update_Fourier(r_eyes1,th1,e_xy,exy_out); update_Fourier(r_eyes1,th1,e_yy,eyy_out)

    print("{:d}/{:d}".format(cnf,cnfs[-1]),flush=True)

#### normalisation ####
nanmask0,nanmask1 = hist0==0,hist1==0
anmask0,anmask1 = np.invert(nanmask0),np.invert(nanmask1)

# one-time quantities
nanmask=nanmask0+nanmask1; anmask=np.invert(nanmask)
for q,q0,q1 in [[dg_out,g0_r,g1_r],[dV_out,V0_r,V1_r],[dpress_out,press0_r,press1_r],[ddevstress_out,devstress0_r,devstress1_r],
                [dsxx_out,sxx0_r,sxx1_r],[dsxy_out,sxy0_r,sxy1_r],[dsyy_out,syy0_r,syy1_r]]:
    q0[:,anmask0] /= hist0[anmask0].reshape((1,-1)).astype(q0.dtype) # Fourier coefficient c_k = < f (theta_j) cos (k th_j) >_j for neighbours j
    q1[:,anmask1] /= hist1[anmask1].reshape((1,-1)).astype(q1.dtype) # (after enforcing th_j -> -th_j symmetry to exp(i k th_j))
    q[:,anmask] = q1[:,anmask]-q0[:,anmask]
    q[:,nanmask],q0[:,nanmask0],q1[:,nanmask1] = np.nan,np.nan,np.nan
g0_r[:,nanmask0],g1_r[:,nanmask1] = 0.,0. # g0_r,g1_r are zero rather than nan when hist=0

# two-time quantities
for q in [msd_out,msrd_out,iso_out,dev_out,exx_out,exy_out,eyy_out]:
    q[:,anmask0] /= hist0[anmask0].astype(q.dtype).reshape((1,-1))
    q[:,nanmask0] = np.nan

# At this point g0_r contains P_r(th) (Fourier expansion of angular distn P_r(th), s.t. at each r, Int_0^{2 pi} P_r(th) dth = 1).
# Multiply by 2 pi g(r), where g(r) is angular-averaged pair distn, to get Fourier series expansion of pair distn fn g(r,th).
dV = np.pi*(r2edges[1:]-r2edges[:-1])
g0ofr,g1ofr = (hist0/(nhops*dV))/(N/L**d),(hist1/(nhops*dV))/(N/L**d)
g0_r,g1_r = 2.*np.pi*g0ofr.reshape((1,-1))*g0_r,2.*np.pi*g1ofr.reshape((1,-1))*g1_r
dg_out[:,anmask] = g1_r[:,anmask]-g0_r[:,anmask]

#######################

# fin #
np.savez(outpath,
         redges=redges,ks=2*np.arange(num_coeffs),numhops=nhops,hist0=hist0,hist1=hist1,
         g0=g0_r,g1=g1_r,dg=dg_out,
         V0=V0_r,V1=V1_r,dV=dV_out,
         P0=press0_r,P1=press1_r,dP=dpress_out,
         sdev0=devstress0_r,sdev1=devstress1_r,dsdev=ddevstress_out,
         sxx0=sxx0_r,sxx1=sxx1_r,dsxx=dsxx_out,
         sxy0=sxy0_r,sxy1=sxy1_r,dsxy=dsxy_out,
         syy0=syy0_r,syy1=syy1_r,dsyy=dsyy_out,
         msd=msd_out,msrd=msrd_out,iso=iso_out,dev=dev_out,exx=exx_out,exy=exy_out,eyy=eyy_out,
         metadata=metadata)
