"""
perturbed_fields.py: get change in fields across an event between times
t0=0,t1=Dt (default 10), as a function of distance and angle from the event,
in an Eulerian frame.
rij determined separately at times t0 and t1.
use pre-event quenched configurations for two-time quantities.
time-symmetric neighbourhoods (TS for ``time symmetric'') when calculating dp_com
and the local displacement gradient
and time-symmetric local displacement gradient
(average of forward grad u and reversed - grad u)
"""

import os
import argparse
import host_info
import sys; sys.path.extend(host_info.import_paths)
from datetime import datetime

import numpy as np
from numba import njit

import grid_tools
import SoftClasses
from swap_tools import PBC,get_xi_dyn,get_potential_energy,get_stress,FixNumHops
from numba_tools import get_dp_com,mask_to_indices,parse_brackets,update_hist,update_field
from nlist_tools import get_nlist,merge_nlists,shrink_nhood
from nlist_tools import calc_E as calc_E_unnorm
from nlist_tools import calc_E_normed

##### function defs #####

def r_and_th_inds(r,theta,r2edges,thedges,retmask=False):
    r2 = np.sum(r**2,axis=1)
    e_ext = np.array([np.cos(theta),np.sin(theta)])
    e_com = np.array([-e_ext[1],e_ext[0]])
    
    mask = (r2edges[0]<r2)*(r2<r2edges[-1])
    r,r2 = r[mask],r2[mask]
    
    th = np.arctan2(np.abs(r@e_com),np.abs(r@e_ext))  # post-rotation angle w.r.t. ext axis

    if not retmask:
        return [np.searchsorted(r2edges,r2,side='right')-1,np.searchsorted(thedges,th,side='right')-1]
    else:
        return [np.searchsorted(r2edges,r2,side='right')-1,np.searchsorted(thedges,th,side='right')-1,mask]

#########################

##### Sort out input #####

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-r","--r_string",
                   help="Array of r bin edges.\
                         Input [r0,r1,num_rs] corresponds to array redges = np.logspace(r0,r1,num_rs).\
                         Default: None (np.logspace(log10(0.5),log10(40),51))",
                   default=None)
parser.add_argument("-t","--Dt_i",type=int,
                   help="Rearrangement interval Dt in MD simulation frame units.\
                         Default: 10000",
                   default=10000)
parser.add_argument("-c","--criterion",type=float,
                    help="A hop has occurred if (dr/rmsd_plat)**2 > criterion.\
                          Default: 5.",
                    default=5.)
parser.add_argument("-R","--cage_radius",type=float,
                    help="Calculate displacements relative to the motion of the centre of mass of particles within a distance R\
                          from each particle (excluding the particle).\
                          Default: None (get cage_radius from get_xi_dyn)",
                    default=None)
parser.add_argument("-F","--FL_radius",type=float,
                    help="Calculate best-fit displacement gradient tensor for particles within distance FL_radius\
                          from each particle (excluding the particle).\
                          Default: 3.",
                    default=3.)
parser.add_argument("-a","--num_th",type=int,
                   help="Bin half-circle into num_th bins.\
                         Default: 12",
                   default=12)
parser.add_argument("-u","--unidirectional",action='store_true',
                    help="This option sets a flag to only consider events in one direction in time.")
parser.add_argument("-p","--post",action='store_true',
                    help="This option sets a flag to use relative positions at t1 instead of t1.")
parser.add_argument("-b","--backwards",action='store_true',
                    help="This option sets a flag to use t0=Dt and t1=0 instead of the converse.")
parser.add_argument("-z","--norm",action='store_true',
                    help="This option sets a flag to use calc_E_normed with density=False instead of calc_E.")
parser.add_argument("-Z","--normdense",action='store_true',
                    help="This option sets a flag to use calc_E_normed with density=True instead of calc_E.")
parser.add_argument("-x","--fixnumhops",type=float,
                    help="Temperature such that the number of hops is constrained to being less than\
                          that found for that temperature.\
                          Default: Don't fix the number of hops.",
                    default=None)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-V","--maxnumVs",type=int,
                    help='Maximum number of isoconfigurational trajectories per configuration to average over.\
                          Default: Use all available isoconfigurational trajectories.',
                    default=None)
parser.add_argument("-s","--suffix",
                    help='Custom suffix to file name.\
                          Default: None (no extra suffix).',
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':str(datetime.now()),**vars(args)}

T = args.temperature
N = args.num_particles
Dt_i = args.Dt_i
crit = args.criterion
R_c = get_xi_dyn(T) if args.cage_radius is None else args.cage_radius
R_D = args.FL_radius
num_th = args.num_th
T_fix,fixflag = args.fixnumhops,(args.fixnumhops is not None)
maxnumVs = args.maxnumVs

prefix = ""
if args.normdense:
    prefix += "normdense_"
elif args.norm:
    prefix += "norm_"
if args.unidirectional:
    prefix += "unidir_"
if args.post:
    prefix += "post_"
if args.backwards:
    prefix += "reversed_" # I avoid calling it args.reversed because seq.reversed() is a standard python function
suffix = "_x{:.4f}".format(T_fix) if fixflag else ""
if args.suffix is not None:
    suffix += "_{}".format(args.suffix)
argstring = "{}T{:.4f}_N{:d}_t{:d}_c{:.3g}_R{:.3g}_F{:.3g}_a{:d}{}".format(prefix,T,N,Dt_i,crit,R_c,R_D,num_th,suffix)

platpath = '{}/T{:.4f}/N{:d}/pc_plateau.npz'.format(host_info.TwoTimeOut,T,N)
inpath = '{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}'.format(host_info.TwoTimeTrajs,N,T)
outpath = '{}/T{:.4f}/N{:d}/perturbed_fields_{}.npz'.format(host_info.TwoTimeOut,T,N,argstring)
platdata = np.load(platpath,allow_pickle=True)

rarg0,rarg1,rarg2 = [np.log10(0.5),np.log10(40.),51] if args.r_string is None else parse_brackets(args.r_string,[float,float,int])
redges=np.logspace(rarg0,rarg1,rarg2); r2edges=redges**2

thedges,dth = np.linspace(0,.5*np.pi,num_th+1,endpoint=True,retstep=True) # gradient and flow dirns are indistinguishable

if args.normdense:
    def calc_E(p0,dp,L,nlist,nlist_inds):
        return calc_E_normed(p0,dp,L,nlist,nlist_inds,density=True)
elif args.norm:
    def calc_E(p0,dp,L,nlist,nlist_inds):
        return calc_E_normed(p0,dp,L,nlist,nlist_inds,density=False)
else:
    calc_E = calc_E_unnorm

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=='Cnf-'])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

if fixflag:
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnfs[0])) if Vdir[:2]=='V-'])
    if maxnumVs is not None:
        Vs = Vs[:maxnumVs]
    fixer = FixNumHops(T_fix,T,N,Dt_i,crit,num_cnfs=cnfs.size,num_Vs=Vs.size)

##########################

##### array prep #####

# Note:
# For input fields (e.g. p0,V0),
# A0 means the value of A at time t=0, A1 that of A at time t=Dt
# however, for output/binned fields (e.g. hist0,V0_r)
# A0 means the value of A before (after if args.post==True) the rearrangement,
# A1 means that of A after (before if args.post==True) the rearrangement.
# (This is important to note when averaging over trajectories backwards in time)

hist0,hist1 = np.zeros((redges.size-1,num_th),dtype=int),np.zeros((redges.size-1,num_th),dtype=int) # hist for pre- and post-hop positions
zero_arr = np.zeros((redges.size-1,num_th)) # initial array to make initialisation blocks below cleaner

# one-time quantities #
V0_r,V1_r,dV_out                         =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # V: potential energy
press0_r,press1_r,dpress_out             =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # press: pressure -T:I
devstress0_r,devstress1_r,ddevstress_out =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # devstress: sqrt(2 T':T')
sxx0_r,sxx1_r,dsxx_out                   =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # sxx: T':e_ext e_ext
sxy0_r,sxy1_r,dsxy_out                   =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # sxy: T':e_ext e_com
syy0_r,syy1_r,dsyy_out                   =  np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # syy: T':e_com e_com

# two-time quantities #
iso_out,dev_out         = np.copy(zero_arr),np.copy(zero_arr)                   # iso: isotropic strain E:I;  dev: deviatoric strain sqrt(2 E':E')
exx_out,exy_out,eyy_out = np.copy(zero_arr),np.copy(zero_arr),np.copy(zero_arr) # exx: shear strain E':e_ext e_ext
msd_out,msrd_out        = np.copy(zero_arr),np.copy(zero_arr)                   # ms(r)d: mean squared (relative) displacement
dpx_out,dpy_out         = np.copy(zero_arr),np.copy(zero_arr)
drx_out,dry_out         = np.copy(zero_arr),np.copy(zero_arr)

# scalar quantities
num_trajs,num_hops = 0,0
msd_mean,msrd_mean = 0.,0.

######################

for cnf_i,cnf in enumerate(cnfs):
    Vs = np.sort([int(Vdir[2:]) for Vdir in os.listdir('{}/Cnf-{:d}'.format(inpath,cnf)) if Vdir[:2]=='V-'])
    if fixflag:
        Vs = Vs[:fixer.num_Vs(cnf_i)]
    elif maxnumVs is not None:
        Vs = Vs[:maxnumVs]

    for V in Vs:
        num_trajs += 1

        Vpath = '{}/Cnf-{:d}/V-{:d}'.format(inpath,cnf,V)
        sys_params = np.load('{}/sys_params.npz'.format(Vpath),allow_pickle=True)
        d,L,diams = int(sys_params['d']),float(sys_params['L']),sys_params['diameter']
        assert d==2

        types,num_species = np.searchsorted(platdata['dedges'][:-1],diams,side='right')-1,platdata['dedges'].size-1
        plateaus = np.zeros(N)
        for i in range(platdata['dedges'].size-1):
            plateaus[types==i] = platdata['p_c'][i]
        pc2 = plateaus**2

        p0 = PBC(np.load('{}/quenched_position/0.npy'.format(Vpath)).astype(float),L)
        p1 = PBC(np.load('{}/quenched_position/{:d}.npy'.format(Vpath,Dt_i)).astype(float),L)
        if args.backwards:
            p0,p1 = p1,p0

        # prep. cage-relative stuff #
        nlist_c0,nlist_inds_c0 = get_nlist(p0,L,R_c)
        nlist_c1,nlist_inds_c1 = get_nlist(p1,L,R_c)
        nlist_c,nlist_inds_c = merge_nlists(nlist_c0,nlist_inds_c0,nlist_c1,nlist_inds_c1)

        nlist_D0,nlist_inds_D0 = get_nlist(p0,L,R_D)
        nlist_D1,nlist_inds_D1 = get_nlist(p1,L,R_D)
        nlist_D,nlist_inds_D = merge_nlists(nlist_D0,nlist_inds_D0,nlist_D1,nlist_inds_D1)

        # calc. one-time quantities #

        # potential energy and stress
        nlist_i0,nlist_inds_i0 = shrink_nhood(p0,L,1.25,nlist_D0,nlist_inds_D0)   # ''interaction grid'':
        nlist_i1,nlist_inds_i1 = shrink_nhood(p1,L,1.25,nlist_D1,nlist_inds_D1)   # smallest grid that captures all interacting pairs

        V0 = get_potential_energy(p0,diams,L,nlist_i0,nlist_inds_i0)
        V1 = get_potential_energy(p1,diams,L,nlist_i1,nlist_inds_i1)

        Stress0 = get_stress(p0,diams,L,nlist_i0,nlist_inds_i0)
        Stress1 = get_stress(p1,diams,L,nlist_i1,nlist_inds_i1)

        Stress0_trace = np.trace(Stress0,axis1=1,axis2=2)
        Stress1_trace = np.trace(Stress1,axis1=1,axis2=2)

        Stress0_detraced = Stress0-np.multiply.outer(Stress0_trace,np.identity(d))/d
        Stress1_detraced = Stress1-np.multiply.outer(Stress1_trace,np.identity(d))/d

        press0,press1 = -Stress0_trace/d,-Stress1_trace/d
        dev_stress0,dev_stress1 = np.sqrt(2)*np.linalg.norm(Stress0_detraced,axis=(1,2)),np.sqrt(2)*np.linalg.norm(Stress1_detraced,axis=(1,2))

        # calc. two-time quantities #

        # displacement
        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist_c,nlist_inds_c)
        dp2=np.sum(dp**2,axis=1)

        msd_mean += np.sum(dp2)

        # strain
        F = .5*(calc_E(p0,dp,L,nlist_D,nlist_inds_D)-calc_E(p1,-dp,L,nlist_D,nlist_inds_D)) # deformation gradient tensor F,
        E = .5*(F+np.swapaxes(F,1,2)) # linear strain tensor E                              # antisym w.r.t. time reversal
        
        E_trace = np.trace(E,axis1=1,axis2=2)
        E_detraced = E-np.multiply.outer(E_trace,np.identity(d))/d
        
        iso_strain = E_trace
        dev_strain = np.sqrt(2)*np.linalg.norm(E_detraced,axis=(1,2))

        # identify and loop over events #

        hopmask = dp2/pc2 > crit
        if not np.any(hopmask):
            continue

        hoppers = mask_to_indices(hopmask)
        num_hops += hoppers.size
        if not args.unidirectional:
            num_hops += hoppers.size

        lambda_plus = .5*(iso_strain[hopmask]+dev_strain[hopmask]) # largest eigenvalue
        thetas = np.arctan((lambda_plus-E[hopmask,0,0])/E[hopmask,0,1]) # angle of eigvec corresp. largest eigval

        for i,theta in zip(hoppers,thetas):
            # forwards in time #

            e_ext = np.array([np.cos(theta),np.sin(theta)])
            e_com = np.array([-e_ext[1],e_ext[0]])

            rij0,rij1 = PBC(p0-p0[i].reshape((1,d)),L),PBC(p1-p1[i].reshape((1,d)),L)
            drij=PBC(rij1-rij0,L); drij2=np.sum(drij**2,axis=1)

            msrd_mean += np.sum(drij2)

            if not args.post:
                dr0,dr1 = rij0,PBC(p1-p0[i].reshape((1,d)),L) # for Eulerian
            else:                                             # coordinates
                dr0,dr1 = PBC(p0-p1[i].reshape((1,d)),L),rij1

            r_eyes0,th_eyes0,mask0 = r_and_th_inds(dr0,theta,r2edges,thedges,retmask=True);  update_hist(r_eyes0,th_eyes0,hist0)
            r_eyes1,th_eyes1,mask1 = r_and_th_inds(dr1,theta,r2edges,thedges,retmask=True);  update_hist(r_eyes1,th_eyes1,hist1)

            # calc. rel. quantities
            s_xx0,s_xx1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_ext),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_ext)
            s_xy0,s_xy1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_com)
            s_yy0,s_yy1 = np.einsum('i,...ij,j',e_com,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_com,Stress1_detraced[mask1],e_com)
            e_xx = np.einsum('i,...ij,j',e_ext,E_detraced[mask0],e_ext)
            e_xy = np.einsum('i,...ij,j',e_ext,E_detraced[mask0],e_com)
            e_yy = np.einsum('i,...ij,j',e_com,E_detraced[mask0],e_com)

            sgnx,sgny = np.sign(dr0[mask0]@e_ext),np.sign(dr0[mask0]@e_com)
            dpx,dpy = sgnx*(dp[mask0]@e_ext),sgny*(dp[mask0]@e_com)
            drx,dry = sgnx*(drij[mask0]@e_ext),sgny*(drij[mask0]@e_com)

            # one-time quantities
            update_field(r_eyes0,th_eyes0,V0[mask0],V0_r);                   update_field(r_eyes1,th_eyes1,V1[mask1],V1_r)
            update_field(r_eyes0,th_eyes0,press0[mask0],press0_r);           update_field(r_eyes1,th_eyes1,press1[mask1],press1_r)
            update_field(r_eyes0,th_eyes0,dev_stress0[mask0],devstress0_r);  update_field(r_eyes1,th_eyes1,dev_stress1[mask1],devstress1_r)
            update_field(r_eyes0,th_eyes0,s_xx0,sxx0_r);                     update_field(r_eyes1,th_eyes1,s_xx1,sxx1_r)
            update_field(r_eyes0,th_eyes0,s_xy0,sxy0_r);                     update_field(r_eyes1,th_eyes1,s_xy1,sxy1_r)
            update_field(r_eyes0,th_eyes0,s_yy0,syy0_r);                     update_field(r_eyes1,th_eyes1,s_yy1,syy1_r)

            # two-time quantities
            update_field(r_eyes0,th_eyes0,dp2[mask0],msd_out);               update_field(r_eyes0,th_eyes0,drij2[mask0],msrd_out)
            update_field(r_eyes0,th_eyes0,iso_strain[mask0],iso_out);        update_field(r_eyes0,th_eyes0,dev_strain[mask0],dev_out)
            update_field(r_eyes0,th_eyes0,e_xx,exx_out); update_field(r_eyes0,th_eyes0,e_xy,exy_out); update_field(r_eyes0,th_eyes0,e_yy,eyy_out)
            update_field(r_eyes0,th_eyes0,dpx,dpx_out);                      update_field(r_eyes0,th_eyes0,dpy,dpy_out)
            update_field(r_eyes0,th_eyes0,drx,drx_out);                      update_field(r_eyes0,th_eyes0,dry,dry_out)

            ## backwards in time #
            if not args.unidirectional:
                theta += .5*np.pi
                e_ext,e_com = e_com,e_ext

                if not args.post:
                    # dr0 (dr1) here is after- (before-) hop relative position (Euler coords)
                    dr0,dr1 = PBC(p0-p1[i].reshape((1,d)),L),rij1
                else:
                    dr0,dr1 = rij0,PBC(p1-p0[i].reshape((1,d)),L)

                sgnx,sgny = np.sign(dr1[mask1]@e_ext),np.sign(dr1[mask1]@e_com)
                dpx,dpy = -sgnx*(dp[mask1]@e_ext),-sgny*(dp[mask1]@e_com)
                drx,dry = -sgnx*(drij[mask1]@e_ext),-sgny*(drij[mask1]@e_com)

                r_eyes0,th_eyes0,mask0 = r_and_th_inds(dr0,theta,r2edges,thedges,retmask=True);  update_hist(r_eyes0,th_eyes0,hist1)
                r_eyes1,th_eyes1,mask1 = r_and_th_inds(dr1,theta,r2edges,thedges,retmask=True);  update_hist(r_eyes1,th_eyes1,hist0)

                # calc. rel. quantities
                s_xx0,s_xx1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_ext),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_ext)
                s_xy0,s_xy1 = np.einsum('i,...ij,j',e_ext,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_ext,Stress1_detraced[mask1],e_com)
                s_yy0,s_yy1 = np.einsum('i,...ij,j',e_com,Stress0_detraced[mask0],e_com),np.einsum('i,...ij,j',e_com,Stress1_detraced[mask1],e_com)
                e_xx = np.einsum('i,...ij,j',e_ext,-E_detraced[mask1],e_ext)
                e_xy = np.einsum('i,...ij,j',e_ext,-E_detraced[mask1],e_com)
                e_yy = np.einsum('i,...ij,j',e_com,-E_detraced[mask1],e_com)

                # one-time quantities
                update_field(r_eyes0,th_eyes0,V0[mask0],V1_r);                   update_field(r_eyes1,th_eyes1,V1[mask1],V0_r)
                update_field(r_eyes0,th_eyes0,press0[mask0],press1_r);           update_field(r_eyes1,th_eyes1,press1[mask1],press0_r)
                update_field(r_eyes0,th_eyes0,dev_stress0[mask0],devstress1_r);  update_field(r_eyes1,th_eyes1,dev_stress1[mask1],devstress0_r)
                update_field(r_eyes0,th_eyes0,s_xx0,sxx1_r);                     update_field(r_eyes1,th_eyes1,s_xx1,sxx0_r)
                update_field(r_eyes0,th_eyes0,s_xy0,sxy1_r);                     update_field(r_eyes1,th_eyes1,s_xy1,sxy0_r)
                update_field(r_eyes0,th_eyes0,s_yy0,syy1_r);                     update_field(r_eyes1,th_eyes1,s_yy1,syy0_r)

                # two-time quantities
                update_field(r_eyes1,th_eyes1,dp2[mask1],msd_out);               update_field(r_eyes1,th_eyes1,drij2[mask1],msrd_out)
                update_field(r_eyes1,th_eyes1,-iso_strain[mask1],iso_out);       update_field(r_eyes1,th_eyes1,dev_strain[mask1],dev_out)
                update_field(r_eyes1,th_eyes1,e_xx,exx_out); update_field(r_eyes1,th_eyes1,e_xy,exy_out); update_field(r_eyes1,th_eyes1,e_yy,eyy_out)
                update_field(r_eyes1,th_eyes1,dpx,dpx_out);                      update_field(r_eyes1,th_eyes1,dpy,dpy_out)
                update_field(r_eyes1,th_eyes1,drx,drx_out);                      update_field(r_eyes1,th_eyes1,dry,dry_out)

    print("{:d}/{:d}".format(cnf,cnfs[-1]),flush=True)

#### normalisation ####

nanmask0,nanmask1 = hist0==0,hist1==0
anmask0,anmask1 = np.invert(nanmask0),np.invert(nanmask1)

# one-time quantities
nanmask=nanmask0+nanmask1; anmask=np.invert(nanmask)
for q,q0,q1 in [[dV_out,V0_r,V1_r],[dpress_out,press0_r,press1_r],[ddevstress_out,devstress0_r,devstress1_r],
                [dsxx_out,sxx0_r,sxx1_r],[dsxy_out,sxy0_r,sxy1_r],[dsyy_out,syy0_r,syy1_r]]:
    q0[anmask0] /= hist0[anmask0].astype(q0.dtype)
    q1[anmask1] /= hist1[anmask1].astype(q1.dtype)
    q[anmask] = q1[anmask] - q0[anmask]
    q[nanmask],q0[nanmask0],q1[nanmask1] = np.nan,np.nan,np.nan

# g(r,theta)
dV = 2.*(r2edges[1:]-r2edges[:-1])*dth # pi r^2 * (dth/(2 pi)) times 4 (because of 4 quadrants)
g0,g1 = (hist0/(num_hops*dV.reshape((-1,1))))/(N/L**d),(hist1/(num_hops*dV.reshape((-1,1))))/(N/L**d)
dg = g1-g0

# two-time quantities
for q in [msd_out,msrd_out,iso_out,dev_out,exx_out,exy_out,eyy_out,dpx_out,dpy_out,drx_out,dry_out]:
    q[anmask0] /= hist0[anmask0].astype(q.dtype)
    q[nanmask0] = np.nan

# scalar quantities
msd_mean = msd_mean/(N*num_trajs)
msrd_mean = msrd_mean/(N*num_hops)

#######################

# combine dpx,dpy and drx,dpy
dp_out = np.zeros((redges.size-1,num_th,2))
dp_out[:,:,0],dp_out[:,:,1] = dpx_out,dpy_out

dr_out = np.zeros((redges.size-1,num_th,2))
dr_out[:,:,0],dr_out[:,:,1] = drx_out,dry_out

# fin #
np.savez(outpath,
         redges=redges,thedges=thedges,numhops=num_hops,hist0=hist0,hist1=hist1,
         g0=g0,g1=g1,dg=dg,
         V0=V0_r,V1=V1_r,dV=dV_out,
         P0=press0_r,P1=press1_r,dP=dpress_out,
         sdev0=devstress0_r,sdev1=devstress1_r,dsdev=ddevstress_out,
         sxx0=sxx0_r,sxx1=sxx1_r,dsxx=dsxx_out,
         sxy0=sxy0_r,sxy1=sxy1_r,dsxy=dsxy_out,
         syy0=syy0_r,syy1=syy1_r,dsyy=dsyy_out,
         iso=iso_out,dev=dev_out,exx=exx_out,exy=exy_out,eyy=eyy_out,
         dp=dp_out,dr=dr_out,msd=msd_out,msrd=msrd_out,msd_mean=msd_mean,msrd_mean=msrd_mean,
         metadata=metadata)
