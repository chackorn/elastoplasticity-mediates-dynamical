"""
rmsd_LogTime.py: Calculate the rmsd and non-Gaussian parameter alpha_2
given log-spaced data, averaged over independent configurations (but not time).
The output is for data averaged over all bins, as well as averaged within bins of particle diameter.
"""

import os
import argparse
import subprocess
import shlex
from datetime import datetime

import numpy as np
from numba import njit

import host_info
import sys; sys.path.extend(host_info.import_paths)
from swap_tools import get_dminmax,get_xi_dyn
from numba_tools import get_dp_com,parse_brackets,update_hist,update_field
from nlist_tools import PBC,get_nlist,merge_nlists

##### Sort out input #####

parser = argparse.ArgumentParser()
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-R","--cage_radius",type=float,
                    help="Calculate displacements relative to the motion of the centre of mass of particles within a distance R\
                          from each particle (excluding the particle).\
                          Default: None (get cage_radius from get_xi_dyn)",
                    default=None)
parser.add_argument("-b","--num_bins",type=int,
                    help="Number of bins into which to separate particle diameters.\
                          Default: 10",
                    default=10)
parser.add_argument("-t","--tmax",type=float,
                   help="Max Dt (in time units) to use, out of those available.\
                         Default: np.inf (use all available frames).",
                   default=np.inf)
parser.add_argument("-C","--Cnfs",
                    help='Maximum range of configurations (one traj per cnf) to average over.\
                          Input [C0,C1] means cnfs is a subset of np.arange(C0,C1).\
                          Default: Use all available configurations',
                    default=None)
parser.add_argument("-i","--inpath",
                    help='Path to trajectory directory.\
                          Default: "{}/trajs_EQ_poly_SS_2D_N{{N:d}}_T{{T:.4f}}"'.format(host_info.TwoTimeTrajs),
                    default=None)
parser.add_argument("-o","--outfile",
                    help='Path to .npz in which to save rmsd output.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/rmsd.npz"'.format(host_info.TwoTimeOut),
                    default=None)
parser.add_argument("-a","--outfile_a2",
                    help='Path to .npz in which to save alpha_2 output.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/alpha_2.npz"'.format(host_info.TwoTimeOut),
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':datetime.now(),**vars(args)}

T = args.temperature
N = args.num_particles
R = args.cage_radius if args.cage_radius is not None else get_xi_dyn(T)
num_bins = args.num_bins
tmax = args.tmax

inpath = args.inpath
if inpath is None:
    inpath = "{}/trajs_EQ_poly_SS_2D_N{:d}_T{:.4f}".format(host_info.TwoTimeTrajs,N,T)
else:
    inpath = os.path.expanduser(inpath)

outfile = args.outfile
if outfile is None:
    outfile = "{}/T{:.4f}/N{:d}/rmsd.npz".format(host_info.TwoTimeOut,T,N)
else:
    outfile = os.path.expanduser(outfile)

assert outfile[-4:] == ".npz"
outpath = outfile[:outfile.rfind('/')]
subprocess.call(shlex.split("mkdir -p {}".format(outpath)))

outfile_a2 = args.outfile_a2
if outfile_a2 is None:
    outfile_a2 = "{}/T{:.4f}/N{:d}/alpha_2.npz".format(host_info.TwoTimeOut,T,N)
else:
    outfile_a2 = os.path.expanduser(outfile_a2)

assert outfile_a2[-4:] == ".npz"
outpath_a2 = outfile_a2[:outfile_a2.rfind('/')]
subprocess.call(shlex.split("mkdir -p {}".format(outpath_a2)))

cnfs = np.sort([int(cnfdir[4:]) for cnfdir in os.listdir(inpath) if cnfdir[:4]=="Cnf-"])
if args.Cnfs is not None:
    cnfs = np.intersect1d(cnfs,np.arange(*parse_brackets(args.Cnfs,[int,int])))

##########################

# get list of frames #
sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,cnfs[0]),allow_pickle=True)
d,L = int(sys_params['d']),float(sys_params['L'])
assert int(sys_params['N']) == N

assert sys_params['step'][0]==0
tmask = sys_params['time'][1:]<=tmax
frames,Dts = sys_params['step'][1:][tmask],sys_params['time'][1:][tmask]

dmin,dmax = get_dminmax()
dedges = np.logspace(np.log10(dmin),np.log10(dmax),num_bins+1)
hist = np.zeros(num_bins,dtype=int)
msd,mqd = np.zeros((Dts.size,num_bins)),np.zeros((Dts.size,num_bins))

for cnf in cnfs:
    sys_params = np.load('{}/Cnf-{:d}/sys_params.npz'.format(inpath,cnf),allow_pickle=True)
    diams = sys_params['diameter']
    types = np.searchsorted(dedges[:-1],diams,side='right')-1

    update_hist(types,hist)

    p0 = np.load("{}/Cnf-{:d}/position/0.npy".format(inpath,cnf))
    nlist0,nlist_inds0 = get_nlist(p0.astype(float),L,R)

    for Dt_i,frame in enumerate(frames):
        p1 = np.load("{}/Cnf-{:d}/position/{:d}.npy".format(inpath,cnf,frame))
        nlist1,nlist_inds1 = get_nlist(p1.astype(float),L,R)
        nlist,nlist_inds = merge_nlists(nlist0,nlist_inds0,nlist1,nlist_inds1)

        dp = PBC(p1-p0,L); dp -= get_dp_com(dp,nlist,nlist_inds)

        update_field(types,np.sum(dp**2,axis=1),msd[Dt_i,:])
        update_field(types,np.sum(dp**4,axis=1),mqd[Dt_i,:])

    print("{:d}/{:d}".format(cnf,cnfs[-1]),flush=True)

nanmask=(hist==0); anmask=np.invert(nanmask)
msd_all,mqd_all = [np.sum(q,axis=1)/np.sum(hist) for q in [msd,mqd]]
for q in [msd,mqd]:
    q[:,anmask] /= hist[anmask].astype(q.dtype).reshape(1,-1)
    q[:,nanmask] = np.nan

rmsd,rmsd_all = [np.sqrt(q) for q in [msd,msd_all]]
alpha_2,alpha_2_all = [(d/(d+2.))*q/(s**2)-1. for (s,q) in [(msd,mqd),(msd_all,mqd_all)]]

np.savez(outfile,dedges=dedges,Dt=Dts,rmsd=rmsd,rmsd_all=rmsd_all,hist=hist,metadata=metadata)
np.savez(outfile_a2,dedges=dedges,Dt=Dts,alpha_2=alpha_2,alpha_2_all=alpha_2_all,hist=hist,metadata=metadata)
