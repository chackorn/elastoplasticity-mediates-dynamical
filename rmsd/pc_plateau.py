"""
pc_plateau.py: Given rmsd data that has been binned based on particle diameter,
save a file containing the rmsd plateau values exctracted from that data.
"""

import os
import argparse
import subprocess
import shlex
from datetime import datetime

import numpy as np
from numba import njit

import host_info
import sys; sys.path.extend(host_info.import_paths)
from swap_tools import get_dminmax,get_xi_dyn
from numba_tools import get_dp_com,parse_brackets,update_hist,update_field
from nlist_tools import PBC,get_nlist,merge_nlists

##### Sort out input #####

parser = argparse.ArgumentParser()
parser.add_argument("-T","--temperature",type=float,required=True,
                    help="Temperature T.")
parser.add_argument("-N","--num_particles",type=int,
                    help="Number of particles N.\
                          Default: 10000",
                    default=10000)
parser.add_argument("-t","--plateau_time",type=float,
                    help="Time at which sample the plateau value from the rmsd data.\
                          Default: 10.",
                    default=10.)
parser.add_argument("-i","--infile",
                    help='Path to .npz containing rmsd data.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/rmsd.npz"'.format(host_info.TwoTimeOut),
                    default=None)
parser.add_argument("-o","--outfile",
                    help='Path to .npz in which to save plateau output.\
                          Default: "{}/T{{T:.4f}}/N{{N:d}}/pc_plateau.npz"'.format(host_info.TwoTimeOut),
                    default=None)
args = parser.parse_args()

metadata = {'code':os.path.abspath("./{}".format(sys.argv[0])),'timestamp':datetime.now(),**vars(args)}

T = args.temperature
N = args.num_particles
t = args.plateau_time

infile = args.infile
if infile is None:
    infile = "{}/T{:.4f}/N{:d}/rmsd.npz".format(host_info.TwoTimeOut,T,N)

outfile = args.outfile
if outfile is None:
    outfile = "{}/T{:.4f}/N{:d}/pc_plateau.npz".format(host_info.TwoTimeOut,T,N)
else:
    outfile = os.path.expanduser(outfile)

assert outfile[-4:] == ".npz"
outpath = outfile[:outfile.rfind('/')]
subprocess.call(shlex.split("mkdir -p {}".format(outpath)))

##########################

data = np.load(infile,allow_pickle=True)
dedges,ts,rmsd = data['dedges'],data['Dt'],data['rmsd']

dataplat = np.load("{}/T{:.4f}/N{:d}/pc_plateau.npz".format(host_info.TwoTimeOut,T,N),allow_pickle=True)

t_i = np.searchsorted(ts,t)
r0,r1 = rmsd[t_i-1],rmsd[t_i]
t0,t1 = ts[t_i-1],ts[t_i]
pc = (r0*t1 - r1*t0 + (r1-r0)*t)/(t1-t0)

np.savez(outfile,p_c=pc,dedges=dedges,metadata=metadata)
